<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use App\Models\FundNAV;


class UpdateVNIndexCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'uploadVNIndex';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info('uploadVNIndex');
        // $response = Http::get('https://api.tradingeconomics.com/markets/search/vnindex?c=guest:guest');
        // $data = $response->collect()->first();
        $headers = array(
            'Content-Type: application/json',
            'Accept: application/json',
        );
        $url = 'https://api.tradingeconomics.com/markets/search/vnindex?c=guest:guest';
        $curl = curl_init($url);
        curl_setopt_array($curl, array(
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_VERBOSE => true,
        ));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($curl);
        $data = json_decode($response)[0];
        FundNAV::updateOrCreate([
            'trading_session_time' => substr($data->Date, 0, 10),
            'type' => 1,
        ],[
            'amount' => $data->Last,
        ]);
        curl_close($curl);
        Log::info($data->Last.' '.substr($data->Date, 0, 10));
    }
}
