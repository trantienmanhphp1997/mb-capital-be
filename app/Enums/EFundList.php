<?php

namespace App\Enums;

class EFundList {

    public static function getListData()
    {
        return [
            1 => 'Quỹ đầu tư trái phiếu MB (MBBOND)',
            2 => 'Quỹ đầu tư Japan Asia MB Capital (JAMBF)',
            3 => 'Chương trình Hưu trí An thịnh',
            4 => 'Quỹ đầu tư giá trị MB Capital (MBVF)',
        ];
    }

    public static function valueToName($key)
    {
        $data = static::getListData();
        return $data[$key] ?? '';
    }
}
