<?php

namespace App\Exports;

use App\Models\Fund;
use App\Models\FundNAV;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Events\AfterSheet;

class FundNavExport implements  FromCollection,WithHeadings, WithMapping,ShouldAutoSize, WithEvents
{

    protected $searchFund,$listNav,$from_date,$to_date;

    function __construct($searchFund,$listNav,$from_date,$to_date) {
            $this->searchFund = $searchFund;
            $this->listNav = $listNav;
            $this->from_date =$from_date;
            $this->to_date =$to_date;
    }

    public function collection()
    {
        $query=FundNAV::query();
        $query->leftjoin('fund','fund_nav.fund_id','=','fund.id')
        ->select('fund_nav.*','fund.shortname as fundname');
        if ($this->searchFund) {
            if ($this->searchFund == 'type_1') {
                $query->where('fund_nav.type', '1'); // VNINDEX
            } else {
                $query->where('fund_id', $this->searchFund);
            }
        }
        if (!empty($this->from_date) && !empty($this->to_date)) {
            $query->whereDate('fund_nav.trading_session_time', '>=', $this->from_date)
            ->whereDate('fund_nav.trading_session_time', '<=', $this->to_date);
        }
        $data=$query->orderBy('id','desc')->get();
        
        return $data;
    }

    public function headings(): array
    {
        return [
          'Tên quỹ đầu tư',
          'Số tiền',
          'Ngày công bố',
        ];
    }

    public function map($listNav): array
    {
        
        return [
            $listNav->fundname == NULL ? ($listNav->type == '1' ? 'VNINDEX' : '') : $listNav->fundname,
            $listNav->amount,
            reformatDate($listNav->trading_session_time,'d/m/Y')
        ];
        dd($listNav);
    }
    
    public function registerEvents(): array
    {
        return [AfterSheet::class => function (AfterSheet $event) {
        }];
  
    }
}