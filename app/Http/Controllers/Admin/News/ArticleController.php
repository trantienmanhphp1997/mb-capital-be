<?php

namespace App\Http\Controllers\Admin\News;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Helpers;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Session;

class ArticleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $keyword = $request->get('keyword');
        $is_search = (isset($keyword) && $keyword);
        $newData = [];
        if($is_search){
            $newData = Article::where('status', '=', 1)
                ->query(function($query) use($keyword) {
                    $query->where('name_vi', 'like', '%'. $keyword .'%')
                    ->orWhere('name_en', 'like', '%'. $keyword .'%')
                    ->orWhere('intro_vi', 'like', '%'. $keyword .'%')
                    ->orWhere('intro_en', 'like', '%'. $keyword .'%')
                    ->orWhere('content_vi', 'like', '%'. $keyword .'%')
                    ->orWhere('content_en', 'like', '%'. $keyword .'%');
                })->paginate(10);
        }else{
            $newData = Article::Where('status', '=', 1)->paginate(10);
        }
        // dd($newData);
        return view('admin.news.list.index', compact('newData'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        return view('admin.news.list.create');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Article::rules(),
        $messages = [
            'name_vi.required' => 'Tiêu đề không được phép để trống.',
            'intro_vi.required' => 'Mô tả không được phép để trống.'
        ]);
        if($validator->fails()){
            return redirect('new/create')->withErrors($validator)->withInput();
        }
        $input= new Article();
        // save content_vi
        if($request->content_vi){
            $content_vi = $request->content_vi;
            $content_vi = str_replace('&gt;','>',$content_vi);
            $content_vi = str_replace('&lt;','<',$content_vi);
            $dom = new \DomDocument();
            // dd($content_vi);
            libxml_use_internal_errors(true);
            $dom->loadHtml(mb_convert_encoding($content_vi, 'HTML-ENTITIES', 'UTF-8'));
            $images = $dom->getElementsByTagName('img');
            foreach($images as $k => $img){
                $data = $img->getAttribute('src');
                $image_old = strpos($data, 'data:image');
                if ($image_old !== false) {
                    list($type, $data) = explode(';', $data);
                    list(, $data)      = explode(',', $data);
                    $data = base64_decode($data);
                    $image_name= 'public/article-file-upload/' . time().mt_rand(1000000, 9999999).$k.'.png';
                    $path = storage_path() .'/app/'. $image_name;
                    $path = str_replace('be/storage','storage',$path);
                    $path = str_replace('be\storage','storage',$path);
                    file_put_contents($path, $data);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', '/storage/'.$image_name);
                }
            }
            $content_vi = $dom->saveHTML();
            // xử lý bỏ html body
            if(strpos($content_vi, '<html><body>')!==false){
                $pos = strpos($content_vi, '<html><body>');
                $content_vi = substr($content_vi, $pos+12); // length <html><body>
            }
            if(stripos($content_vi, '</body></html>')!==false){
                $pos = stripos($content_vi, '</body></html>');
                $content_vi = substr($content_vi, 0,$pos);
            }
            $input->content_vi = $content_vi;
            libxml_use_internal_errors(false);
        }
        // save content_en
        if($request->content_en){
            $content_en = $request->content_en;
            $content_en = str_replace('&gt;','>',$content_en);
            $content_en = str_replace('&lt;','<',$content_en);
            $dom = new \DomDocument();
            libxml_use_internal_errors(true);
            $dom->loadHtml(mb_convert_encoding($content_en, 'HTML-ENTITIES', 'UTF-8'));
            $images = $dom->getElementsByTagName('img');
            foreach($images as $k => $img){
                $data = $img->getAttribute('src');
                $image_old = strpos($data, 'data:image');
                if ($image_old !== false) {
                    list($type, $data) = explode(';', $data);
                    list(, $data)      = explode(',', $data);
                    $data = base64_decode($data);
                    $image_name= 'public/article-file-upload/' . time().mt_rand(1000000, 9999999).$k.'.png';
                    $path = storage_path() .'/app/'. $image_name;
                    $path = str_replace('be/storage','storage',$path);
                    $path = str_replace('be\storage','storage',$path);
                    file_put_contents($path, $data);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', '/storage/'.$image_name);
                }
            }
            $content_en = $dom->saveHTML();
            // xử lý bỏ html body
            if(strpos($content_en, '<html><body>')!==false){
                $pos = strpos($content_en, '<html><body>');
                $content_en = substr($content_en, $pos+12); // length <html><body>
            }
            if(stripos($content_en, '</body></html>')!==false){
                $pos = stripos($content_en, '</body></html>');
                $content_en = substr($content_en, 0,$pos);
            }
            $input->content_en = $content_en;
            libxml_use_internal_errors(false);
        }


        $input->name_vi= $request->name_vi;
        $input->name_en= $request->name_en;
        $input->intro_vi= $request->intro_vi;
        $input->intro_en= $request->intro_en;
        // $input->content_vi= $request->content_vi;
        // $input->content_en= $request->content_en;
        $input->meta_title_vi= $request->meta_title_vi;
        $input->meta_title_en= $request->meta_title_en;
        $input->meta_des_vi= $request->meta_des_vi;
        $input->meta_des_en= $request->meta_des_vi;
        $input->author= $request->author;
        if($request->image){
            $input->image= 'storage/'.$request->image->store('public/article-file-upload');
        }
        $input->status= $request->status;
        $input->category= $request->category2;
        $input->slug= Helpers\Slug::slugify($request->name_vi);
        $input->slug_en= Helpers\Slug::slugify($request->name_en);
        $input->date_submit = $request->date_submit;
        $input->save();
        return redirect()->route('admin.new.list.index')->with('success', 'Tạo mới thành công');
    }


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {
        $info = Article::findOrFail($id);
        //content_vi
        $image_old = strpos($info->content_vi, '/storage/public/article-file-upload/');
        if ($image_old !== false) {
            $info->content_vi = str_replace('https://mbcapital.com.vn/storage/public/article-file-upload/','/storage/public/article-file-upload/',$info->content_vi);
        }
        //content_en
        $image_old = strpos($info->content_en, '/storage/public/article-file-upload/');
        if ($image_old !== false) {
            $info->content_en = str_replace('https://mbcapital.com.vn/storage/public/article-file-upload/','/storage/public/article-file-upload/',$info->content_en);
        }

        return view('admin.news.list.edit', compact('id', 'info'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), Article::rules(),
        $messages = [
            'name_vi.required' => 'Tiêu đề không được phép để trống.',
            'intro_vi.required' => 'Mô tả không được phép để trống.'
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }
        // save content_vi
        if($request->content_vi){
            $content_vi = $request->content_vi;
            $content_vi = str_replace('&gt;','>',$content_vi);
            $content_vi = str_replace('&lt;','<',$content_vi);
            $dom = new \DomDocument();
            libxml_use_internal_errors(true);
            $dom->loadHtml(mb_convert_encoding($content_vi, 'HTML-ENTITIES', 'UTF-8'));
            $images = $dom->getElementsByTagName('img');
            foreach($images as $k => $img){
                $data = $img->getAttribute('src');
                // kiểm tra là ảnh mới hay ảnh cũ
                $image_old = strpos($data, 'data:image');
                if ($image_old !== false) {
                    list($type, $data) = explode(';', $data);
                    list(, $data)      = explode(',', $data);
                    $data = base64_decode($data);
                    $image_name= '/public/article-file-upload/' . time().mt_rand(1000000, 9999999).$k.'.png';
                    $path = public_path('storage') . $image_name;
                    $path = str_replace('be/storage','storage',$path);
                    $path = str_replace('be\storage','storage',$path);
                    file_put_contents($path, $data);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', '/storage/'.$image_name);
                }
                else {
                    $image_old = strpos($data, '/storage/public/article-file-upload/');
                    if ($image_old !== false) {
                        $data = str_replace('https://mbcapital.com.vn','',$data);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', $data);
                    }
                }
            }

            $content_vi = $dom->saveHTML();
            libxml_use_internal_errors(false);

            // xử lý bỏ html body
            if(strpos($content_vi, '<html><body>')!==false){
                $pos = strpos($content_vi, '<html><body>');
                $content_vi = substr($content_vi, $pos+12); // length <html><body>
            }
            if(stripos($content_vi, '</body></html>')!==false){
                $pos = stripos($content_vi, '</body></html>');
                $content_vi = substr($content_vi, 0,$pos);
            }
        }
        // save content_en
        if($request->content_en){
            $content_en = $request->content_en;
            $content_en = str_replace('&gt;','>',$content_en);
            $content_en = str_replace('&lt;','<',$content_en);
            $dom = new \DomDocument();
            libxml_use_internal_errors(true);
            $dom->loadHtml(mb_convert_encoding($content_en, 'HTML-ENTITIES', 'UTF-8'));
            $images = $dom->getElementsByTagName('img');
            foreach($images as $k => $img){
                $data = $img->getAttribute('src');
                // kiểm tra là ảnh mới hay ảnh cũ
                $image_old = strpos($data, 'data:image');
                if ($image_old !== false) {
                    list($type, $data) = explode(';', $data);
                    list(, $data)      = explode(',', $data);
                    $data = base64_decode($data);
                    $image_name= '/public/article-file-upload/' . time().mt_rand(1000000, 9999999).$k.'.png';
                    $path = public_path('storage') . $image_name;
                    $path = str_replace('be/storage','storage',$path);
                    $path = str_replace('be\storage','storage',$path);
                    file_put_contents($path, $data);
                    $img->removeAttribute('src');
                    $img->setAttribute('src', '/storage/'.$image_name);
                }
                else {
                    $image_old = strpos($data, '/storage/public/article-file-upload/');
                    if ($image_old !== false) {
                        $data = str_replace('https://mbcapital.com.vn','',$data);
                        $img->removeAttribute('src');
                        $img->setAttribute('src', $data);
                    }
                }
            }
            $content_en = $dom->saveHTML();
            libxml_use_internal_errors(false);
            // xử lý bỏ html body
            if(strpos($content_en, '<html><body>')!==false){
                $pos = strpos($content_en, '<html><body>');
                $content_en = substr($content_en, $pos+12); // length <html><body>
            }
            if(stripos($content_en, '</body></html>')!==false){
                $pos = stripos($content_en, '</body></html>');
                $content_en = substr($content_en, 0,$pos);
            }
        }

        $data = [
            'name_vi'=> $request->name_vi,
            'name_en'=> $request->name_en,
            'intro_vi'=> $request->intro_vi,
            'intro_en'=> $request->intro_en,
            'content_vi'=> $content_vi??$request->content_vi,
            'content_en'=> $content_en??$request->content_en,
            'meta_title_vi'=> $request->meta_title_vi,
            'meta_title_en'=> $request->meta_title_en,
            'meta_des_vi'=> $request->meta_des_vi,
            'meta_des_en'=> $request->meta_des_vi,
            'status'=> $request->status,
            'author'=> $request->author,
            'slug'=> Helpers\Slug::slugify($request->name_vi),
            'slug_en'=> Helpers\Slug::slugify($request->name_en),
            'category' => $request->category2,
            'date_submit' => $request->date_submit,
        ];
        if($request->image){
            $data['image'] = 'storage/'.$request->image->store('public/article-file-upload');
        }
        $status = Article::findOrFail($id)->update($data);
        $info = Article::findOrFail($id);
        return redirect()->route('admin.new.list.index')->with('success', 'Chỉnh sửa thành công');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function detail($id)
    {
        $info = Article::findOrFail($id);
        // dd($info);
        return view('admin.news.list.detail', compact('info'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function destroy($id)
    {
        return redirect()->route('admin.new.list.index')->with('status', 'success');
    }
}
