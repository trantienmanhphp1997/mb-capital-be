<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Fund as Model;


class Fund extends BaseLive
{

    public $shortname;
    public $shortname_en;
    public $fullname;
    public $fullname_en;
    public $description;
    public $description_en;
    public $content;
    public $content_en;
    public $created_by;
    public $type;
    public $current_nav;
    public $growth;
    public $slug;
    public $slug_en;
    public $fun_code;
    public $interest;
    public $parent_id;
    public $priority;
    public $shortname2;
    public $shortname2_en;
    public $fullname2;
    public $fullname2_en;
    public $public_date;
    public $public_date_en;


    public $mode = 'create';

    public $showForm = false;

    public $primaryId = null;

    public $search;

    public $showConfirmDeletePopup = false;

    protected $rules = [
        'shortname' => 'required',
        'shortname_en' => 'required',
        'fullname' => 'required',
        'fullname_en' => 'required',
        'description' => 'required',
        'description_en' => 'required',
        'content' => 'required',
        'content_en' => 'required',
        'created_by' => 'required',
        'type' => 'required',
        'current_nav' => 'required',
        'growth' => 'required',
        'slug' => 'required',
        'slug_en' => 'required',
        'fun_code' => 'required',
        'interest' => 'required',
        'parent_id' => 'required',
        'priority' => 'required',
        'shortname2' => 'required',
        'shortname2_en' => 'required',
        'fullname2' => 'required',
        'fullname2_en' => 'required',

    ];



    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function updatingSearch()
    {
        $this->resetPage();
    }

    public function render()
    {
        $model = Model::where('shortname', 'like', '%'.$this->search.'%')->orWhere('shortname_en', 'like', '%'.$this->search.'%')->orWhere('fullname', 'like', '%'.$this->search.'%')->orWhere('fullname_en', 'like', '%'.$this->search.'%')->orWhere('description', 'like', '%'.$this->search.'%')->orWhere('description_en', 'like', '%'.$this->search.'%')->orWhere('content', 'like', '%'.$this->search.'%')->orWhere('content_en', 'like', '%'.$this->search.'%')->orWhere('created_by', 'like', '%'.$this->search.'%')->orWhere('type', 'like', '%'.$this->search.'%')->orWhere('current_nav', 'like', '%'.$this->search.'%')->orWhere('growth', 'like', '%'.$this->search.'%')->orWhere('slug', 'like', '%'.$this->search.'%')->orWhere('slug_en', 'like', '%'.$this->search.'%')->orWhere('fun_code', 'like', '%'.$this->search.'%')->orWhere('interest', 'like', '%'.$this->search.'%')->orWhere('parent_id', 'like', '%'.$this->search.'%')->orWhere('priority', 'like', '%'.$this->search.'%')->orWhere('shortname2', 'like', '%'.$this->search.'%')->orWhere('shortname2_en', 'like', '%'.$this->search.'%')->orWhere('fullname2', 'like', '%'.$this->search.'%')->orWhere('fullname2_en', 'like', '%'.$this->search.'%')->orWhere('shortname2', 'like', '%'.$this->search.'%')->orWhere('shortname2_en', 'like', '%'.$this->search.'%')->latest()->paginate($this->paginate);
        return view('livewire.fund', [
            'rows'=> $model
        ]);
    }


    public function create ()
    {
        $this->mode = 'create';
        $this->resetForm();
        $this->showForm = true;
    }


    public function edit($primaryId)
    {
        $this->mode = 'update';
        $this->primaryId = $primaryId;
        $model = Model::find($primaryId);

        $this->shortname= $model->shortname;
        $this->shortname_en= $model->shortname_en;
        $this->fullname= $model->fullname;
        $this->fullname_en= $model->fullname_en;
        $this->description= $model->description;
        $this->description_en= $model->description_en;
        $this->content= $model->content;
        $this->content_en= $model->content_en;
        $this->created_by= $model->created_by;
        $this->type= $model->type;
        $this->current_nav= $model->current_nav;
        $this->growth= $model->growth;
        $this->slug= $model->slug;
        $this->slug_en= $model->slug_en;
        $this->fun_code= $model->fun_code;
        $this->interest= $model->interest;
        $this->parent_id= $model->parent_id;
        $this->priority= $model->priority;
        $this->shortname2= $model->shortname2;
        $this->shortname2_en= $model->shortname2_en;
        $this->public_date= $model->public_date;
        $this->public_date_en= $model->public_date_en;
        $this->showForm = true;
    }

    public function closeForm()
    {
        $this->showForm = false;
    }

    public function store()
    {
        $this->validate();

        $model = new Model();

        $model->shortname= $this->shortname;
        $model->shortname_en= $this->shortname_en;
        $model->fullname= $this->fullname;
        $model->fullname_en= $this->fullname_en;
        $model->description= $this->description;
        $model->description_en= $this->description_en;
        $model->content= $this->content;
        $model->content_en= $this->content_en;
        $model->created_by= $this->created_by;
        $model->type= $this->type;
        $model->current_nav= $this->current_nav;
        $model->growth= $this->growth;
        $model->slug= $this->slug;
        $model->slug_en= $this->slug_en;
        $model->fun_code= $this->fun_code;
        $model->interest= $this->interest;
        $model->parent_id= $this->parent_id;
        $model->priority= $this->priority;
        $model->shortname2= $this->shortname2;
        $model->shortname2_en= $this->shortname2_en;
        $model->fullname2= $this->fullname2;
        $model->fullname2_en= $this->fullname2_en;
        $model->public_date= $this->public_date;
        $model->public_date_en= $this->public_date_en;
        $model->save();

        $this->resetForm();
        session()->flash('message', 'Record Saved Successfully');
        $this->showForm = false;
    }

    public function resetForm()
    {
        $this->shortname= "";
        $this->shortname_en= "";
        $this->fullname= "";
        $this->fullname_en= "";
        $this->description= "";
        $this->description_en= "";
        $this->content= "";
        $this->content_en= "";
        $this->created_by= "";
        $this->type= "";
        $this->current_nav= "";
        $this->growth= "";
        $this->slug= "";
        $this->slug_en= "";
        $this->fun_code= "";
        $this->interest= "";
        $this->parent_id= "";
        $this->priority= "";
        $this->shortname2= "";
        $this->shortname2_en= "";
        $this->fullname2= "";
        $this->fullname2_en= "";
        $this->public_date= "";
        $this->public_date_en= "";

    }


    public function update()
    {
        $this->validate();

        $model = Model::find($this->primaryId);

        $model->shortname= $this->shortname;
        $model->shortname_en= $this->shortname_en;
        $model->fullname= $this->fullname;
        $model->fullname_en= $this->fullname_en;
        $model->description= $this->description;
        $model->description_en= $this->description_en;
        $model->content= $this->content;
        $model->content_en= $this->content_en;
        $model->created_by= $this->created_by;
        $model->type= $this->type;
        $model->current_nav= $this->current_nav;
        $model->growth= $this->growth;
        $model->slug= $this->slug;
        $model->slug_en= $this->slug_en;
        $model->fun_code= $this->fun_code;
        $model->interest= $this->interest;
        $model->parent_id= $this->parent_id;
        $model->priority= $this->priority;
        $model->shortname2= $this->shortname2;
        $model->shortname2_en= $this->shortname2_en;
        $model->fullname2= $this->fullname2;
        $model->fullname2_en= $this->fullname2_en;
        $model->public_date= $this->public_date;
        $model->public_date_en= $this->public_date_en;
        $model->save();
        $this->resetForm();

        $this->showForm = false;

        session()->flash('message', 'Record Updated Successfully');
    }

    public function confirmDelete($primaryId)
    {
        $this->primaryId = $primaryId;
        $this->showConfirmDeletePopup = true;
    }

    public function destroy()
    {
        Model::find($this->primaryId)->delete();
        $this->showConfirmDeletePopup = false;
        session()->flash('message', 'Record Deleted Successfully');
    }

    public function clearFlash()
    {
        session()->forget('message');
    }

}
