<?php

namespace App\Http\Controllers;

use App\Enums\ECommon;
use App\Models\Employee;
use App\Models\Fund;
use http\Url;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Session;

class EmployeeController extends Controller
{
    //
    public function index()
    {
        return view('admin.employee.index');
    }

    /**
     * Show the create ui.
     * May be : show, create-form, edit-form
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = [
            '1' => ECommon::BOARD_OF_DIRECTORS,
            '2' => ECommon::CONTROL_BOARD,
            '3' => ECommon::DIRECTOR_AND_DEPUTY_DIRECTOR,
        ];
        $funds = Fund::all();
        $routeAction = route('admin.employee.store');
        return view('admin.employee.detail')->with(['employee' => null, 'routeAction'=> $routeAction, 'funds'=>$funds, 'roles' => $roles]);
    }

    /**
     * Show the resource detail.
     * May be : show, create-form, edit-form
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = [
            '1' => ECommon::BOARD_OF_DIRECTORS,
            '2' => ECommon::CONTROL_BOARD,
            '3' => ECommon::DIRECTOR_AND_DEPUTY_DIRECTOR,
        ];
        $employee = Employee::where('id', $id)->first();
        $funds = Fund::all();
        $routeAction = route('admin.employee.update', ['id' => $id]);
        return view('admin.employee.detail')->with(['employee' => $employee, 'routeAction'=> $routeAction, 'funds'=>$funds, 'roles' => $roles]);
    }

    /**
     * Take action create for resource.
     * Action : show, create, update
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'firstname' => 'required',
            'lastname' => 'required',
            'sex' => 'required',
            'title' => 'required',
            'type' => 'required',
            'priority' => 'numeric|min:1'
        ],
            [
                'firstname.required' => 'Tên là bắt buộc',
                'lastname.required' => 'Họ là bắt buộc',
                'sex.required' => 'Giới tính là bắt buộc',
                'title.required' => 'Chức danh là bắt buộc',
                'type.required' => 'Vai trò là bắt buộc',
                'priority.numeric' => 'Độ ưu tiên là số',
                'priority.min' => 'Độ ưu tiên là số lớn hơn hoặc bằng 1'
            ]);

        $employee = new Employee ();
        $employee->firstname = $request->firstname;
        $employee->lastname = $request->lastname;
        $employee->fullname = $request->lastname . ' ' . $request->firstname;
        $employee->sex = $request->sex;
        $employee->title = $request->title;
        $employee->title_en = $request->title_en ?? $request->title;
        $employee->title_2 = $request->title_2;
        $employee->title_2_en = $request->title_2_en ?? $request->title;
        $employee->type = $request->type;
        $employee->type_value = $request->type == 1 ? ECommon::BOARD_OF_DIRECTORS :($request->type == 2 ? ECommon::CONTROL_BOARD : ECommon::DIRECTOR_AND_DEPUTY_DIRECTOR);
        $employee->type_value_en = $request->type == 1 ? 'Board of directors' :($request->type == 2 ? 'Control board' : 'Manager and vice manager');
        $employee->img = $request->img;
        $employee->fund_id = $request->fund_id;
        $employee->content = $request->content_vi ?? '';
        $employee->content_en = $request->content_en ?? '';
        $employee->active = $request->active ?-1:null;
        $employee->priority = $request->priority;
        if ($request->hasFile('img')) {
            $file = $request->img;
            $employee->img = \url('/').'/storage/'.$file->store('images');
        }

        $routeRedrict = redirect()->route('admin.employee.index');
        try {
            $employee->save();
            Session::put("message","Tạo mới thành công");
        } catch (\Throwable $th) {
            Session::put("error","Đã có lỗi xảy ra !");
            throw $th;
        }

        return $routeRedrict;
    }


    /**
     * Take action update for resource.
     * Action : show, create, update
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'firstname' => 'required',
            'lastname' => 'required',
            'sex' => 'required',
            'type' => 'required',
            'priority' => 'numeric|min:1',
        ],
            [
                'firstname.required' => 'Tên là bắt buộc',
                'lastname.required' => 'Họ là bắt buộc',
                'sex.required' => 'Giới tính là bắt buộc',
                'title.required' => 'Chức danh là bắt buộc',
                'type.required' => 'Vai trò là bắt buộc',
                'priority.min' => 'Độ ưu tiên là số lớn hơn hoặc bằng 1',
            ]);

        $employee = new Employee ();
        $employee->firstname = $request->firstname;
        $employee->lastname = $request->lastname;
        $employee->fullname = $request->lastname . ' ' . $request->firstname;
        $employee->sex = $request->sex;
        $employee->title = $request->title;
        $employee->title_en = $request->title_en;
        $employee->title_2 = $request->title_2;
        $employee->title_2_en = $request->title_2_en;
        $employee->type = $request->type;
        $employee->type_value = $request->type == 1 ? ECommon::BOARD_OF_DIRECTORS :($request->type == 2 ? ECommon::CONTROL_BOARD : ECommon::DIRECTOR_AND_DEPUTY_DIRECTOR);
        $employee->type_value_en = $request->type == 1 ? 'Board of directors' :($request->type == 2 ? 'Control board' : 'Manager and vice manager');
        $employee->img = $request->img;
        $employee->fund_id = $request->fund_id;
        $employee->content = $request->content_vi ?? '';
        $employee->content_en = $request->content_en ?? '';
        $employee->active = $request->active ?-1:null;
        $employee->priority = $request->priority;
        if ($request->hasFile('img')) {
            $file = $request->img;
//            $employee->img = \url('/').'/storage/'.$file->store('images');
            $employee->img = 'storage/'.$file->store('images');
        }else{
            $oldemp = Employee::where('id', $request->id)->first();
            $employee->img = $oldemp ? $oldemp->img : '';
        }

        $routeRedrict = redirect()->route('admin.employee.index');
        try {

                $updateEmployee = Employee::where('id', $request->id)->update($employee->toArray());
                if ($updateEmployee) {
                    Session::put("message","Cập nhật thông tin thành công");
                }
                else {
                    Session::put("error","Đã có lỗi xảy ra trong quá trình cập nhật");
                }

        } catch (\Throwable $th) {
            Session::put("error","Đã có lỗi xảy ra !");
            throw $th;
        }

        return $routeRedrict;
    }
}
