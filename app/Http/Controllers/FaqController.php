<?php

namespace App\Http\Controllers;

use App\Enums\EFundMaster;
use App\Models\FundMaster;
use App\Models\Fund;
use Illuminate\Http\Request;
use Session;

class FaqController extends Controller {

    public function index()
    {
        return view('admin.faq.index');
    }

}