<?php

namespace App\Http\Controllers;

use App\Models\File;
use Illuminate\Http\Request;

class FilesController extends Controller
{
    public function index(){
       return view('admin.files.index');
    }
    public function edit($id){
        $file = File::find($id);
        return view('admin.files.edit',['file'=>$file]);
    }
}
