<?php

namespace App\Http\Controllers;

use App\Enums\EFundMaster;
use App\Models\File;
use App\Models\FundMaster;
use App\Models\Fund;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class FundMasterController extends Controller
{
    public function index()
    {

        return view('admin.fundmaster.index');
    }

    public function detail($id = null)
    {
        // dd(route('admin.funds.store'));
        $fundms = $id ? FundMaster::where('id', $id)->first() : null;
        // dd($fundms);
        $fundList = Fund::all();

        $model_name = FundMaster::class;
        // dd($model_name);
        $fundMasterTypes = EFundMaster::getList();
        $routeAction = $id ? route('admin.fundmaster.update', ['id' => $id]) : route('admin.fundmaster.store');
        return view('admin.fundmaster.detail')->with([
            'fundms' => $fundms,
            'fundList' => $fundList,
            'routeAction' => $routeAction,
            'model_name' => $model_name,
            'model_id' => $id,
            'fundMasterTypes' => $fundMasterTypes
        ]);
    }

    public function actionDetail(Request $request)
    {
        $this->validate($request, [
            'fund_id' => 'required',
            'title_vi' => 'required',
            'content_vi' => 'required',
            'title_en' => 'required',
            'content_en' => 'required',
            'number_value' => 'numeric'
            // 'img' => 'nullable|image'
        ], [
            'fund_id.required' => 'Quỹ đầu tư là bắt buộc',
            'title_vi.required' => 'Tiêu đề là bắt buộc',
            'content_vi.required' => 'Nội dung là bắt buộc',
            'title_en.required' => 'Tiêu đề là bắt buộc',
            'content_en.required' => 'Nội dung là bắt buộc',
            'number_value.numeric' => 'Giá trị phải là số',
            // 'img.image' => 'Ảnh tải lên không đúng định dạng'
        ]);

        $fundms = FundMaster::query()->findOrNew($request->id);
        // dd($fundms);
        $fundms->fund_id = $request->fund_id;
        $fundms->title = $request->title_vi;
        $fundms->title_en = $request->title_en ?? $request->title_vi;
        $fundms->content = $request->content_vi;
        $fundms->content_en = $request->content_en;
        $fundms->content_mobile = $request->content_mobile_vi;
        $fundms->content_mobile_en = $request->content_mobile_en;
        $fundms->type = $request->type;
        if($request->fund_id == 5 || $request->fund_id == 6){
            $fundms->type = 8;
        }

        // if ($request->hasFile('img')) {
        //     $file = $request->file('img');
        //     // $file->move('images', $file->hashName());
        //     $path = Storage::put('uploads/images', $file);
        //     $fundms->img = $path;
        // }
        if ($request['img']) {
            $file = $request['img']->store('uploads/images');
            if($file){
                if(strlen($fundms->img) && file_exists($fundms->img)){
                    unlink(public_path(). '/storage/'. $fundms->img);
                }
                $fundms->img =  $file;
            }
        }

        $fundms->url = $request->url;
        $fundms->v_key = $request->v_key_vi;
        $fundms->v_key_en = $request->v_key_en;
        $fundms->note = $request->note_vi;
        $fundms->note_en = $request->note_en;
        $fundms->number_value = $request->number_value;

        try {
            $message = $fundms->exists ? "Cập nhật thành công" : "Tạo mới thành công";
            $fundms->save();
            File::query()->where('model_name', FundMaster::class)->where('admin_id', auth()->id())->whereNull('model_id')->update([
                'model_id' => $fundms->id
            ]);

            Session::put("message", $message);
            return redirect()->route('admin.fundmaster');
        } catch (\Throwable $th) {
            Session::put("error", "Đã có lỗi xảy ra!");
            throw $th;
        }
    }

    public function destroy($id)
    {
        $fundmsd = FundMaster::find($id);
        $fundmsd->delete();
        return redirect()->back();
    }
}
