<?php

namespace App\Http\Controllers;

use App\Models\Fund;
use App\Models\FundNAV;
use Carbon\Carbon;
use Illuminate\Http\Request;

class FundNAVController extends Controller
{
    public function index()
    {
        return view('admin.fundnav.index');
    }

    public function create()
    {
        $fundid = Fund::all();
        return view('admin.fundnav.create',compact('fundid'));
    }

    public function store(Request $request)
    {
        $request->amount = str_replace(',','',$request->amount);
        $this->validate($request, [ 
            // 'fundid' => 'required', 
            'amount' => 'required|regex:/[0-9]$/', 
        ],[ 
            'fundid.required' => 'Tên quỹ đầu tư là bắt buộc', 
            'amount.required' => 'Số tiền là bắt buộc', 
            'amount.regex' => 'Số tiền không đúng định dạng', 
        ]); 
 
        if (empty($request->trading_session_time)) {
            $request->trading_session_time = now()->toDateString('Y-m-d');
        }
        if ($request->fundid) { 
            $pre = FundNAV::where('fund_id', $request->fundid)->where('trading_session_time', $request->trading_session_time)->first(); 
        } 
         
        if (isset($pre)) { 
            $pre->amount=$request->amount; 
            $this->updateFund($request); 
            $pre->save(); 
            return redirect()->route('admin.fundnav'); 
        } else { 
            $fundnav =new FundNAV(); 
            if ($request->fundid) { 
                $fundnav->fund_id = $request->fundid; 
                $this->updateFund($request); 
                $fundnav->type = '0'; // default 
            } else {  
                $fundnav->fund_id = null; 
                $fundnav->type = '1'; // VNINDEX 
            } 
            $fundnav->amount=$request->amount; 
            $fundnav->trading_session_time = $request->trading_session_time ?: Carbon::now(); 
            $fundnav->save(); 
            return redirect()->route('admin.fundnav')->with('success','Tạo mới thành công.'); 
        } 
    }

    public function edit($id)
    {
        $fundnav = FundNAV::findorfail($id);
        $fundid = Fund::all();
        return view('admin.fundnav.edit',compact('fundnav','fundid'));
    }

    public function update(Request $request)
    {
        $fundnav=new FundNAV();
        if ($request->fundid) {
            $fundnav->fund_id = $request->fundid;
            $this->updateFund($request);
            $fundnav->type = '0'; // default
        } else { 
            $fundnav->fund_id = null;
            $fundnav->type = '1'; // VNINDEX
        }
        $fundnav->amount=str_replace(',','',$request->amount);
        $fundnav->trading_session_time = $request->trading_session_time ?: Carbon::now();
        $rq=FundNAV::where('id',$request->id)->update($fundnav->toArray());
        if($rq)
        {
            return redirect()->route('admin.fundnav')->with('success','Cập nhật thành công');
        }
        return redirect()->route('admin.fundnav')->with('error','Cập nhật thành công');
    }

    public function updateFund(Request $request) {
        $last_day = FundNAV::where('fund_id', $request->fundid)->orderBy('trading_session_time','desc')->first();
        if ($last_day&&($request->trading_session_time >= $last_day->trading_session_time)) {
            $fund = Fund::findorfail($request->fundid);
            $getPreviousDayRecord=FundNAV::where('fund_id',$request->fundid)->orderBy('trading_session_time','desc')->first();
            $fund->growth = ($getPreviousDayRecord->amount) ? ((str_replace(',','',$request->amount) - $getPreviousDayRecord->amount)/($getPreviousDayRecord->amount))*100 : NULL;
            $fund->current_nav = str_replace(',','',$request->amount);
            $fund->save();
        }
        else {
            $fund = Fund::findorfail($request->fundid);
            $fund->current_nav = str_replace(',','',$request->amount);
            $fund->save();
        }
    }
}
