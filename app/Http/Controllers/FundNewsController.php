<?php

namespace App\Http\Controllers;

use App\Models\Fund;
use App\Models\FundNews;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;

class FundNewsController extends Controller
{
    //
    public function index()
    {
        return view('admin.fundnews.index');
    }

    public function create() {
        return view('admin.fundnews.create');
    }

    public function edit($id) {
        $data = FundNews::findorfail($id);
        $funds = Fund::all();
        return view('admin.fundnews.edit', compact('data', 'funds'));
    }

    public function update($id, Request $request){
        $request->validate(
            [
                'file-upload' => 'nullable|file|mimes:doc,docx,xlsx,jpg,png,pdf,xls,txt,zip,rar',
                'title_vi' => 'required',
                'title_en' => 'required',
                'type' => 'required',
                // 'url' =>  'required_if:type,5|regex:/^(?:http[s?]):\/\/(?:.*).(?:.*)$/u|nullable'
            ], [
                'file-upload.file' => 'Tệp tải lên phải là file',
                'file-upload.mimes' => 'Tệp tải lên không đúng định dạng',
                'title_vi.required' => 'Tiêu đề (Tiếng Việt) bắt buộc',
                'title_en.required' => 'Tiêu đề (Tiếng Anh) bắt buộc',
                'type.required' => 'Thể loại bắt buộc',
                // 'url.required' => 'Link bài viết là bắt buộc',
                // 'url.regex' => 'Link không đúng định dạng. Ví dụ: (https|http)://mbcapital.vn'
            ], []
        );

        $fundNew = FundNews::findOrFail($id);
        $data = [
            'title_vi'=> $request->title_vi,
            'title_en'=> $request->title_en,
            'content_vi'=> $request->content_vi,
            'content_en'=> $request->content_en,
            'type'=> $request->type,
            'url'=> $request->url,
            'fund_id'=> ($request->fund_id) ? $request->fund_id : null,
            'public_date'=> ($request->public_date) ? ($request->public_date) : now()
        ];
        // dd($request->remove_path,$request['file-upload'],$fundNew->file_path);
        if(isset($request->remove_path) && $request->remove_path && strpos($request->remove_path, $fundNew->file_path)!==false){
            if(file_exists(public_path(). '/storage/'. $fundNew->file_path)){
                unlink(public_path(). '/storage/'. $fundNew->file_path);
            }
            $data['file_path'] = null;
        }
        // dd($request->remove_path,$request['file-upload']);
        if ($request['file-upload']) {
            // dd($request['file-upload']);
            $file = $request['file-upload']->store('public');
            if($file){
                $data['file_path'] = $file;
                if(strlen($fundNew->file_path) && file_exists($fundNew->file_path)){
                    unlink(public_path(). '/storage/'. $fundNew->file_path);
                }
            }
        }

        // dd($data);
        $fundNew->update($data);
        return redirect()->route('admin.fundnews');
    }

    public function download($id){
        $fundNew = FundNews::findOrFail($id);
        return Storage::download($fundNew->file_path);
    }
}
