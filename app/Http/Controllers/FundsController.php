<?php

namespace App\Http\Controllers;

use App\Models\Fund;
use Illuminate\Http\Request;
use Session;

class FundsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $funds = Fund::Paginate(25);
        return view('admin.funds.index');
    }

    /**
     * Show the resource detail.
     * May be : show, create-form, edit-form
     * @return \Illuminate\Http\Response
    */
    public function detail($id = null)
    {
        // dd(route('admin.funds.store'));
        $fund = $id ? Fund::where('id', $id)->first() : null;
        $allfunds = $id ? Fund::query()->pluck('shortname', 'id')->toArray() : Fund::where('id', '!=', $id)->pluck('shortname', 'id')->toArray();
        $routeAction = $id ? route('admin.funds.update', ['id' => $id]) :route('admin.funds.store');
        return view('admin.funds.detail')->with(['fund' => $fund, 'routeAction'=> $routeAction, 'allfunds' => $allfunds]);
    }

    /**
     * Take action for resource.
     * Action : show, create, update
     * @return \Illuminate\Http\Response
     */
    public function actionDetail(Request $request)
    {
        if($request->current_nav) {
            $request->current_nav = str_replace(',','',$request->current_nav);
        }
        if($request->growth) {
            $request->growth = str_replace(',','',$request->growth);
        }
        $this->validate($request, [
            'shortname_vi' => 'required|max:191',
            'fullname_vi' => 'required|max:191',
            'growth' => 'numeric',
            'type' => 'required',
        ],
        [
            'shortname_vi.required' => 'Tên rút gọn là bắt buộc',
            'fullname_vi.required' => 'Tên đầy đủ là bắt buộc',
            'shortname_vi.max' => 'Tên rút gọn tối đa 191 kí tự',
            'fullname_vi.max' => 'Tên đầy đủ tối đa 191 kí tự',
            'current_nav.required' => 'Nav quỹ bắt buộc',
            'growth.required' => 'Tăng trưởng bắt buộc',
            'growth.numeric' => 'Tăng trưởng là số',
            'type.required' => 'Loại quỹ bắt buộc',
        ]);
        $fund = new Fund ();
        $fund->shortname = $request->shortname_vi;
        $fund->shortname_en = $request->shortname_en ?? $request->shortname_vi;
        $fund->fullname = $request->fullname_vi;
        $fund->fullname_en = $request->fullname_en ?? $request->fullname_vi;
        $fund->description = $request->description_vi;
        $fund->description_en = $request->description_en;
        $fund->content = $request->content_vi;
        $fund->content_en = $request->content_en;
        $fund->content_mobile = $request->content_mobile_vi;
        $fund->content_mobile_en = $request->content_mobile_en;
        $fund->public_date = $request->public_date_vi;
        $fund->public_date_en = $request->public_date_en;
        $fund->current_nav = $request->current_nav;
        $fund->growth = $request->growth;
        $fund->shortname2 = $request->shortname2_vi;
        $fund->shortname2_en = $request->shortname2_en ?? $request->shortname2_vi;
        $fund->fullname2 = $request->fullname2_vi;
        $fund->fullname2_en = $request->fullname2_en ?? $request->fullname2_vi;
        $fund->slug = $request->slug_vi;
        $fund->slug_en = $request->slug_en ?? $request->slug_vi;
        $fund->type = $request->type;
        $fund->priority = $request->priority;
        $fund->enable_performance = $request->enable_performance;
        $fund->enable_info_disclosure = $request->enable_info_disclosure;
        $fund->parent_id = $request->parent_id;
        $fund->created_by = auth()->id();
        $fund->active = $request->active?-1:null;
        $routeRedrict = redirect()->route('admin.funds');

        try {
            if ($request->id) {
                $updateFund = Fund::where('id', $request->id)->update($fund->toArray());
                if ($updateFund) {
                    Session::put("message","Cập nhật thông tin thành công");
                }
                else {
                    Session::put("error","Đã có lỗi xảy ra trong quá trình cập nhật");
                }
                return $routeRedrict;
            }
            else {
                $fund->save();
                Session::put("message","Tạo mới thành công");
            }
        } catch (\Throwable $th) {
            Session::put("error","Đã có lỗi xảy ra !");
            throw $th;
        }

        return $routeRedrict;
    }
}
