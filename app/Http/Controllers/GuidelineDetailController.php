<?php

namespace App\Http\Controllers;

use App\Models\Guideline;
use App\Models\GuidelineDetail;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Session;
class GuidelineDetailController extends Controller
{
    public function edit($id)
    {
        $data=GuidelineDetail::find($id);
        $guideid=$data->guideline_id;
        return view('guideline._form',compact('data', 'guideid'));
    }

    public function update($id,Request $request)
    {
       $guidt=new GuidelineDetail();
       $guidt->name=$request->name;
       $guidt->name_en=$request->name_en;
       $guidt->content=$request->content_vi;
       $guidt->content_en=$request->content_en;
       $guidt->content_mobile=$request->content_mobile;
       $guidt->content_mobile_en=$request->content_mobile_en;
       

       try {
        $query = GuidelineDetail::where('id', $request->id);
        $data = $query->first();
        $updateGuideDt = $query->update($guidt->toArray());
        if ($updateGuideDt) {
            Session::put("message","Cập nhật thông tin thành công");
        }
        else {
            Session::put("error","Đã có lỗi xảy ra trong quá trình cập nhật");
        }

        } catch (\Throwable $th) {
            Session::put("error","Đã có lỗi xảy ra !");
            throw $th;
        }

       return redirect()->route('guideline.edit',['id' => $data->guideline_id])->with('success','Cập nhật thành công.');
    }

    public function create(Request $request)
    {
        return view('guideline._form', ['guideid'=>$request->guideid]);
    }

    public function store(Request $request)
    {
        $id = $request->query('guideid');
        $this->validate($request, [
            'name' => 'required'
        ],[
            'name.required' => 'Tên bước hướng dẫn là bắt buộc'
        ]);
        $max_order = GuidelineDetail::where('guideline_id', $id)->max('order_number');
        $guide = new GuidelineDetail();
        $guide->name=$request->name;
        $guide->name_en=$request->name_en;
        $guide->guideline_id = $id;
        $guide->content = $request->content_vi;
        $guide->content_en = $request->content_en;
        $guide->content_mobile = $request->content_mobile;
        $guide->content_mobile_en = $request->content_mobile_en;
        $guide->order_number = $max_order + 1;
        $guide->created_at = Carbon::now();
        $guide->status = 1; // default
        $guide->save();

        return redirect()->route('guideline.detail',['id' => $id])->with('success','Tạo mới thành công.');
    }

    public function destroy($id)
    {
        $guide=GuidelineDetail::find($id);
        $guide->delete();
        return redirect()->back();
    }
}
