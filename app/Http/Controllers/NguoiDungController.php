<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class NguoiDungController extends Controller
{
    public function index(Request $request) {
        $query = User::query();
        if($request->UserName){
            $query->where('users.name','like','%'.trim($request->UserName).'%');
        }
        if($request->Email){
            $query->where('users.email','like','%'.trim($request->Email).'%');
        }
        $users = $query->get();
        return view('nguoi-dung.index', compact('users'));
    }


    public function create() {
        $roles = Role::pluck('name','id');
        return view('nguoi-dung.create', compact('roles'));
    }


    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required|min:8',
            'password_confirm'=> 'required_with:password|same:password',
        ], [
            'name.required' => 'Tên người dùng bắt buộc',
            'email.required' => 'Email bắt buộc',
            'email.unique' => 'Email đã tồn tại',
            'password.required' => 'Mật khẩu bắt buộc',
            'password.min' => 'Mật khẩu có ít nhất 8 ký tự',
            'password_confirm.same' => 'Mật khẩu xác nhận phải giống mật khẩu',
            'password_confirm.required_with' => 'Mật khẩu xác nhận bắt buộc',
        ], []);
        $user = User::create([
            'name' =>  $request->name,
            'email' => $request->email,
            'email_verified_at' => now(),
            'password' => bcrypt($request->password),
        ]);
        $user->assignRole($request->input('roles'));
        return redirect()->route('nguoiDung.index')->with('success','Thêm mới người dùng thành công');
    }


    // public function show($slug) {
    //     $data = $this->product->getProductBySlug($slug, true);
    //     return view('products.show', $data);
    // }

    public function edit($id) {
        $data = User::find($id);
        $roles = Role::pluck('name','id')->toArray();
        $rolesUser = DB::table('model_has_roles')->where('model_id',$id)->pluck('role_id')->toArray();
        return view('nguoi-dung.edit', compact('data','roles','rolesUser'));
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required',
            'password_new' => 'nullable|min:8',
        ], [
            'name.required' => 'Tên người dùng bắt buộc',
            // 'password_new.required' => 'Mật khẩu mới bắt buộc',
            'password_new.min' => 'Mật khẩu mới có nhất 8 ký tự'
        ], []);
        $passwordOld = User::find($id)->password;
        $roleUser=User::where('id',$id)->update([
            'name' =>  $request->name,
            'password' => $request->password_new?bcrypt($request->password_new):$passwordOld,
        ]);
        
        $roleUser=User::find($id);
        $roleUser->syncRoles($request->roles);
        return redirect()->route('nguoiDung.index')->with('success','Chỉnh sửa người dùng thành công');
    }

    public function updateRole(Request $request)
    {
        $user = User::find($request->id);
        if(!empty($request->role_name)) {
            $user->syncRoles($request->role_name);
        }
        else {
            DB::table('model_has_roles')->where('model_id',$request->id)->delete();
        }

        return back()->with('success','Cập nhật quyền thành công');
    }
}
