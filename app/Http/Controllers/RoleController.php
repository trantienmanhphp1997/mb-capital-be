<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Permisson;
use App\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\RoleHasPermisson;

class RoleController extends Controller
{
    public function index() {
        return view('role.index');
    }
    
    public function create() {
        $rolePermissions = Permission::pluck('name','id')->toArray();
        $permissions = [];

        return view('role.create', compact('rolePermissions', 'permissions'));
    }

    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'permissions' => 'required',
        ], [
            'name.required' => 'Tên vai trò bắt buộc',
            'permissions.required' => 'Quyền bắt buộc',
        ], []);
        $role = Role::create([
            'name' => $request->name,
            'guard_name' => 'web',
        ]);
        // dd($request->permissions);
        if (!empty($request->permissions)) {
            foreach ($request->permissions as $val) {
                RoleHasPermisson::create([
                    'role_id' => $role->id,
                    'permission_id' => $val,
                ]);
            }
        }

        return redirect()->route('roles.index')->with('success','Thêm mới vai trò thành công');
    }

    public function edit($id) {
        $data = Role::find($id);
        $rolePermissions = Permission::pluck('name','id')->toArray();
        $permissions = $this->getRolePermissions($id);

        return view('role.edit', compact('data', 'permissions', 'rolePermissions'));
    }

    public function update($id, Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'permissions' => 'required',
        ], [
            'name.required' => 'Tên vai trò bắt buộc',
            'permissions.required' => 'Quyền bắt buộc',
        ], []);
        Role::findorfail($id)->update([
            'name' => $request->name,
        ]);
        // dd($request);
        if (!empty($request->permissions)) {
            foreach ($request->permissions as $val) {
                RoleHasPermisson::where('role_id', $id)->delete();
            }
            foreach ($request->permissions as $val) {
                RoleHasPermisson::create([
                    'role_id' => $id,
                    'permission_id' => $val,
                ]);
            }
        }

        return redirect()->route('roles.index')->with('success','Cập nhật vai trò thành công');
    }

    function getRolePermissions ($idRole) {
        $rolePermissions = RoleHasPermisson::where('role_has_permissions.role_id', $idRole)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();

        $dataPermission = [];
        foreach ($rolePermissions as $permission) {
            $dataPermission[] = $permission;
        }

        return $dataPermission;
    }
}
