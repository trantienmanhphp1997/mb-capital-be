<?php

namespace App\Http\Controllers;

use App\Models\Survey;
use Carbon\Carbon;
use Illuminate\Http\Request;
class SurveyController extends Controller
{
    public function index()
    {
        return view('survey.index');
    }

    // public function edit($id)
    // {
    //     $data=Guideline::findOrFail($id)->load('details');
    //     return view('guideline.edit',compact('data', 'id'));
    // }

    public function detail($id) {
        $title = Survey::findOrFail($id)->name;
        return view('survey.detail', [
            'id' => $id,
            'title' => $title,
        ]);
    }
    public function log_survey(){
        return view('survey.log_survey.index');
    }
    // public function update($id,Request $request)
    // {
    //     $guideUpdate=Guideline::findOrFail($id);

    //     $guideUpdate->update();
    // }

    // public function create($guide_id)
    // {
    //     return view('guideline.edit', ['id'=>$guide_id]);
    // }

    // public function store(Request $request)
    // {
    //     $this->validate($request, [
    //         'fundid' => 'required',
    //         'amount' => 'required|regex:/[0-9]$/',
    //     ],[
    //         'fundid.required' => 'Tên quỹ đầu tư là bắt buộc',
    //         'amount.required' => 'Số tiền là bắt buộc',
    //         'amount.regex' => 'Số tiền không đúng định dạng',
    //     ]);

    //     $guide = new Guideline();
    //     $guide->fund_id=$request->fundid;
    //     $guide->amount=$request->amount;
    //     $guide->trading_session_time = $request->trading_session_time ?: Carbon::now();
    //     $guide->type = '0'; // default
    //     $guide->save();

    //     return redirect()->route('admin.fundnav')->with('success','Tạo mới thành công.');
    // }

    // public function destroy($id)
    // {
    //     $guide=Guideline::find($id);
    //     $guide->delete();
    //     return redirect()->back();
    // }
}
