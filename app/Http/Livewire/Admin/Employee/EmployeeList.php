<?php

namespace App\Http\Livewire\Admin\Employee;

use App\Http\Livewire\Base\BaseLive;
use App\Models\Employee;
use App\Models\Fund;
use Livewire\Component;
use App\Enums\ECommon;

class EmployeeList extends BaseLive
{
    public $searchName;
    public $searchType;
    public $searchFund;
    public $roles;
    public $dataSortable;
    public function render()
    {
        $query = Employee::query();
        if ($this->searchName) {
            $query->where('fullname', 'like', '%' . trim($this->searchName) . '%');
        }
        if ($this->searchType) {
            $query->where('type', $this->searchType);
        }
        if (!empty($this->searchFund)) {
            if($this->searchFund == -1) {
                $query->whereNull('fund_id');
            } else {
                $query->where('fund_id', $this->searchFund);
            }
        }

        $this->roles = [
            '1' => ECommon::BOARD_OF_DIRECTORS,
            '2' => ECommon::CONTROL_BOARD,
            '3' => ECommon::DIRECTOR_AND_DEPUTY_DIRECTOR,
        ];

        $funds = Fund::all();
        $data = $query->orderBy('priority', 'asc')->paginate($this->perPage);
        if($data->total()>0){
            $arrSortable = $data->toArray();
            $this->dataSortable=$arrSortable['data'];
        }
        return view('livewire.admin.employee.employee-list', [
            'data' => $data,
            'funds' => $funds
        ]);
    }

    public function delete() {
        Employee::findOrFail($this->deleteId)->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa thành công." ]);
    }
    public function updateOrder($list){
        if($this->searchType){
            foreach ($this->dataSortable as $key_o=> $value_o){
                foreach ($list as $key_n=> $value_n) {
                    if($key_o==$key_n){
                        Employee::where('id',$value_o['id'])->update(['priority'=>$value_n['order']]);
                    }
                }
            }
        }
    }
}
