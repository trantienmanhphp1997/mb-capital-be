<?php

namespace App\Http\Livewire\Admin\Faq;
use App\Http\Livewire\Base\BaseLive;
use App\Enums\EFaqType;
use App\Models\Faq;
use App\Models\Fund;


class FaqList extends BaseLive
{
    public $filterFundId = -1;
    public $isEdit = false;
    public $faq;

    //data for create
    public $question;
    public $question_en;
    public $content;
    public $content_en;
    public $type;
    public $fund_id;

    protected $listeners = [
        'set-content' => 'setContent',
    ];

    public function setContent($content, $content_en) {
        $this->content = $content;
        $this->content_en = $content_en;
        $this->store();
    }
    
    public function render() {
        $query = Faq::query();
        if(!empty($this->filterFundId)) {
            if($this->filterFundId != -1) {
                $query->where('fund_id',$this->filterFundId);
            }
        } else {
            $query->where('type', EFaqType::FREQUENT);
        }
        if($this->searchTerm) {
            $searchTerm = $this->searchTerm;
            $query->where(function($where) use ($searchTerm) {
                $where->whereRaw('lower(content) like ? ', ['%' . trim(mb_strtolower($searchTerm, 'UTF-8')) . '%'])
                    ->orWhereRaw('lower(content_en) like ? ', ['%' . trim(mb_strtolower($searchTerm, 'UTF-8')) . '%'])
                    ->orWhereRaw('lower(question) like ? ', ['%' . trim(mb_strtolower($searchTerm, 'UTF-8')) . '%'])
                    ->orWhereRaw('lower(question_en) like ? ', ['%' . trim(mb_strtolower($searchTerm, 'UTF-8')) . '%']);
                });
        }
        $data = $query->with('fund')->orderBy('id','DESC')->paginate($this->perPage);
        $listFund = Fund::query()->whereNull('parent_id')->orderBy('priority')->get();
        return view('livewire.admin.faq.faq-list', ['data' => $data, 'listFund' => $listFund]);
    }
    
    public function resetform() {
        $this->reset(['question', 'question_en','content','content_en','type', 'fund_id', 'isEdit', 'faq']);
        $this->emit('resetContent');
    }

    public function edit($id) {
        $faq = Faq::where('id', $id)->first();
        $this->faq= $faq;
        $this->isEdit = true;
        $this->question = $faq->question;
        $this->question_en = $faq->question_en;
        $this->content = $faq->content;
        $this->content_en = $faq->content_en;
        $this->type = $faq->type;
        $this->fund_id = $faq->fund_id;
        $this->resetValidation();
        $this->emit('setContentEdit', $faq->content, $faq->content_en);
    }
    public function store() {
        $this->validate([
            'question' => 'required',
            'question_en' => 'required',
            // 'content' => 'required',
            // 'content_en' => 'required',
            'type' => 'required',
        ], [
            'question.required' => 'Câu hỏi(VI) bắt buộc',
            'question_en.required' => 'Câu hỏi(EN) bắt buộc',
            // 'content' => 'câu trả lời(VI)',
            // 'content_en' => 'câu trả lời(EN)',
            'type.required' => 'Loại câu hỏi bắt buộc',
        ]);
        if(is_null($this->faq)) {
            $this->faq = new Faq();
        }
        $this->faq->question = $this->question;
        $this->faq->question_en = $this->question_en;
        $this->faq->content = $this->content;
        $this->faq->content_en = $this->content_en;
        $this->faq->type = $this->type;
        $this->faq->fund_id = $this->fund_id;

        $this->faq->save();
        if($this->isEdit) {
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.edit')] );
        } else {
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.add')] );
        }
        $this->emit('closeModal');
    }

    public function delete()
    {
        Faq::findOrFail($this->deleteId)->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')] );
    }

    public function updatingFilterFundId() {
        $this->resetPage();
    }
}