<?php

namespace App\Http\Livewire\Admin\FundEmployeeXRF;

use Livewire\Component;
use App\Http\Livewire\Base\BaseLive;
use App\Models\FundEmployeeXRF;
use Excel;
use App\Exports\FundEmployeeXRFExport;
use App\Models\Employee;
use App\Models\Fund;

class Index extends BaseLive {

    public $mode = 'create';
    public $editId;
    public $deleteId;
    public $key_name='id', $sortingName='desc';
    public $employee_id;
    public $fund_id;
    public $role_name;
    public $funds;
    public $employees;
    public $title_vi, $title_en;

    protected $rules = [
    ];


    public $search;

    public function mount() {
        $this->funds = Fund::all();
        $this->employees = Employee::query()->whereNotIn('id', FundEmployeeXRF::all()->pluck('employee_id')->toArray())->get();
    }   

    public function render(){
        $query = FundEmployeeXRF::query()
            ->leftjoin('employee', 'employee.id', '=', 'fun_employee_xrf.employee_id')
            ->leftjoin('fund', 'fund.id', '=', 'fun_employee_xrf.fund_id')
            ->select('fun_employee_xrf.id', 'employee.fullname as employee_name', 'employee.id as employee_id',
                'fund.fullname as fund_name', 'fund.id as fund_id',
                'employee.title', 'employee.title_en');
        if($this->search) {
        }

        $data = $query->orderBy($this->key_name,$this->sortingName)->paginate($this->perPage);
        return view('livewire.admin.FundEmployeeXRF.index', [
            'data'=> $data,
        ]);
    }

    public function updatedSearch(){
        $this->resetPage();
    }

    public function resetValidate(){
        
        $this->resetValidation();
    }

    public function create (){
        $this->mode = 'create';
        $this->reset(['employee_id','fund_id','title_vi','title_en']);
        $this->employees = Employee::query()->whereNotIn('id', FundEmployeeXRF::all()->pluck('employee_id')->toArray())->get();
    }

    public function saveData (){
        $this->standardData();
        // $this->validate();
        if($this->mode=='create'){
            FundEmployeeXRF::create([
                'employee_id' => $this->employee_id,
                'fund_id' => $this->fund_id,
                'role_name' => $this->title_vi,
            ]);

        }
        else {
            FundEmployeeXRF::where("id",$this->editId)->update([
                // 'employee_id' => $this->employee_id,
                'fund_id' => $this->fund_id,
                // 'role_name' => $this->title_vi,
            ]);

        }
        $this->resetValidate();
        if($this->mode=='create'){
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => 'Thêm mới thành công']);
        }
        else {
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => 'Chỉnh sửa thành công']);
        }
        $this->emit('closeModalCreateEdit');
    }

    public function edit($row){
        $this->mode = 'update';
        $this->employees = Employee::where('id', $row['employee_id'])->get();
        $this->editId = $row['id'];
        $this->employee_id = $row['employee_id'];
        $this->fund_id = $row['fund_id'];
        $this->title_vi = $row['title'];
        $this->title_en = $row['title_en'];
    }

    public function standardData(){
        
    }

    public function delete(){
        FundEmployeeXRF::find($this->deleteId)->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => 'Xóa thành công']);
    }

    public function resetSearch(){
        $this->search = "";
        $this->reset('key_name');
        $this->reset('sortingName');
    }

    public function sorting($key){
        if($this->key_name == $key){
            $this->sortingName = $this->getSortName();
        } else {
            $this->sortingName ='desc';
        }
        $this->key_name = $key;
    }
    public function getSortName(){
        return $this->sortingName == "desc" ? "asc" : "desc";
    }
    
    public function updatedEmployeeId() {
        $employee = Employee::find($this->employee_id);
        $this->title_vi = $employee->title;
        $this->title_en = $employee->title_en;
    }
}
