<?php

namespace App\Http\Livewire\Admin\FundExpectedProfit;

use Livewire\Component;
use App\Http\Livewire\Base\BaseLive;
use App\Models\FundExpectedProfit;
use Excel;
use App\Exports\FundExpectedProfitExport;
use App\Models\Fund;

class Index extends BaseLive {

    public $mode = 'create';
    public $editId;
    public $deleteId;
    public $key_name='id', $sortingName='desc';
    public $fundList;
    public $fund_id, $period, $period_name, $period_name_en, $percent, $percent_view;
    
    protected $rules = [
        'period' => 'required',
        'period_name' => 'required',
        'period_name_en' => 'required',
        'percent' => 'required',
    ];

    public function mount() {
        $this->fundList = Fund::all();
    }

    public $search;

    public function render(){
        $query = FundExpectedProfit::query();
        if($this->search) {
            $query = $query->where(function ($query) {
                $query->where("period_name", "like", "%".$this->search."%");
            });
        }

        $data = $query->orderBy($this->key_name,$this->sortingName)->paginate($this->perPage);
        return view('livewire.admin.FundExpectedProfit.index', [
            'data'=> $data,
        ]);
    }

    public function updatedSearch(){
        $this->resetPage();
    }

    public function resetValidate(){
        
        $this->resetValidation();
    }

    public function create (){
        $this->resetInputFields();
        $this->resetValidate();
        $this->mode = 'create';
    }

    public function saveData (){
        $this->standardData();
        $this->validate();
        if($this->mode=='create'){
            FundExpectedProfit::create([
                'fund_id' => $this->fund_id==0?3:$this->fund_id, // default gửi tiết kiệm là 3
                'type' => $this->fund_id==0?0:1, // 0 là gửi tiết kiệm, 1 là gửi quỹ
                'period' => $this->period,
                'period_name' => $this->period_name,
                'period_name_en' => $this->period_name_en,
                'percent' => $this->percent,
                'percent_view' => $this->percent_view,
            ]);

        }
        else {
            FundExpectedProfit::where("id",$this->editId)->update([
                'fund_id' => $this->fund_id==0?3:$this->fund_id, // default gửi tiết kiệm là 3
                'type' => $this->fund_id==0?0:1, // 0 là gửi tiết kiệm, 1 là gửi quỹ
                'period' => $this->period,
                'period_name' => $this->period_name,
                'period_name_en' => $this->period_name_en,
                'percent' => $this->percent,
                'percent_view' => $this->percent_view,
            ]);

        }
        $this->resetValidate();
        if($this->mode=='create'){
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => 'Thêm mới thành công']);
        }
        else {
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => 'Chỉnh sửa thành công']);
        }
        $this->emit('closeModalCreateEdit');
    }

    public function edit($row){
        $this->resetInputFields();
        $this->resetValidate();
        $this->mode = 'update';
        $this->editId = $row['id'];
        $this->fund_id = $row['type']==0?0:$row['fund_id'];
        $this->period = $row['period'];
        $this->period_name = $row['period_name'];
        $this->period_name_en = $row['period_name_en'];
        $this->percent = $row['percent'];
        $this->percent_view = $row['percent_view'];
    }

    public function standardData(){
        
    }

    public function delete(){
        FundExpectedProfit::find($this->deleteId)->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => 'Xóa thành công']);
    }

    public function resetSearch(){
        $this->search = "";
        $this->reset('key_name');
        $this->reset('sortingName');
    }

    public function export(){
        $today = date("d_m_Y");
        return Excel::download(new FundExpectedProfitExport($this->key_name, $this->sortingName, $this->search), "FundExpectedProfit-export-".$today.".xlsx");
    }

    public function sorting($key){
        if($this->key_name == $key){
            $this->sortingName = $this->getSortName();
        } else {
            $this->sortingName ='desc';
        }
        $this->key_name = $key;
    }
    public function getSortName(){
        return $this->sortingName == "desc" ? "asc" : "desc";
    }
    
    public function resetInputFields() {
        $this->reset([
            'fund_id',
            'period',
            'period_name',
            'period_name_en',
            'percent',
            'percent_view',
        ]);
    }

}
