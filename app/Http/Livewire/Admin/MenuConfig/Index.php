<?php

namespace App\Http\Livewire\Admin\MenuConfig;

use Livewire\Component;
use App\Http\Livewire\Base\BaseLive;
use App\Models\MenuConfig;
use App\Models\Menu;
use App\ENums\EMenuConfigType;
use DB;

class Index extends BaseLive {

    public $name;
    public $name_en;
    public $type;
    public $menu_id;
    public $parent_id;
    public $id_select;

    public function render() {
        $data = MenuConfig::query();
        $data = $data->paginate($this->perPage);
        foreach($data as $item) {
            if (!empty($item->parent_id)) {
                $item->parent_name = MenuConfig::find($item->parent_id)->name;
            } else {
                $item->parent_name = null;
            }
        }
        return view('livewire.admin.menu-config.index', [
            'data' => $data,
            'menuList' => Menu::get(),
        ]);
    }

    public function saveData() {
        return DB::transaction(function() {
            if (!empty($this->id_select)) {
                $menuConfig = MenuConfig::find($this->id_select);
            } else {
                $menuConfig = new MenuConfig();
            }
            $menuConfig->name = $this->name;
            $menuConfig->name_en = $this->name_en;
            $menuConfig->type = $this->type;
            $menuConfig->menu_id = $this->menu_id;
            $menuConfig->parent_id = $this->parent_id;
            $menuConfig->admin_id = auth()->id();
            if (!empty($this->parent_id)) {
                $parent = MenuConfig::find($this->parent_id);
                $menuConfig->level = $parent->level + 1;
            } else {
                $menuConfig->level = 1;
            }
            $menuConfig->save();
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('common.message.save-success')]);
            $this->emit('closeModal');
        });
    }

    public function edit($item) {
        $this->name = $item['name'];
        $this->name_en = $item['name_en'];
        $this->type = $item['type'];
        $this->menu_id = $item['menu_id'];
        $this->parent_id = $item['parent_id'];
        $this->id_select = $item['id'];
    }

    public function delete() {
        $menuConfig = MenuConfig::findOrFail($this->deleteId);
        if (!empty($menuConfig)) {
            $menuConfig->delete();
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')]);
        }
    }
}
