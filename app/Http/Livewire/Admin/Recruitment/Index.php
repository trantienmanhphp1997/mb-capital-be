<?php

namespace App\Http\Livewire\Admin\Recruitment;

use Livewire\Component;
use App\Http\Livewire\Base\BaseLive;
use App\Models\Recruitment;
use DB;
use Livewire\WithFileUploads;
use App\Helpers;

class Index extends BaseLive {

    use WithFileUploads;
    public $editId;
    public $content;
    public $content_en;
    public $short_content;
    public $short_content_en;
    public $title;
    public $title_en;
    public $date_submit;
    public $file;
    public $position;
    public $job;
    public $skill;
    public $language;
    public $position_en;
    public $job_en;
    public $skill_en;
    public $language_en;
    public $url;
    public $canEdit;
    protected $listeners = [
        'set-content' => 'setContent',
    ];
    public function setContent($content, $content_en, $short_content, $short_content_en) {
        $this->content = $content;
        $this->content_en = $content_en;
        $this->short_content = $short_content;
        $this->short_content_en = $short_content_en;
        $this->saveData();
    }
    public function render() {
        $query=Recruitment::query();

        if ($this->searchTerm) {
            $query->where('title', 'like', '%' . trim($this->searchTerm) . '%')
                  ->orwhere('title_en', 'like', '%' . trim($this->searchTerm) . '%')
                  ->orwhere('content', 'like', '%' . trim($this->searchTerm) . '%')
                  ->orwhere('content_en', 'like', '%' . trim($this->searchTerm) . '%');
        }
        $data=$query->orderBy('recruitments.id','desc')->paginate($this->perPage);
        return view('livewire.recruitment.index',['data'=> $data]);
    }

    public function delete(){
        Recruitment::find($this->deleteId)->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => 'Xóa danh mục tuyển dụng thành công']);
    }

    public function edit($row){
        $this->editId = $row['id'];
        $this->content = $row['content'];
        $this->content_en = $row['content_en'];
        $this->short_content = $row['short_content'];
        $this->short_content_en = $row['short_content_en'];
        $this->title = $row['title'];
        $this->title_en = $row['title_en'];
        $this->url = $row['url'];
        $this->date_submit = $row['date_submit'];
        $this->position = $row['position'];
        $this->job = $row['job'];
        $this->skill = $row['skill'];
        $this->language = $row['language'];
        $this->position_en = $row['position_en'];
        $this->job_en = $row['job_en'];
        $this->skill_en = $row['skill_en'];
        $this->language_en = $row['language_en'];
        $this->canEdit = true;
        $this->emit('setContentEdit', $this->content, $this->content_en, $this->short_content, $this->short_content_en);
    }
    public function create(){
        $this->resetData();
        $this->canEdit = false;
    }

    public function saveData(){
        $this->trimData();
        $this->validateData();
        if($this->canEdit){
            Recruitment::find($this->editId)->update([
                'title' => $this->title,
                'title_en' => $this->title_en,
                'content' => $this->content,
                'content_en' => $this->content_en,
                'short_content' => $this->short_content,
                'short_content_en' => $this->short_content_en,
                'url' => $this->url,
                'date_submit' => $this->date_submit,
                'file_path' => ($this->file) ? $this->file->store('public') : null,
                'position' => $this->position,
                'job' => $this->job,
                'skill' => $this->skill,
                'language' => $this->language,
                'position_en' => $this->position_en,
                'job_en' => $this->job_en,
                'skill_en' => $this->skill_en,
                'language_en' => $this->language_en,
                'slug' => Helpers\Slug::slugify($this->title),
                'slug_en' => Helpers\Slug::slugify($this->title_en),
            ]);
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => 'Cập nhật danh mục tuyển dụng thành công']);
        }
        else {
            Recruitment::create([
                'title' => $this->title,
                'title_en' => $this->title_en,
                'content' => $this->content,
                'content_en' => $this->content_en,
                'short_content' => $this->short_content,
                'short_content_en' => $this->short_content_en,
                'url' => $this->url,
                'date_submit' => $this->date_submit,
                'file_path' => ($this->file) ? $this->file->store('public') : null,
                'position' => $this->position,
                'job' => $this->job,
                'skill' => $this->skill,
                'language' => $this->language,
                'position_en' => $this->position_en,
                'job_en' => $this->job_en,
                'skill_en' => $this->skill_en,
                'language_en' => $this->language_en,
                'slug' => Helpers\Slug::slugify($this->title),
                'slug_en' => Helpers\Slug::slugify($this->title_en),
            ]);
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => 'Thêm mới danh mục tuyển dụng thành công']);           
        }
        $this->emit('closeModalCreateEdit');
    }
    public function validateData(){
        $this->validate([
            'content' => 'required|max:5000',
            'content_en' => 'max:5000',
            'title' => 'required|max:1000',
            'title_en' => 'max:1000',
            'url' => 'required|max:1000',
        ],[
            'content.required' => 'Nội dung tuyển dụng bắt buộc',
            'content.max' => 'Nội dung tuyển dụng không được quá 5000 ký tự',
            'content_en.max' => 'Nội dung tuyển dụng tiếng anh không được quá 5000 ký tự',
            'title.required' => 'Tiêu đề tuyển dụng bắt buộc',
            'title.max' => 'Tiêu đề tuyển dụng không được quá 1000 ký tự',
            'title_en.max' => 'Tiêu đề tuyển dụng tiếng anh không được quá 1000 ký tự',
            'url.required' => 'Đường dẫn bắt buộc',
            'url.max' => 'Đường dẫn không được quá 1000 ký tự',
        ],[]);
    }
    public function trimData(){
        $this->content = trim($this->content);
        $this->content_en = trim($this->content_en);
        $this->short_content = trim($this->short_content);
        $this->short_content_en = trim($this->short_content_en);
        $this->title = trim($this->title);
        $this->title_en = trim($this->title_en);
        $this->url = trim($this->url);
        $this->position = trim($this->position);
        $this->job = trim($this->job);
        $this->skill = trim($this->skill);
        $this->language = trim($this->language);
        $this->position_en = trim($this->position_en);
        $this->job_en = trim($this->job_en);
        $this->skill_en = trim($this->skill_en);
        $this->language_en = trim($this->language_en);
    }
    public function resetData(){
        $this->content = null;
        $this->content_en = null;
        $this->short_content = null;
        $this->short_content_en = null;
        $this->title = null;
        $this->title_en = null;
        $this->url = null;
        $this->file = null;
        $this->date_submit = null;
        $this->position = null;
        $this->job = null;
        $this->skill = null;
        $this->language = null;
        $this->position_en = null;
        $this->job_en = null;
        $this->skill_en = null;
        $this->language_en = null;
        $this->emit('setContent');
    }
    public function resetValidate(){
        $this->resetValidation();
    }
}
