<?php

namespace App\Http\Livewire\Admin\SurveyResponse;

use Livewire\Component;
use App\Http\Livewire\Base\BaseLive;
use App\Models\SurveyResponse;
use Excel;
use App\Exports\SurveyResponseExport;


class Index extends BaseLive {

    public $mode = 'create';
    public $editId;
    public $deleteId;
    public $key_name='id', $sortingName='desc';
    public $fromDate, $toDate;
    

    protected $rules = [
    ];


    public $search;
    

    public function render(){
        $query = SurveyResponse::query();
        if($this->search) {
            $query = $query->where(function ($query) {
                $query->where("sum_point", "like", "%".$this->search."%")->orWhere('rate_level_risk', 'like', '%'.$this->search.'%');
            });
        }

        if ($this->fromDate && $this->toDate) {
            $query = $query->where(function ($query) {
                $query->whereDate('request_date','>=',date('Y-m-d', strtotime($this->fromDate)))->whereDate('request_date','<=',date('Y-m-d', strtotime($this->toDate)));
            });
        }

        $data = $query->orderBy($this->key_name,$this->sortingName)->paginate($this->perPage);
        return view('livewire.admin.SurveyResponse.index', [
            'data'=> $data,
        ]);
    }

    public function updatedSearch(){
        $this->resetPage();
    }

    public function resetValidate(){
        
        $this->resetValidation();
    }

    public function create (){
        $this->mode = 'create';
    }

    public function saveData (){
        $this->standardData();
        $this->validate();
        if($this->mode=='create'){
            SurveyResponse::create([
            ]);

        }
        else {
            SurveyResponse::where("id",$this->editId)->update([
            ]);

        }
        $this->resetValidate();
        if($this->mode=='create'){
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => 'Thêm mới thành công']);
        }
        else {
            $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => 'Chỉnh sửa thành công']);
        }
        $this->emit('closeModalCreateEdit');
    }

    public function edit($row){
        $this->mode = 'update';
        $this->editId = $row['id'];
        
    }

    public function standardData(){
        
    }

    public function delete(){
        SurveyResponse::find($this->deleteId)->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => 'Xóa thành công']);
    }

    public function resetSearch(){
        dd ($this);
        $this->search = "";
        $this->fromDate = "";
        $this->toDate = "";
        $this->reset('key_name');
        $this->reset('sortingName');
    }

    public function export(){
        $today = date("d_m_Y");
        return Excel::download(new SurveyResponseExport($this->key_name, $this->sortingName, $this->search, $this->fromDate, $this->toDate), "SurveyResponse-export-".$today.".xlsx");
    }

    public function sorting($key){
        if($this->key_name == $key){
            $this->sortingName = $this->getSortName();
        } else {
            $this->sortingName ='desc';
        }
        $this->key_name = $key;
    }
    public function getSortName(){
        return $this->sortingName == "desc" ? "asc" : "desc";
    }
    
}
