<?php

namespace App\Http\Livewire\Advise;

use App\Http\Livewire\Base\BaseLive;
use App\Models\Advise;
use Livewire\Component;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB as FacadesDB;

class AdviseList extends BaseLive
{
    public $searchName;
    public $searchEmailOrPhone;
    public $searchContent;
    public $searchIP;

    public function render()
    {
        if ($this->reset) {
            $this->searchName = null;
            $this->searchEmailOrPhone = null;
            $this->searchContent =null;
            $this->searchIP=null;
        }

        $searchName=trim($this->searchName);
        $searchEmailOrPhone=trim($this->searchEmailOrPhone);
        $searchContent=trim($this->searchContent);
        $searchIP=trim($this->searchIP);

        $query=Advise::query();

        if($searchName)
        {
            $query->where('name','like','%'.$searchName.'%');
        }

        if($searchEmailOrPhone)
        {
            $query->where('email_phone','like','%'.$searchEmailOrPhone.'%');
        }
        
        if($searchContent)
        {
            $query->where('advise_content','like','%'.$searchContent.'%');
        }

        if($searchIP)
        {
            $query->where('IP','like','%'.$searchIP.'%');
        }

        $data=$query->orderBy('advise_list.id','desc')->paginate($this->perPage);
        return view('livewire.advise.advise-list',compact('data'));
    }

    public function updatedSearchName(){
        $this->resetPage();
    }
    
    public function updatedSearchEmailOrPhone(){
        $this->resetPage();
    }
    public function updatedSearchContent(){
        $this->resetPage();
    }
    public function updatedSearchIP(){
        $this->resetPage();
    }

    public function resetSearch()
    {
        $this->searchName="";
        $this->searchEmailOrPhone="";
        $this->searchContent="";
        $this->searchIP="";
    }
}
