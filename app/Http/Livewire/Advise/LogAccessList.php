<?php

namespace App\Http\Livewire\Advise;

use App\Http\Livewire\Base\BaseLive;
use App\Models\LogAccess;
use Livewire\Component;

class LogAccessList extends BaseLive
{
    public $search;
    public function render()
    {
        if($this->reset)
        {
            $this->search=null;
        }

        $search=trim($this->search);
        $query=LogAccess::query();
        if($search)
        {
            $query->where('ip_address','like','%'.$search.'%')
            ->orWhere('url_previous','like','%'.$search.'%')
            ->orWhere('device','like','%'.$search.'%')
            ->orWhere('browser','like','%'.$search.'%')
            ->orWhere('cookies','like','%'.$search.'%')
            ->orWhere('note','like','%'.$search.'%')
            ->orWhere('user_agent','like','%'.$search.'%');
        }

        $data=$query->orderBy('id','desc')->paginate($this->perPage);

        return view('livewire.advise.log-access-list',compact('data'));
    }

    public function updatedSearch(){
        $this->resetPage();
    }
}
