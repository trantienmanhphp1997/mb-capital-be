<?php

namespace App\Http\Livewire\Fund;

use App\Http\Livewire\Base\BaseLive;
use Livewire\Component;
use App\Models\Fund;

class FundList extends BaseLive
{
  
        public $search;
        public function render()
        {
            if($this->reset)
            {
                $this->search=null;
            }
            $query=Fund::query();
            if($this->search)
            {
                $query->where('shortname','like','%'.$this->search.'%')
                ->orwhere('fullname','like','%'.$this->search.'%')
                ->orwhere('description','like','%'.$this->search.'%')
                ->orwhere('content','like','%'.$this->search.'%');
            }
            $data=$query->orderBy('id','asc')->paginate($this->perPage);
            return view('livewire.fund.fund-list',compact('data'));
        }
}

