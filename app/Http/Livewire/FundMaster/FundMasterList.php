<?php

namespace App\Http\Livewire\FundMaster;

use App\Http\Livewire\Base\BaseLive;
use App\Models\FundMaster;
use App\Enums\EFundMaster;
use App\Models\Fund;
use Livewire\Component;

class FundMasterList extends BaseLive
{

    public $searchTerm;
    public $typeFilter;
    public $typeFilterFund;
    public $vkeyFilter;
    public $dataSortable;

    public function render()
    {
        if ($this->reset) {
            $this->searchTerm = null;
        }


        $query=FundMaster::query();
        $query
        ->leftjoin('fund','fund_master.fund_id','=','fund.id')
        ->select('fund_master.*','fund.shortname as fund_name');
        if($this->searchTerm)
        {
            $query->where('fund_master.title','like','%'.$this->searchTerm.'%')
            ->orWhere('fund_master.content','like','%'.$this->searchTerm.'%')
            ->orWhere('fund_master.content_en','like','%'.$this->searchTerm.'%')
            ->orWhere('fund_master.title','like','%'.$this->searchTerm.'%')
            ->orWhere('fund_master.title_en','like','%'.$this->searchTerm.'%')
            ->orWhere('fund_master.url','like','%'.$this->searchTerm.'%')
            ->orWhere('fund_master.note','like','%'.$this->searchTerm.'%')
            ->orWhere('fund_master.note_en','like','%'.$this->searchTerm.'%')
            ->orWhere('fund_master.v_key','like','%'.$this->searchTerm.'%')
            ->orWhere('fund_master.v_key_en','like','%'.$this->searchTerm.'%');
        }

        if($this->typeFilter) {
            $query->where('fund_master.type', $this->typeFilter);
            $query->orderBy('fund_master.order_number');
        }else{
            $query->orderBy('fund_master.id','desc');
        }

        if($this->typeFilterFund) {
            $query->where('fund_master.fund_id', $this->typeFilterFund);
        }

        if($this->vkeyFilter) {
            $query->where('fund_master.v_key', $this->vkeyFilter);
        }

        // $getType=FundMaster::get()->unique('type');
        $fundMasterTypes = EFundMaster::getList();
        // $getTypeFund=Fund::get()->unique('type');
        $getTypeFund=Fund::all();
        $getVkey=FundMaster::get()->unique('v_key');

        $data=$query->paginate($this->perPage);
        if($data->total()>0){
            $arrSortable = $data->toArray();
            $this->dataSortable=$arrSortable['data'];
        }
        return view('livewire.fund-master.fund-master-list',['data'=>$data,'getTypeFund'=>$getTypeFund,'getVkey'=>$getVkey, 'fundMasterTypes' => $fundMasterTypes]);
    }

    public function updatedSearchTerm(){
        $this->resetPage();
    }
    public function updatedTypeFilter(){
        $this->resetPage();
    }
    public function updatedTypeFilterFund(){
        $this->resetPage();
    }
    public function updatedVkeyFilter(){
        $this->resetPage();
    }
    public function resetSearch()
    {
        $this->searchTerm="";
        $this->vkeyFilter="";
        $this->typeFilterFund="";
        $this->typeFilter="";
    }

    public function delete(){
        $fundmsd = FundMaster::find($this->deleteId);
        $fundmsd->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa thành công." ]);
    }
    public function updateOrder($list){
        if($this->typeFilter){
            foreach ($this->dataSortable as $key_o=> $value_o){
                foreach ($list as $key_n=> $value_n) {
                    if($key_o==$key_n&& $value_n['value']!=$value_o['order_number']){
                        FundMaster::where('id',$value_o['id'])->update(['order_number'=>$value_n['value']? $value_n['value']:null]);
                    }
                }
            }
        }
    }
}
