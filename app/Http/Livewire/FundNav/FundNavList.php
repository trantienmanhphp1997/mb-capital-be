<?php

namespace App\Http\Livewire\FundNav;

use App\Http\Livewire\Base\BaseLive;
use App\Models\Fund;
use Livewire\Component;
use App\Models\FundNAV;
use App\Exports\FundNavExport;
use Maatwebsite\Excel\Facades\Excel;

class FundNavList extends BaseLive
{
    public $searchFund, $from_date, $to_date;
    public function render()
    {    
        $query=FundNAV::query();
        $query->leftjoin('fund','fund_nav.fund_id','=','fund.id')
        ->select('fund_nav.*','fund.shortname as fundname');
        if ($this->searchFund) {
            if ($this->searchFund == 'type_1') {
                $query->where('fund_nav.type', '1'); // VNINDEX
            } else {
                $query->where('fund_id', $this->searchFund);
            }
        }
        if (!empty($this->from_date) && !empty($this->to_date)) {
            $query->whereDate('fund_nav.trading_session_time', '>=', $this->from_date)
            ->whereDate('fund_nav.trading_session_time', '<=', $this->to_date);
            
        // dd($this);
        }
        $data=$query->orderBy('trading_session_time','desc')->paginate($this->perPage);
        $funds = Fund::all();
        return view('livewire.fund-nav.fund-nav-list',compact('data', 'funds'));
    }

    public function delete()
    {
        $deleteFund=FundNAV::findOrFail($this->deleteId);
        $deleteFund->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa thành công." ]);
    }

    public function export()
    {
        $listNav=FundNAV::query();
        if ($this->searchFund) {
            if ($this->searchFund == 'type_1') {
                $listNav->where('fund_nav.type', '1'); // VNINDEX
            } else {
                $listNav->where('fund_id', $this->searchFund);
            }
        }
        if (!empty($this->from_date) && !empty($this->to_date)) {
            $listNav->whereDate('fund_nav.trading_session_time', '>=', $this->from_date)
            ->whereDate('fund_nav.trading_session_time', '<=', $this->to_date);
            
        }
        if ($listNav->count() == 0) {
            $this->dispatchBrowserEvent('show-toast', ['type' => 'warning', 'message' => 'Ko có bản ghi nào!']);
            $this->emit('close-modal-export');
        }else{
            return Excel::download(new FundNavExport($this->searchFund,$listNav,$this->from_date,$this->to_date), 'dsNavQuy_'.date('Y-m-d-His').'.xlsx');
        }
    }
}
