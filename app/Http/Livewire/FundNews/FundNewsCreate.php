<?php

namespace App\Http\Livewire\Fundnews;

use App\Http\Livewire\Base\BaseLive;
use Livewire\Component;
use App\Models\FundNews;
use App\Models\Fund;
use Livewire\WithFileUploads;

class FundNewsCreate extends BaseLive
{

    use WithFileUploads;
    public $file;
    public $title_vi;
    public $title_en;
    public $content_vi;
    public $content_en;
    public $type;
    public $fund_id;
    public $public_date;
    public $url;
    public $file_name;

    public function mount(){
        $this->type = key(\App\Enums\EFundNews::getListData());
        $this->public_date = date('Y-m-d');

    }

    public function render()
    {
        $funds = Fund::all();
        return view('livewire.fundnews.fund-news-create', compact('funds'));
    }

    public function updatedFile() {
        $fileName = pathinfo($this->file->getClientOriginalName(), PATHINFO_FILENAME);
        $this->file_name = $fileName;
        $this->title_vi = $fileName;
        $this->title_en = $fileName;
        if(is_numeric(substr($fileName, -8))){
            $this->public_date =date(substr($fileName, -8,-4).'-'.substr($fileName, -4,-2).'-'.substr($fileName, -2));
        }
        $fund_name = "";

        if(strpos(strtolower($fileName), 'mbcapital') !== false){
            $this->type = 6;
        }
        elseif(strpos(strtolower($fileName), 'nav') !== false || strpos($fileName, 'gttsr') !== false){
            $this->type = 1;
        }
        elseif(strpos(strtolower($fileName), 'jambf') !== false){
            $this->fund_id = 1;
            $fund_name = "JAMBF";
        }
        elseif(strpos(strtolower($fileName), 'mbankhang') !== false || strpos(strtolower($fileName), 'mbthinhvuong') !== false ){
            $this->fund_id = 4;
            $fund_name = "MBANKHANG";
        }
        elseif(strpos(strtolower($fileName), 'mbvf') !== false){
            $this->fund_id = 2;
            $fund_name = "MBVF";
        }
        elseif(strpos(strtolower($fileName), 'mbbond') !== false){
            $this->fund_id = 3;
            $fund_name = "MBBOND";
        }

        if($this->type == 6 || $this->fund_id == 4) {
            $this->title_vi = $fileName;
            $this->title_en = $fileName;
        }
        elseif($this->type == 1 && $fund_name){
            $this->title_vi = $fund_name . " - Báo cáo NAV phiên GD ngày " . reFormatDate($this->public_date, "d/m/Y" );
            $this->title_en = $fund_name . " - NAV Report for Trading date " . reFormatDate($this->public_date, "d/m/Y" );;
        }
    }

    public function store() {
        $this->validate(
            [
                'file' => 'nullable|file|mimes:doc,docx,xlsx,jpg,png,pdf,xls,txt,zip,rar',
                'title_vi' => 'required',
                'title_en' => 'required',
                'type' => 'required',
                // 'url' =>  'required_if:type,5|regex:/^(?:http[s?]):\/\/(?:.*).(?:.*)$/u|nullable'
            ], [
                'file.file' => 'Tệp tải lên phải là file',
                'file.mimes' => 'Tệp tải lên không đúng định dạng',
                'title_vi.required' => 'Tiêu đề (Tiếng Việt) bắt buộc',
                'title_en.required' => 'Tiêu đề (Tiếng Anh) bắt buộc',
                'type.required' => 'Thể loại bắt buộc',
                // 'url.required' => 'Link bài viết là bắt buộc',
                // 'url.regex' => 'Link không đúng định dạng. Ví dụ: (https|http)://mbcapital.vn'
            ], []
        );

        $data = [
            'title_vi' => $this->title_vi,
            'content_vi' => $this->content_vi,
            'title_en' => $this->title_en,
            'content_en' => $this->content_en,
            'file_name' => $this->file_name,
            'type' => $this->type,
            'url' => $this->url,
            'fund_id' => ($this->fund_id) ? $this->fund_id : null,
            'file_path' => ($this->file) ? $this->file->store('public') : null,
            'public_date' => ($this->public_date) ? ($this->public_date) : now()
        ];
        FundNews::create($data);
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.add')] );
        return redirect()->route('admin.fundnews');
    }

    public function resetInputFields() {
        $this->reset([
            'file',
            'title_vi',
            'title_en',
            'content_vi',
            'content_en',
            'type',
            'public_date',
            'url',
            'file_name'
        ]);
    }
}
