<?php

namespace App\Http\Livewire\Fundnews;

use Livewire\Component;
use App\Http\Livewire\Base\BaseLive;
use App\Models\Fund;
use App\Models\FundNews;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB as FacadesDB;
use Livewire\WithFileUploads;

class FundNewsList extends BaseLive
{
    use WithFileUploads;

    public $searchName;
    public $searchType;
    public $searchFund;
    public $file;

    public function render()
    {

        $query=FundNews::query();

        if ($this->searchName) {
            $query->where('title_vi', 'like', '%' . trim($this->searchName) . '%')
                ->orwhere('title_en', 'like', '%' . trim($this->searchName) . '%')
                ->orwhere('content_vi', 'like', '%' . trim($this->searchName) . '%')
                ->orwhere('content_en', 'like', '%' . trim($this->searchName) . '%');
        }
        if ($this->searchType) {
            $query->where('type', $this->searchType);
        }
        if ($this->searchFund) {
            $query->where('fund_id', $this->searchFund);
        }

        $funds = Fund::all();
        $data= $query->orderBy('created_at','desc')->paginate($this->perPage);
        return view('livewire.fundnews.fund-news-list',compact('data', 'funds'));
    }

    public function resetSearch()
    {
        $this->reset([
            'searchName',
            'searchType',
            'searchFund',
        ]);
    }

    public function resetInputFields(){
        $this->file ='';
    }

    public function delete() {
        FundNews::findOrFail($this->deleteId)->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')] );
    }
}
