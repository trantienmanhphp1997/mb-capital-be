<?php

namespace App\Http\Livewire\Guideline;

use App\Http\Livewire\Base\BaseLive;
use App\Models\GuidelineDetail;
use Livewire\Component;

class GuidelineDetailList extends BaseLive
{
    public $guide_id;
    public $title;
    public $searchTerm;

    public function mount($guide_id, $title) {
        $this->guide_id = $guide_id;
        $this->title = $title;
    }

    public function render()
    {
        $query = GuidelineDetail::where('guideline_id', $this->guide_id);
        if ($this->searchTerm) {
            $query->where('name', 'like', '%' . trim($this->searchTerm) . '%')
                ->orwhere('name_en', 'like', '%' . trim($this->searchTerm) . '%')
                ->orwhere('content', 'like', '%' . trim($this->searchTerm) . '%')
                ->orwhere('content_en', 'like', '%' . trim($this->searchTerm) . '%');
        }
    
        $data = $query->orderBy('order_number','asc')->paginate($this->perPage);
        return view('livewire.guideline.guideline-detail-list', [
            'data' => $data,
            'guide_id' => $this->guide_id,
            'title' => $this->title
        ]);
    }

    public function delete() {
        GuidelineDetail::findOrFail($this->deleteId)->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa thành công." ]);
    }
}
