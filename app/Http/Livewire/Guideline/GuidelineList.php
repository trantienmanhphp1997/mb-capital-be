<?php

namespace App\Http\Livewire\Guideline;

use App\Http\Livewire\Base\BaseLive;
use App\Models\Fund;
use Livewire\WithFileUploads;
use App\Models\Guideline;

class GuidelineList extends BaseLive
{
    use WithFileUploads;
    public $searchName;
    public $searchFund;
    public $searchType;

    //data for create
    public $g_id;
    public $name;
    public $name_en;
    public $type;
    public $fund_id;
    public $image;
    public $video;
    public $remove_image;
    public $remove_video;
    public $change_image = false;
    public $change_video = false;
    public $url_video;

    protected $listeners = [
        'remove_image' => 'removeImage',
        'remove_video' => 'removeVideo',
    ];

    public function render()
    {
        $query=Guideline::query();
        if($this->searchName) {
            $query->where('name','like','%'.$this->searchName.'%')
            ->orwhere('name_en','like','%'.$this->searchName.'%');
        }
        if ($this->searchFund) {
            $query->where('fund_id', $this->searchFund);
        }
        if ($this->searchType) {
            $query->where('type', $this->searchType);
        }

        $data=$query->orderBy('id','asc')->paginate($this->perPage);

        $listFund = Fund::query()->whereNull('parent_id')->orderBy('priority')->get();
        return view('livewire.guideline.guideline-list',compact('data', 'listFund'));
    }

    public function delete() {
        $gl = Guideline::findOrFail($this->deleteId);
        // if($gl->image){
        //     if(file_exists('./storage/'. $gl->image)){
        //         unlink('./storage/'. $gl->image);
        //     }
        // }
        // if($gl->video){
        //     if(file_exists('./storage/'. $gl->video)){
        //         unlink('./storage/'. $gl->video);
        //     }
        // }
        $gl->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa thành công." ]);
    }

    public function resetform() {
        $this->reset(['name', 'name_en','type', 'fund_id', 'image', 'video', 'remove_image', 'remove_video','url_video']);
        $this->change_image = false;
        $this->change_video = false;
        // $this->emit('resetContent');
    }

    public function store() {
        $this->validate([
            'name' => 'required',
            'name_en' => 'required',
            'type' => 'required',
            'fund_id'=>'required',
        ], [
            'name.required' => 'Tên hướng dẫn (VI) bắt buộc',
            'name_en.required' => 'Tên hướng dẫn (EN) bắt buộc',
            'type.required' => 'Loại hướng dẫn bắt buộc',
            'fund_id.required'=>'Quỹ đầu tư bắt buộc',
        ], []);

        $guide = new Guideline();
        $guide->name = $this->name;
        $guide->name_en = $this->name_en;
        $guide->image = $this->image ? $this->image->storeAs('upload', $this->image->getClientOriginalName()) : '';
        $guide->video = $this->video ? $this->video->storeAs('upload', $this->video->getClientOriginalName()) : '';
        $guide->type = $this->type;
        $guide->fund_id = $this->fund_id;
        $guide->status = 1;
        $guide->url_video = $this->url_video;

        $guide->save();
        $this->resetform();
        $this->emit('close-modal-create');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.add')] );
    }

    public function edit($id){
        $this->updateMode = true;
        $gl = Guideline::findOrFail($id);
        $this->g_id = $gl->id;
        $this->name = $gl->name;
        $this->name_en = $gl->name_en;
        $this->fund_id = $gl->fund_id;
        $this->type = $gl->type;
        $this->image = $gl->image;
        $this->video = $gl->video;
        $this->url_video = $gl->url_video;
    }

    public function update(){

        $this->validate([
            'name' => 'required',
            'name_en' => 'required',
            'type' => 'required',
            'fund_id'=>'required',
        ], [
            'name.required' => 'Tên hướng dẫn (VI) bắt buộc',
            'name_en.required' => 'Tên hướng dẫn (EN) bắt buộc',
            'type.required' => 'Loại hướng dẫn bắt buộc',
            'fund_id.required'=>'Quỹ đầu tư bắt buộc',
        ], []);

        $gl = Guideline::findOrFail($this->g_id);
        $gl->name = $this->name;
        $gl->name_en = $this->name_en;
        $gl->fund_id = $this->fund_id;
        $gl->type = $this->type;
        if(isset($this->remove_image) && $this->remove_image){
            if(file_exists('./storage/'. $this->remove_image)){
                unlink('./storage/'. $this->remove_image);
            }
            $gl->image = null;
        }
        if ($this->change_image && $this->image) {
            $gl->image = $this->image->storeAs('upload', $this->image->getClientOriginalName());
        }
        if(isset($this->remove_video) && $this->remove_video){
            if(file_exists('./storage/'. $this->remove_video)){
                unlink('./storage/'. $this->remove_video);
            }
            $gl->video = null;
        }
        if ($this->change_video && $this->video) {
            $gl->video = $this->video->storeAs('upload', $this->video->getClientOriginalName());
        }
        $gl->url_video = $this->url_video;
        $gl->save();

        $this->resetform();
        $this->emit('close-modal-edit');
        $this->updateMode = false;
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.update')] );
    }

    public function removeImage() {
        $this->remove_image = $this->image;
        $this->image = null;
        $this->change_image = true;
    }

    public function removeVideo() {
        $this->remove_video = $this->video;
        $this->video = null;
        $this->change_video = true;
    }
}
