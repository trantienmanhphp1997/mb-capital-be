<?php

namespace App\Http\Livewire\News;
use App\Http\Livewire\Base\BaseLive;
use App\Models\Article;
use Livewire\Component;
use Illuminate\Support\Facades\Config;

class ArticleList extends BaseLive
{
    public $searchName;
    public $searchType;
    public $searchFund;
    public function render()
    {
        $query = Article::query()->where('status', '=', 1);
        $this->searchName = trim($this->searchName);
        if($this->searchName){
            $query = $query->where('name_vi', 'like', '%'. $this->searchName .'%')
            ->orWhere('name_en', 'like', '%'. $this->searchName .'%')
            ->orWhere('intro_vi', 'like', '%'. $this->searchName .'%')
            ->orWhere('intro_en', 'like', '%'. $this->searchName .'%')
            ->orWhere('content_vi', 'like', '%'. $this->searchName .'%')
            ->orWhere('content_en', 'like', '%'. $this->searchName .'%')
            ->orWhere('author','like', '%'. $this->searchName .'%');
        }

        $newData = $query->orderBy('created_at', 'desc')->paginate($this->perPage);
        return view('livewire.news.article-list', compact('newData'));
    }

    public function delete() {
        Article::findOrFail($this->deleteId)->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.delete')] );
    }
}
