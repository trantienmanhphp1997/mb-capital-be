<?php

namespace App\Http\Livewire\Survey;

use App\Http\Livewire\Base\BaseLive;
use App\Models\SurveyDetail;
use Livewire\Component;

class SurveyDetailList extends BaseLive
{
    public $survey_id;
    public $survey_detail_id;
    public $title;
    public $name;
    public $name_en;
    public $point;
    public $searchTerm;

    public function mount($survey_id, $title) {
        $this->survey_id = $survey_id;
        $this->title = $title;
    }

    public function render()
    {
        $query = SurveyDetail::where('survey_id', $this->survey_id);
        // if ($this->searchTerm) {
        //     $query->where('name', 'like', '%' . trim($this->searchTerm) . '%')
        //         ->orwhere('name_en', 'like', '%' . trim($this->searchTerm) . '%');
        // }
    
        $data = $query->paginate($this->perPage);
        return view('livewire.survey.survey-detail-list', [
            'data' => $data,
            'survey_id' => $this->survey_id,
            'title' => $this->title,
        ]);
    }

    public function delete() {
        SurveyDetail::findOrFail($this->deleteId)->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa thành công." ]);
    }

    public function resetform() {
        $this->reset(['name', 'name_en', 'point']);
    }

    public function store() {
        $this->validate([
            'name' => 'required',
            'name_en' => 'required',
        ], [
            'name.required' => 'Tên hướng dẫn (VI) bắt buộc',
            'name_en.required' => 'Tên hướng dẫn (EN) bắt buộc',
        ], []);

        $surveyDetail = new SurveyDetail();
        $surveyDetail->name = $this->name;
        $surveyDetail->name_en = $this->name_en;
        $surveyDetail->point = $this->point;
        $surveyDetail->survey_id = $this->survey_id;
        $surveyDetail->save();
        $this->resetform();
        $this->emit('close-modal-create');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.add')] );
    }

    public function edit($id){
        $this->updateMode = true;
        $surveyDetail = SurveyDetail::findOrFail($id);
        $this->survey_detail_id = $id;
        $this->name = $surveyDetail->name;
        $this->name_en = $surveyDetail->name_en;
        $this->point = $surveyDetail->point;
    }

    public function update(){

        $this->validate([
            'name' => 'required',
            'name_en' => 'required',
        ], [
            'name.required' => 'Tên hướng dẫn (VI) bắt buộc',
            'name_en.required' => 'Tên hướng dẫn (EN) bắt buộc',
        ], []);

        $surveyDetail = SurveyDetail::findOrFail($this->survey_detail_id);
        $surveyDetail->name = $this->name;
        $surveyDetail->name_en = $this->name_en;
        $surveyDetail->point = $this->point;
        $surveyDetail->save();
        $this->resetform();
        $this->emit('close-modal-edit');
        $this->updateMode = false;
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.update')] );
    }
}
