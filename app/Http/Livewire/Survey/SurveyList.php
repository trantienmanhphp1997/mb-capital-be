<?php

namespace App\Http\Livewire\Survey;

use App\Http\Livewire\Base\BaseLive;
use Livewire\WithFileUploads;
use App\Models\Survey;

class SurveyList extends BaseLive
{
    use WithFileUploads;

    public $survey_id;
    public $searchName;
    public $order_number;
    public $name;
    public $name_en;
    public $multi_select;

    public function render()
    {
        $query=Survey::query();
        $data=$query->orderBy('order_number','asc')->paginate($this->perPage);

        return view('livewire.survey.survey-list',compact('data'));
    }

    public function delete() {
        Survey::findOrFail($this->deleteId)->delete();
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => "Xóa thành công." ]);
    }

    public function resetform() {
        $this->reset(['name', 'name_en', 'multi_select', 'order_number']);
    }

    public function store() {
        $this->validate([
            'name' => 'required',
            'name_en' => 'required',
            'order_number' => 'required|unique:survey'
        ], [
            'name.required' => 'Tên hướng dẫn (VI) bắt buộc',
            'name_en.required' => 'Tên hướng dẫn (EN) bắt buộc',
            'order_number.required' => 'Số thứ tự bắt buộc',
            'order_number.unique' => 'Số thứ tự đã tồn tại'
        ], []);

        $survey = new Survey();
        $survey->name = $this->name;
        $survey->name_en = $this->name_en;
        $survey->multi_select = $this->multi_select?1:0;
        $survey->order_number = $this->order_number;
        $survey->save();
        $this->resetform();
        $this->emit('close-modal-create');
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.add')] );
    }

    public function edit($id){
        $this->updateMode = true;
        $survey = Survey::findOrFail($id);
        $this->survey_id = $id;
        $this->name = $survey->name;
        $this->name_en = $survey->name_en;
        $this->multi_select = $survey->multi_select;
        $this->order_number = $survey->order_number;
    }

    public function update(){

        $this->validate([
            'name' => 'required',
            'name_en' => 'required',
            'order_number' => 'required|unique:survey,order_number,'.$this->survey_id,
        ], [
            'name.required' => 'Tên câu hỏi (VI) bắt buộc',
            'name_en.required' => 'Tên câu hỏi (EN) bắt buộc',
            'order_number.required' => 'Số thứ tự bắt buộc',
            'order_number.unique' => 'Số thứ tự đã tồn tại'
        ], []);
        $survey = Survey::findOrFail($this->survey_id);
        $survey->name = $this->name;
        $survey->name_en = $this->name_en;
        $survey->multi_select = $this->multi_select?1:0;
        $survey->order_number = $this->order_number;
        $survey->save();

        $this->resetform();
        $this->emit('close-modal-edit');
        $this->updateMode = false;
        $this->dispatchBrowserEvent('show-toast', ["type" => "success", "message" => __('notification.common.success.update')] );
    }
}
