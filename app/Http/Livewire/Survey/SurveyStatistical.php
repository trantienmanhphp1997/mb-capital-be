<?php

namespace App\Http\Livewire\Survey;


use App\Enums\EMasterData;
use App\Http\Livewire\Base\BaseLive;
use App\Models\MasterData;
use App\Models\Survey;
use App\Models\SurveyResponse;
use Livewire\Component;
use DB;
class SurveyStatistical extends BaseLive
{
    public $countQuestion = 0;
    public $searchTerm = 1;
    public $fromDate, $toDate;
    public $answer;
    public $status=false;
    public $dataPieAll=1;
    public function render()
    {
        if($this->reset){
            $this->searchTerm=1;
            $this->fromDate='';
            $this->toDate='';
        }
        $selectShow = Survey::orderBy('order_number')->pluck('order_number', 'order_number')->toArray();
//        dd($selectShow);
        $this->countQuestion=count($selectShow);
        $survey=[];
        $dataPie=collect();

        $arr_pie=[];
        $color=['#E5E5E5','#6285C1','#123673','#B9CDE5','#BD9976'];
        $query=SurveyResponse::leftJoin('survey_response_detail','survey_response_detail.survey_response_id','=','survey_response.id')
        ->leftJoin('survey_detail','survey_detail.id','=','survey_response_detail.survey_details_id')
            ->leftJoin('survey','survey.id','=','survey_response_detail.survey_id');
        if($this->fromDate){
            $query->whereDate('request_date','>=',date('Y-m-d', strtotime($this->fromDate)));
        }
        if($this->toDate){
            $query->whereDate('request_date','>=',date('Y-m-d', strtotime($this->toDate)));
        }
        if($this->searchTerm){
            if($this->searchTerm!='rate_level_risk'){
                $survey=Survey::where('order_number',$this->searchTerm)->first();
                $query->where('order_number',$this->searchTerm);


                $query->groupBy('ip_address')->select( 'ip_address',DB::raw("COUNT(*) as 'all' "));
                foreach ($survey->details as $item) {
                    $query->addSelect(DB::raw("COUNT(CASE WHEN survey_detail.id = " . $item->id . " THEN 1 END) as "."col_$item->id"));
                }
                $arr=$query->get()->toArray();

                $this->dataPieAll=array_sum(array_column($arr,'all'));
                foreach ($survey->details as $key => $item) {
                    $arr_pie['value']=['value'=>array_sum(array_column($arr,"col_$item->id")),'color'=>$color[$key]];
                    $arr_pie['label']="Đáp án ".($key+1);
                    $dataPie->push($arr_pie);
                }
            }
            else{
                $rate_level=MasterData::where('type',EMasterData::RATE_LEVEL_RISK)->get();
                $query->leftJoin('master_data','master_data.id','=','survey_response_detail.survey_id');
                $query->groupBy('master_data.id')->select( 'master_data.id',DB::raw("COUNT(*) as 'all' "));
                foreach ($rate_level as $item) {
                    $query->addSelect(DB::raw("COUNT(CASE WHEN rate_level_risk = " . $item->id . " THEN 1 END) as "."col_$item->id"));
                }
                $arr=$query->first();
                if($arr){
                    $arr= $arr->toArray();
                }
                foreach ($rate_level as $key => $item) {
                    $arr_pie['value']=['value'=>$arr["col_$item->id"]??0,'color'=>$color[$key]];
                    $arr_pie['label']=$item->v_value;
                    $dataPie->push($arr_pie);
                }
                
            }
        }
        $master_data = MasterData::where('type',EMasterData::RATE_LEVEL_RISK)->pluck('v_value','v_value')->toArray();
        $this->dispatchBrowserEvent('update_scripts',['data' => $dataPie]);
        return view('livewire.survey.survey-statistical', ['selectShow' => $selectShow,'master_data'=>$master_data, 'survey'=>$survey,'dataPie'=>$dataPie,'all'=>$this->dataPieAll]);
    }
    public function search(){
        $this->status=true;
    }
}
