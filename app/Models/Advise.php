<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Advise extends Model
{
    use HasFactory;
    protected $table = 'advise_list';
    protected $fillable = [
        'name',
        'email_phone',
        'advise_content',
        'IP',
        'request_time',
    ];

}
