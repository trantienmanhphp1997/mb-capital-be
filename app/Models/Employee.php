<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    protected $table = 'employee';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'fullname',
        'sex',
        'title',
        'type',
        'title_en',
        'type_value',
        'type_value_en',
        'fund_id',
        'content',
        'content_en',
        'img',
        'title_2',
        'title_2_en'
    ];

    public function fund(){
        return $this->belongsTo(Fund::class,'fund_id');
    }
}
