<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class Faq extends Model implements Auditable {
    use \OwenIt\Auditing\Auditable;
    use HasFactory,SoftDeletes;
    protected $table = 'faq';
    public function fund()
    {
        return $this->belongsTo(Fund::class, 'fund_id');
    }
}