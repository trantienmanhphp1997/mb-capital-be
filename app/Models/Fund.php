<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fund extends Model
{
    use HasFactory;
    protected $table = 'fund';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'shortname',
        'shortname_en',
        'fullname',
        'fullname_en',
        'description',
        'description_en',
        'content',
        'content_en',
        'created_by',
        'type',
        'current_nav',
        'growth',
        'slug',
        'slug_en',
        'fun_code',
        'interest',
        'parent_id',
        'priority',
        'shortname2',
        'shortname2_en',
        'fullname2',
        'fullname2_en',
        'shortname2',
        'shortname2_en',
        'enable_performance',
        'enable_info_disclosure',
        'pulic_date',
        'public_date_en'
    ];
}
