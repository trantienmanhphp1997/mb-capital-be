<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FundEmployeeXRF extends Model
{
    use HasFactory;
    protected $table = 'fun_employee_xrf';
    // create, update column
    protected $fillable = [
        'fund_id',
        'employee_id',
        'role_name',
    ];

    //query search
    protected $searchableByOrWhere = [
        
    ];
    public function getSearchableByOrWhere(){
        return $this->searchableByOrWhere;
    }    
    

    protected $searchableByWhere = [

    ];    
    public function getSearchableByWhere(){
        return $this->searchableByWhere;
    } 

    // column table
    protected $columnTable = [
        'fund_id',
        'employee_id',
        'role_name',
    ];    
    public function getColumnTable(){
        return $this->columnTable;
    } 
    //column sorting
    protected $columnSortingTable = [

    ];    
    public function getColumnSortingTable(){
        return $this->columnSortingTable;
    } 

    
    //export
    protected $columnExport = [

    ];  
    public function  getColumnExport() {
        return $this->columnExport;
    }


    public function employee() {
        return $this->belongsTo(Employee::class);
    }

    public function fund() {
        return $this->belongsTo(Fund::class);
    }
}
