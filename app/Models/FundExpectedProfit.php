<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FundExpectedProfit extends Model
{
    use HasFactory;
    protected $table = 'fun_expected_profit';
    // create, update column
    protected $fillable = [
        'fund_id', 'period', 'period_name', 'period_name_en', 'percent', 'percent_view','type',
    ];

    //query search
    protected $searchableByOrWhere = [
        'period_name'
    ];
    public function getSearchableByOrWhere(){
        return $this->searchableByOrWhere;
    }

    protected $searchableByWhere = [];
    public function getSearchableByWhere(){
        return $this->searchableByWhere;
    }

    // column table
    protected $columnTable = [
        'fund_id',
        'period',
        'period_name',
        'percent',
    ];
    public function getColumnTable(){
        return $this->columnTable;
    }

    //column sorting
    protected $columnSortingTable = [
        'fund_id',
        'period',
        'period_name',
        'percent',
    ];
    public function getColumnSortingTable(){
        return $this->columnSortingTable;
    }

    //export
    protected $columnExport = [];
    public function  getColumnExport() {
        return $this->columnExport;
    }

    public function fund(){
        return $this->belongsTo(Fund::class,'fund_id');
    }
}
