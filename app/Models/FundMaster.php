<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class FundMaster extends Model implements Auditable
{
    use HasFactory,SoftDeletes;
    use \OwenIt\Auditing\Auditable;

    protected $table = 'fund_master';

    protected $fillable = [
        'fund_id',
        'title',
        'title_en',
        'content',
        'content_en',
        'type',
        'img',
        'url',
        'v_key',
        'order_number',
        'number_value',
        'note',
        'note_en',
        'v_key_en'
    ];

    public function fund()
    {
        return $this->belongsTo(Fund::class, 'fund_id');
    }
}
