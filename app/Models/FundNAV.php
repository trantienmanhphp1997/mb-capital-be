<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;


class FundNAV extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;

    protected $table='fund_nav';

    protected $fillable=[
        'fund_id',
        'amount',
        'trading_session_time',
        'type',
    ];

    public function fund()
    {
        $this->belongsTo(Fund::class,'fund_id');
    }
}
