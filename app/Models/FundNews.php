<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;


class FundNews extends Model implements Auditable
{
    use HasFactory,SoftDeletes;
    use \OwenIt\Auditing\Auditable;
    protected $table = 'fund_news';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title_vi',
        'content_vi',
        'title_en',
        'content_en',
        'type',
        'fund_id',
        'public_date',
        'file_path',
        'url'
    ];

    public function fund(){
        return $this->belongsTo(Fund::class,'fund_id');
    }
}
