<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;


class Guideline extends Model  implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    protected $table='guideline';
    protected $fillable=[
        'name','name_en','image','video','type','status','url_video'
    ];

    public function details()
    {
        return $this->hasMany(GuidelineDetail::class, 'guideline_id');
    }
    public function fund(){
        return $this->belongsTo(Fund::class,'fund_id');
    }
}
