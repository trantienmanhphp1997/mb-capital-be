<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\GuideLine;
use OwenIt\Auditing\Contracts\Auditable;

class GuidelineDetail extends Model implements Auditable
{
    use HasFactory;
    use \OwenIt\Auditing\Auditable;
    protected $table='guideline_detail';

    protected $fillable=[
        'name','name_en','guideline_id','content','content_en','order_number','status','content_mobile','content_mobile_en'
    ];

    public function guideline()
    {
        $this->belongsTo(GuideLine::class,'guideline_id');
    }
}
