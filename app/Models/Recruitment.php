<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Recruitment extends Model
{
    use HasFactory;
    protected $fillable = [
        'title', 'content','url','title_en', 'content_en', 'date_submit', 'file_path', 'position', 'job', 'skill', 'language', 'position_en', 'job_en', 'skill_en', 'language_en', 'slug', 'slug_en', 'short_content', 'short_content_en',
    ];

}
