<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    use HasFactory;

    protected $table='survey';
    protected $fillable=[
        'name','name_en',
    ];

    public function details()
    {
        return $this->hasMany(SurveyDetail::class, 'survey_id');
    }
}
