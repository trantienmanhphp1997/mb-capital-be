<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SurveyDetail extends Model
{
    use HasFactory;

    protected $table='survey_detail';

    protected $fillable=[
        'name','name_en','survey_id', 'point',
    ];

    public function guideline()
    {
        $this->belongsTo(GuideLine::class,'survey_id');
    }
}
