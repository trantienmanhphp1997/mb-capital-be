<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SurveyResponse extends Model
{
    use HasFactory;
    // create, update column
    protected $table='survey_response';
    protected $fillable = ['ip_address','sum_point','rate_level_risk','request_date','device','browser','save_time'];

    //query search
    protected $searchableByOrWhere = [];
    public function getSearchableByOrWhere(){
        return $this->searchableByOrWhere;
    }

    protected $searchableByWhere = [];
    public function getSearchableByWhere(){
        return $this->searchableByWhere;
    }

    // column table
    protected $columnTable = [
        'ip_address',
        'sum_point',
        'rate_level_risk',
        'request_date',
    ];
    public function getColumnTable(){
        return $this->columnTable;
    }

    //column sorting
    protected $columnSortingTable = [
        'ip_address',
        'sum_point',
        'rate_level_risk',
        'request_date',
    ];
    public function getColumnSortingTable(){
        return $this->columnSortingTable;
    }

    //export
    protected $columnExport = [];
    public function  getColumnExport() {
        return $this->columnExport;
    }

}
