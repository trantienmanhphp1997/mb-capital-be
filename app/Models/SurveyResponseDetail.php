<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SurveyResponseDetail extends Model
{
    use HasFactory;
    protected $table = 'survey_response_detail';
    protected $fillable =['survey_response_id','survey_id','survey_details_id'];
}
