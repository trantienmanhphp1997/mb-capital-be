<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    use HasFactory;
    protected $table = 'tests';
    // create, update column
    protected $fillable = [
        'name',
        'contract_number',
        'actor_name',
        'director',
        'status'
    ];

    //query search
    protected $searchableByOrWhere = [
        'name',
        'contract_number',
    ];
    public function getSearchableByOrWhere(){
        return $this->searchableByOrWhere;
    }    
    

    protected $searchableByWhere = [
        'actor_name',
        'director',
        'status',
    ];    
    public function getSearchableByWhere(){
        return $this->searchableByWhere;
    } 

    // column table
    protected $columnTable = [
        'name',
        'contract_number',
        'actor_name',
        'director',
        'status',
    ];    
    public function getColumnTable(){
        return $this->columnTable;
    } 
    //column sorting
    protected $columnSortingTable = [
        'name',
        'contract_number',
        'actor_name',
        'director',
    ];    
    public function getColumnSortingTable(){
        return $this->columnSortingTable;
    } 

    
    //export
    protected $columnExport = [
        'name',
        'contract_number',
        'actor_name',
        'director',
        'status'
    ];  
    public function  getColumnExport() {
        return $this->columnExport;
    }
    

}
