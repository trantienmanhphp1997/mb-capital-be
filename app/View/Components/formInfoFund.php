<?php

namespace App\View\Components;

use Illuminate\View\Component;

class FormInfoFund extends Component
{
    public $lang;
    public $fund;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($lang, $fund)
    {
        $this->lang = $lang;
        $this->fund = $fund;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form-info-fund');
    }
}
