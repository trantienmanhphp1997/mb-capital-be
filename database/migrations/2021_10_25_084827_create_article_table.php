<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArticleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('article', function (Blueprint $table) {
            $table->id();
            $table->string('name_vi')->comment('Vietnames name');
            $table->string('name_en')->comment('English name');
            $table->text('intro_vi')->nullable()->comment('Vietnames description');
            $table->text('intro_en')->nullable()->comment('English description');
            $table->text('content_vi')->nullable()->comment('Vietnames content');
            $table->text('content_en')->nullable()->comment('English content');
            $table->string('meta_title_vi')->nullable()->comment('Vietnames meta data title');
            $table->string('meta_title_en')->nullable()->comment('English meta data title');
            $table->string('meta_des_vi')->nullable()->comment('Vietnames meta data description');
            $table->string('meta_des_en')->nullable()->comment('English meta data description');
            $table->string('image')->comment('Meta data image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('article');
    }
}
