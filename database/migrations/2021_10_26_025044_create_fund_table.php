<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFundTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fund', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('shortname')->comment('The abbreviation of the investment fund (VI)'); // Tên viết tắt của quỹ đầu tư
            $table->string('shortname_en')->comment('The abbreviation of the investment fund (EN)');
            $table->string('fullname')->comment('The full name of the investment fund (VI)'); // Tên đầy đủ của quỹ đầu tư
            $table->string('fullname_en')->comment('The full name of the investment fund (EN)');
            $table->string('description')->nullable()->comment('Brief description (VI)'); // Mô tả quỹ đầu tư
            $table->string('description_en')->nullable()->comment('Brief description (EN)');
            $table->longText('content')->nullable()->comment('investment fund content (VI)');
            $table->longText('content_en')->nullable()->comment('investment fund content (EN)');
            $table->bigInteger('created_by')->nullable()->comment('creator');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fund');
    }
}
