<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdviseListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advise_list', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email_phone')->unique();
            $table->text('advise_content');
            $table->string('IP');
            $table->timestamp('request_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advise_list');
    }
}
