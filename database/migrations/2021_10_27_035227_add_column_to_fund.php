<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToFund extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund', function (Blueprint $table) {
            //
            $table->tinyInteger('type')->nullable()->comment('type quỹ');
            $table->double('current_nav',19,2)->nullable()->comment('');
            $table->double('growth',8,2)->nullable()->comment('tăng trưởng');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund', function (Blueprint $table) {
            //
            $table->dropColumn('type');
            $table->dropColumn('current_nav');
            $table->dropColumn('growth');
        });
    }
}
