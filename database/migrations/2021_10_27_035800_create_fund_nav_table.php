<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFundNavTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fund_nav', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('fund_id')->nullable()->comment('Map voi fund')->constrained('fund');
            $table->double('amount',19,2)->nullable()->comment('số tiền');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fund_nav');
    }
}
