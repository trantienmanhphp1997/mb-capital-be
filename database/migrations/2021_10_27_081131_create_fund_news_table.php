<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFundNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fund_news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title_vi')->comment('Vietnamese title');
            $table->string('title_en')->comment('English title');
            $table->longtext('content_vi')->nullable()->comment('Vietnamese content');
            $table->longtext('content_en')->nullable()->comment('English content');
            $table->foreignId('fund_id')->nullable()->comment('Map voi fund')->constrained('fund');
            $table->tinyInteger('type')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fund_news');
    }
}
