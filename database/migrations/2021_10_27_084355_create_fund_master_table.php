<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFundMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fund_master', function (Blueprint $table) {
            $table->id();
            $table->foreignId('fund_id')->nullable()->constrained('fund')->comment('mã quỹ đầu tư');
            $table->string('title');
            $table->string('title_en');
            $table->longText('content')->nullable();
            $table->longText('content_en')->nullable();
            $table->integer('type')->comment('1,2,3,4,5');
            $table->string('img');
            $table->string('url');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fund_master');
    }
}
