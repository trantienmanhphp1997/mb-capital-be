<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToFundMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_master', function (Blueprint $table) {
            $table->string('v_key', 255)->comment('khóa');
            $table->integer('order_number')->nullable()->comment('Sắp xếp thứ tự');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund', function (Blueprint $table) {
            //
            $table->dropColumn('v_key');
            $table->dropColumn('order_number');
        });
    }
}
