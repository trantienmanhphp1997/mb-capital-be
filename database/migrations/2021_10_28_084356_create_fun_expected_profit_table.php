<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFunExpectedProfitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fun_expected_profit', function (Blueprint $table) {
            $table->id();
            $table->foreignId('fund_id')->nullable()->constrained('fund')->comment('mã quỹ đầu tư');
            $table->integer('period')->comment('Kì hạn gửi');
            $table->string('period_name')->comment('Tên hiển thị');
            $table->string('period_name_en')->comment('Tên hiển thị');
            $table->float('percent')->comment('Lãi suất gửi theo kì hạn');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expected_profit');
    }
}
