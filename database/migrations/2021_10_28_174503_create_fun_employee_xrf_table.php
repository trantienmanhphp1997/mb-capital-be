<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFunEmployeeXrfTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fun_employee_xrf', function (Blueprint $table) {
            $table->id();
            $table->foreignId('fund_id')->nullable()->constrained('fund')->comment('mã quỹ đầu tư');
            $table->foreignId('employee_id')->nullable()->constrained('employee')->comment('mã nhân viên');
            $table->string('role_name')->nullable()->comment('vai trò');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee');
    }
}
