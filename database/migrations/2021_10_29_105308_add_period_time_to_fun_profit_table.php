<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPeriodTimeToFunProfitTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fun_expected_profit', function (Blueprint $table) {
            $table->integer('period_time_min')->nullable()->comment('Thời gian tối thiểu');
            $table->integer('period_time_max')->nullable()->comment('Thời gian tối đa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fun_expected_profit', function (Blueprint $table) {
            $table->dropColumn('period_time_min');
            $table->dropColumn('period_time_max');
        });
    }
}
