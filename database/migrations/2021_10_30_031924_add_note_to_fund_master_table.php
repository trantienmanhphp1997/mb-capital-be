<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNoteToFundMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_master', function (Blueprint $table) {
            $table->longText('note')->nullable();
            $table->longText('note_en')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_master', function (Blueprint $table) {
            $table->dropColumn('note');
            $table->dropColumn('note_en');
        });
    }
}
