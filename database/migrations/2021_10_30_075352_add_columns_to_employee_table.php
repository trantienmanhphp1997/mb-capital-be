<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToEmployeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee', function (Blueprint $table) {
            $table->string('title_en')->nullable();
            $table->string('type_value');
            $table->string('type_value_en');
            $table->foreignId('fund_id')->nullable()->constrained('fund')->comment('mã quỹ đầu tư');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee', function (Blueprint $table) {
            $table->dropColumn('title_en');
            $table->dropColumn('type_value');
            $table->dropColumn('type_value_en');
            $table->dropColumn('fund_id');
        });
    }
}
