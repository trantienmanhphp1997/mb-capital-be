<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeToFundNavTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_nav', function (Blueprint $table) {
            $table->integer('type')->nullable()->default('0')->comment('1: An Khang, 2: Thịnh Vượng');
            $table->date('trading_session_time')->nullable()->comment('Thời gian công bố thông tin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_nav', function (Blueprint $table) {
            $table->dropColumn('type');
            $table->dropColumn('trading_session_time');
        });
    }
}
