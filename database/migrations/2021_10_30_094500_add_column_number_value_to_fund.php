<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnNumberValueToFund extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund', function (Blueprint $table) {
            $table->string('interest')->nullable()->comment('lãi xuất');
            $table->bigInteger('parent_id')->nullable()->comment('quỹ cha ');
            $table->bigInteger('priority')->nullable()->comment('mức độ ưu tiên');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund', function (Blueprint $table) {
            $table->dropColumn('interest');
            $table->dropColumn('parent_id');
            $table->dropColumn('priority');
        });
    }
}
