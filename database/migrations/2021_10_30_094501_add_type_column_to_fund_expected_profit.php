<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeColumnToFundExpectedProfit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fun_expected_profit', function (Blueprint $table) {
            $table->integer('type')->nullable()->comment('0 là gửi tiết kiệm');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fun_expected_profit', function (Blueprint $table) {
            $table->dropColumn('type');
        });
    }
}
