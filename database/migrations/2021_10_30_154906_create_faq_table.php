<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFaqTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('question', 1000)->nullable()->comment('Question (VI)'); // Câu hỏi
            $table->string('question_en', 1000)->nullable()->comment('Question (EN)');
            $table->string('answer', 1000)->nullable()->comment('Answer (VI)'); // Câu trả lời
            $table->string('answer_en', 1000)->nullable()->comment('Answer (EN)');
            $table->longText('content')->nullable()->comment('content (VI)');
            $table->longText('content_en')->nullable()->comment('content (EN)');
            $table->foreignId('fund_id')->nullable()->comment('Map voi fund')->constrained('fund');
            $table->integer('type')->nullable()->comment('Danh dau cau hoi thuong gap');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faq');
    }
}
