<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnShortname2ToFund extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund', function (Blueprint $table) {
            //
            $table->string('shortname2')->nullable()->comment('The abbreviation of the investment fund, use for footer (VI)');
            $table->string('shortname2_en')->nullable()->comment('The abbreviation of the investment fund, use for footer (VI)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund', function (Blueprint $table) {
            //
            $table->dropColumn('shortname2');
            $table->dropColumn('shortname2_en');
        });
    }
}
