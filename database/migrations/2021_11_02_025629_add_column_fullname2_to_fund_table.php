<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnFullname2ToFundTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund', function (Blueprint $table) {
            //
            $table->string('fullname2')->nullable()->comment('The fullname2 of the investment fund, use for header (VI)');
            $table->string('fullname2_en')->nullable()->comment('The fullname2 of the investment fund, use for header (EN)');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund', function (Blueprint $table) {
            //
            $table->dropColumn('fullname2');
            $table->dropColumn('fullname2_en');
        });
    }
}
