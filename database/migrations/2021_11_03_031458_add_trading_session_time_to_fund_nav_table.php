<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTradingSessionTimeToFundNavTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_nav', function (Blueprint $table) {
            // $table->date('trading_session_time')->nullable()->comment('Ngày công bố');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_nav', function (Blueprint $table) {
            // $table->dropColumn('trading_session_time');
        });
    }
}
