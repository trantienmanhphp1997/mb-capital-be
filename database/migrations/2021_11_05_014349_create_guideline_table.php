<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuidelineTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guideline', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255)->nullable()->comment('Ten huong dan');
            $table->string('name_en', 255)->nullable()->comment('Ten huong dan en');
            $table->string('image', 255)->nullable()->comment('');
            $table->string('video', 255)->nullable()->comment('');
            $table->integer('type')->nullable()->comment('1: huong dan app, 2: huong dan web');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guideline');
    }
}
