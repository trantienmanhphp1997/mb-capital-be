<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGuidelineDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('guideline_detail', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255)->nullable()->comment('Ten cac buoc');
            $table->string('name_en', 255)->nullable()->comment('Ten cac buoc en');
            $table->integer('guideline_id')->nullable()->comment('map voi guideline');
            $table->longtext('content')->nullable()->comment('Noi dung cac buoc huong dan tieng Viet');
            $table->longtext('content_en')->nullable()->comment('Noi dung cac buoc huong dan tieng Anh');
            $table->integer('order_number')->nullable()->comment('So thu tu hien thi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('guideline_detail');
    }
}
