<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnUrlCurrentToLogAccess extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_access', function (Blueprint $table) {
            $table->string('url_current')->nullable()->comment('url dang truy cap');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_access', function (Blueprint $table) {
            $table->dropColumn('url_current');
        });
    }
}
