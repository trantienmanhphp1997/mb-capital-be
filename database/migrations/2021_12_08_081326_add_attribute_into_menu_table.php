<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAttributeIntoMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('menu', function (Blueprint $table) {
            $table->string('meta')->nullable();
            $table->string('meta_en')->nullable();
            $table->string('title')->nullable();
            $table->string('title_en')->nullable();
            $table->string('description')->nullable();
            $table->string('description_en')->nullable();
            $table->string('keywords')->nullable();
            $table->string('keywords_en')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recruitments', function (Blueprint $table) {
            $table->dropColumn('meta');
            $table->dropColumn('meta_en');
            $table->dropColumn('title');
            $table->dropColumn('title_en');
            $table->dropColumn('description');
            $table->dropColumn('description_en');
            $table->dropColumn('keywords');
            $table->dropColumn('keywords_en');
        });
    }
}
