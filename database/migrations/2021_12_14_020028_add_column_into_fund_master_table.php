<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnIntoFundMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund_master', function (Blueprint $table) {
            $table->longText('content_mobile')->nullable();
            $table->longText('content_mobile_en')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund_master', function (Blueprint $table) {
            $table->dropColumn('content_mobile');
            $table->dropColumn('content_mobile_en');
        });
    }
}
