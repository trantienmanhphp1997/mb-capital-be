<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnIntoRecruitmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recruitments', function (Blueprint $table) {
            $table->date('date_submit')->nullable();
            $table->text('file_path')->nullable();
            $table->text('position')->nullable();
            $table->text('position_en')->nullable();
            $table->text('job')->nullable();
            $table->text('job_en')->nullable();
            $table->text('skill')->nullable();
            $table->text('skill_en')->nullable();
            $table->text('language')->nullable();
            $table->text('language_en')->nullable();
            $table->text('slug')->nullable();
            $table->text('slug_en')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recruitments', function (Blueprint $table) {
            $table->dropColumn('date_submit');
            $table->dropColumn('file_path');
            $table->dropColumn('position');
            $table->dropColumn('position_en');
            $table->dropColumn('job');
            $table->dropColumn('job_en');
            $table->dropColumn('skill');
            $table->dropColumn('skill_en');
            $table->dropColumn('language');
            $table->dropColumn('language_en');
            $table->dropColumn('slug');
            $table->dropColumn('slug_en');
        });
    }
}
