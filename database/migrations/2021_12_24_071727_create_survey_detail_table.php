<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_detail', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255)->nullable()->comment('Ten cau tra loi');
            $table->string('name_en', 255)->nullable()->comment('Ten cau tra loi en');
            $table->integer('survey_id')->nullable()->comment('map voi survey');
            $table->integer('point')->nullable()->comment('Diem cau tra loi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_detail');
    }
}
