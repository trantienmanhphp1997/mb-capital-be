<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddContentMobileToGuidelineDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::table('guideline_detail', function (Blueprint $table) {
        //     $table->dropColumn('mobile_content');
        //     $table->dropColumn('mobile_content_en');

        // });

        Schema::table('guideline_detail', function (Blueprint $table) {
            $table->string('content_mobile')->nullable()->comment('Nội dung cho mobile');
            $table->string('content_mobile_en')->nullable()->comment('Nội dung cho mobile tiếng anh');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guideline_detail', function (Blueprint $table) {
        });
    }
}
