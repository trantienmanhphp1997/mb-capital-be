<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_response', function (Blueprint $table) {
            $table->id();
            $table->string('ip_address')->nullable()->comment('dia chi ip');
            $table->integer('sum_point')->nullable()->comment('tong diem');
            $table->tinyInteger('rate_level_risk')->nullable()->comment('muc do rui ro');
            $table->dateTime('request_date')->nullable()->comment('thoi gian nop');
            $table->string('device')->nullable()->comment('thiet bi');
            $table->string('browser')->nullable()->comment('trinh duyet');
            $table->integer('save_time')->nullable()->comment('so lan luu');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_response');
    }
}
