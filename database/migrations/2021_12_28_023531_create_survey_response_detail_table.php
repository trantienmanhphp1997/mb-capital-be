<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveyResponseDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_response_detail', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('survey_response_id')->nullable()->comment('map voi survey_response');
            $table->bigInteger('survey_id')->nullable()->comment('map voi survey');
            $table->bigInteger('survey_details_id')->nullable()->comment('map voi survey_details');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('survey_response_detail');
    }
}
