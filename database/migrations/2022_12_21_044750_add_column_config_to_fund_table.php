<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnConfigToFundTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fund', function (Blueprint $table) {
            //
            $table->tinyInteger('enable_performance')->nullable()->default(1)->comment('Check enable tab Kết quả hoạt động');
            $table->tinyInteger('enable_info_disclosure')->nullable()->default(1)->comment('Check enable tab Công bố thông tin');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fund', function (Blueprint $table) {
            //
            $table->dropColumn(['enable_performance', 'enable_info_disclosure']);
        });
    }
}
