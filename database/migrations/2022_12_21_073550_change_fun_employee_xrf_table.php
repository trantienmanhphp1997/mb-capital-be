<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeFunEmployeeXrfTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fun_employee_xrf', function (Blueprint $table) {
            $table->dropForeign('fun_employee_xrf_fund_id_foreign');
            $table->dropForeign('fun_employee_xrf_employee_id_foreign');
            $table->foreignId('fund_id')->change()->nullable()->constrained('fund')->onDelete('cascade');
            $table->foreignId('employee_id')->change()->nullable()->constrained('employee')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fun_employee_xrf', function (Blueprint $table) {
            //
        });
    }
}
