<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_config', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('name', 500)->unique()->comment('ten thanh phan');
            $table->string('name_en', 500)->unique()->comment('ten thanh phan En');
            $table->bigInteger('menu_id')->nullable()->comment('map voi menu');
            $table->bigInteger('parent_id')->nullable()->comment('id thanh phan cha');
            $table->bigInteger('level')->nullable()->comment('cap thanh phan');
            $table->bigInteger('type')->nullable()->comment('Top hay Footer');
            $table->bigInteger('admin_id')->nullable()->comment('Nguoi tao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_config');
    }
}
