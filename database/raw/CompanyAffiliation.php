<?php
return [
    [
        'type' => 4,
        'v_key' => App\Enums\ECommon::COMPANY_AFFILIATION,
        'v_value' => 'MB Group',
        'v_value_en' => 'MB Group',
        'order_number' => 0
    ],
    [
        'type' => 4,
        'v_key' => App\Enums\ECommon::COMPANY_AFFILIATION,
        'v_value' => 'MB Bank',
        'v_value_en' => 'MB Bank',
        'order_number' => 1
    ],
    [
        'type' => 4,
        'v_key' => App\Enums\ECommon::COMPANY_AFFILIATION,
        'v_value' => 'MB Securities',
        'v_value_en' => 'MB Securities',
        'order_number' => 2
    ],
    [
        'type' => 4,
        'v_key' => App\Enums\ECommon::COMPANY_AFFILIATION,
        'v_value' => 'MIC',
        'v_value_en' => 'MIC',
        'order_number' => 3
    ],
    [
        'type' => 4,
        'v_key' => App\Enums\ECommon::COMPANY_AFFILIATION,
        'v_value' => 'MB AMC',
        'v_value_en' => 'MB AMC',
        'order_number' => 4
    ],
    [
        'type' => 4,
        'v_key' => App\Enums\ECommon::COMPANY_AFFILIATION,
        'v_value' => 'MCredit',
        'v_value_en' => 'MCredit',
        'order_number' => 5
    ],
    [
        'type' => 4,
        'v_key' => App\Enums\ECommon::COMPANY_AFFILIATION,
        'v_value' => 'MB Ageas Life',
        'v_value_en' => 'MB Ageas Life',
        'order_number' => 6
    ],
];
