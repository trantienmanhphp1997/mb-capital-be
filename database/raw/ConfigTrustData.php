<?php
return [
//    [
//        'v_key' => 'config_trust',
//        'v_value' => 'Rất thấp',
//        'v_content' => '{"Cổ phiếu": {"value":"0","color": "#FFFFFF"},"Tài sản thu nhập cố định": {"value":"100","color": "#BD9976"}}',
//        'number_value' => 8,
//        'order_number' => 1,
//        'type' => 8,
//    ],
//    [
//        'v_key' => 'config_trust',
//        'v_value' => 'Thấp',
//        'v_content' => '{"Cổ phiếu": {"value":"25","color": "#FFFFFF"},"Tài sản thu nhập cố định": {"value":"75","color": "#BD9976"}}',
//        'number_value' => 10,
//        'order_number' => 2,
//        'type' => 8,
//    ],
//    [
//        'v_key' => 'config_trust',
//        'v_value' => 'Trung bình',
//        'v_content' => '{"Cổ phiếu": {"value":"50","color": "#FFFFFF"},"Tài sản thu nhập cố định": {"value":"50","color": "#BD9976"}}',
//        'number_value' => 12,
//        'order_number' => 3,
//        'type' => 8,
//    ],
//    [
//        'v_key' => 'config_trust',
//        'v_value' => 'Cao',
//        'v_content' => '{"Cổ phiếu": {"value":"75","color": "#FFFFFF"},"Tài sản thu nhập cố định": {"value":"25","color": "#BD9976"}}',
//        'number_value' => 15,
//        'order_number' => 4,
//        'type' => 8,
//    ],
//    [
//        'v_key' => 'config_trust',
//        'v_value' => 'Rất cao',
//        'v_content' => '{"Cổ phiếu": {"value":"100","color": "#FFFFFF"},"Tài sản thu nhập cố định": {"value":"0","color": "#BD9976"}}',
//        'number_value' => 20,
//        'order_number' => 5,
//        'type' => 8,
//    ],
    [
        'v_key' => 'rate_level_risk',
        'v_value' => 'Rất thấp',
        'note'=>'Quý khách rất sợ rủi ro. Danh mục đầu tư của Quý khách nên được phân bổ theo hướng giúp bảo toàn vốn và đầu tư theo (a)
                        chiến lược có mức độ rủi ro rất thấp và tính thanh khoản cao đã được chứng minh trong quá khứ. Quý khách nhận ra rằng, mặc dù có sự bảo toàn vốn,
                        giá trị tương đối của khoản vốn đó có thể bị giảm đi do yếu tố lạm phát (giá trị thực của khoản đầu tư của Quý khách sẽ giảm theo thời gian).',
        'v_content' => '{
                            "survey_text": {
                                "1": {
                                    "name": "Đầu tư vào các công cụ tiền tệ & trái phiếu/ quỹ trái phiếu",
                                    "value": "100%"
                                }
                            },
                            "survey_chart": {
                                "1": {
                                    "name": "Đầu tư vào các công cụ tiền tệ & trái phiếu/ quỹ trái phiếu",
                                    "value": "100"
                                }
                            }
                        }',
        'number_value' => 8,
        'order_number' => 1,
        'type' => 25,
    ],
    [
        'v_key' => 'rate_level_risk',
        'v_value' => 'Thấp',
        'note'=>'Quý khách là người thận trọng với rủi ro. Quý khách không dễ dàng chấp nhận đầu tư rủi ro nhưng sẵn sàng chấp nhận rủi ro ở một mức độ hạn chế. Danh mục đầu tư của Quý khách nên được cấu trúc theo chiến lược giúp hạn chế khả năng mất vốn đã được chứng minh trong quá. Quý khách sẵn sàng chấp nhận một mức lợi nhuận thấp để đổi lấy sự an tâm.',
        'v_content' => '{
    "survey_text": {
        "1": {
            "name": "Đầu tư vào cổ phiếu/quỹ cổ phiếu & các sản phẩm cấu trúc",
            "value": "0%-20%"
        },
        "2": {
            "name": "Đầu tư vào các công cụ tiền tệ & trái phiếu/ quỹ trái phiếu",
            "value": "80% -100%"
        }
    },
    "survey_chart": {
        "1": {
            "name": "Đầu tư vào cổ phiếu/quỹ cổ phiếu & các sản phẩm cấu trúc",
            "value": "20"
        },
        "2": {
            "name": "Đầu tư vào các công cụ tiền tệ & trái phiếu/ quỹ trái phiếu",
            "value": "80"
        }
    }
}',
        'number_value' => 16,
        'order_number' => 2,
        'type' => 25,
    ],
    [
        'v_key' => 'rate_level_risk',
        'v_value' => 'Trung bình',
        'note'=>"Quý khách có thể chấp nhận rủi ro một cách vừa phải. Danh mục đầu tư của Quý khách nên được cấu trúc theo chiến lược tập trung với khả năng thua lỗ vừa phải. Đối với Quý khách, việc chấp nhận một mức rủi ro đã được tính toán là có thể để đạt được mức lợi nhuận tốt hơn.",
        'v_content' => '{
    "survey_text": {
        "1": {
            "name": "Đầu tư vào các công cụ tiền tệ & trái phiếu/ quỹ trái phiếu",
            "value": "40% - 60%"
        },
        "2": {
            "name": "Đầu tư vào cổ phiếu/quỹ cổ phiếu",
            "value": "40% - 60%"
        }
    },
    "survey_chart": {
        "1": {
            "name": "Đầu tư vào các công cụ tiền tệ & trái phiếu/ quỹ trái phiếu",
            "value": "50"
        },
        "2": {
            "name": "Đầu tư vào cổ phiếu/quỹ cổ phiếu",
            "value": "50"
        }
    }
}',
        'number_value' => 24,
        'order_number' => 3,
        'type' => 25,
    ],
    [
        'v_key' => 'rate_level_risk',
        'v_value' => 'Cao',
        'note'=>'Quý khách sẵn sang chấp nhận rủi ro ở mức cao. Danh mục đầu tư của Quý khách nên được cấu trúc theo chiến lược cho phép tăng trưởng vốn trong trung và dài hạn. Quý khách đang tìm kiếm khoản đầu tư có lợi nhuận cao hơn mức trung bình và kết quả là Quý khách sẵn sàng chấp nhận những biến động đáng kể đối với giá trị khoản đầu tư mà mình bỏ ra.',
        'v_content' => '{
    "survey_text": {
        "1": {
            "name": "Đầu tư vào các công cụ tiền tệ & trái phiếu/ quỹ trái phiếu",
            "value": "0% - 20%"
        },
        "2": {
            "name": "Đầu tư vào cổ phiếu/quỹ cổ phiếu",
            "value": "80% - 100%"
        }
    },
    "survey_chart": {
        "1": {
            "name": "Đầu tư vào các công cụ tiền tệ & trái phiếu/ quỹ trái phiếu",
            "value": "20"
        },
        "2": {
            "name": "Đầu tư vào cổ phiếu/quỹ cổ phiếu",
            "value": "80"
        }
    }
}',
        'number_value' => 32,
        'order_number' => 4,
        'type' => 25,
    ],
    [
        'v_key' => 'rate_level_risk',
        'v_value' => 'Rất cao',
        'note'=>'Quý khách là người cực kỳ mạo hiểm. Danh mục đầu tư của Quý khách được phân bổ theo chiến lược giúp tăng trưởng giá trị khoản đầu tư mà không đa dạng hóa về mặt rủi ro liên quan đến các yếu tố kinh tế chính. Quý khách sẵn sàng chấp nhận sự biến động đáng kể về giá trị khoản đầu tư để có được mức lợi nhuận tiềm năng lớn hơn. Quý khách có khả năng phục hồi tốt sau những suy thoái không lường trước được của thị trường, vì Quý khách có thời gian đủ dài để hồi phục hoặc vì Quý khách có các khoản dự trữ vốn bên ngoài (khoản vốn mà Quý khách có thể mang đi đầu tư trong thời kỳ suy thoái).',
        'v_content' => '{"survey_text":{"1":{"name":"Đầu tư vào cổ phiếu/quỹ cổ phiếu","value": "100%"}},"survey_chart":{"1":{"name":"Đầu tư vào cổ phiếu/quỹ cổ phiếu","value": "100"}}}',
        'number_value' => 40,
        'order_number' => 5,
        'type' => 25,
    ],
];
