<?php
return [
    [
        'type' => 14,
        'v_key' => \App\Enums\ECommon::CONTENT_TO_ADVISE,
        'v_value' => '',
        'v_value_en' => '',
        'v_content' => 'Tôi cần tìm hiểu thêm về MBBOND',
        'v_content_en'=> "I need to learn more about MBBOND",
        'order_number' => 1
    ],
    [
        'type' => 14,
        'v_key' => \App\Enums\ECommon::CONTENT_TO_ADVISE,
        'v_value' => '',
        'v_value_en' => '',
        'v_content' => 'Tôi cần tìm hiểu thêm về MBVF',
        'v_content_en'=> 'I need to learn more about MBVF',
        'order_number' => 2
    ],
    [
        'type' => 14,
        'v_key' => \App\Enums\ECommon::CONTENT_TO_ADVISE,
        'v_value' => '',
        'v_value_en' => '',
        'v_content' => 'Tôi cần tìm hiểu thêm về đầu tư định kỳ MBVF (SIP)',
        'v_content_en'=> 'I need to learn more about MBVF investment (SIP)',
        'order_number' => 3
    ],
    [
        'type' => 14,
        'v_key' => \App\Enums\ECommon::CONTENT_TO_ADVISE,
        'v_value' => '',
        'v_value_en' => '',
        'v_content' => 'Tôi cần tìm hiểu thêm về Uỷ thác đầu tư cho cá nhân',
        'v_content_en'=> 'I need to learn more about Private Investment Trusts',
        'order_number' => 4
    ],
    [
        'type' => 14,
        'v_key' => \App\Enums\ECommon::CONTENT_TO_ADVISE,
        'v_value' => '',
        'v_value_en' => '',
        'v_content' => 'Doanh nghiệp tôi cần tìm hiểu về Hưu trí An Thịnh',
        'v_content_en'=> 'My business needs to learn about An Thinh Retirement',
        'order_number' => 5
    ],
    [
        'type' => 14,
        'v_key' => \App\Enums\ECommon::CONTENT_TO_ADVISE,
        'v_value' => '',
        'v_value_en' => '',
        'v_content' => 'Doanh nghiệp tôi cần tìm hiểu về Uỷ thác đầu tư cho tổ chức',
        'v_content_en'=> 'My business needs to learn about Institutional Investment Trusts',
        'order_number' => 6
    ],
];
