<?php
return [
    [
        'type' => 24,
        'v_key' => 'expected_return_value',
        'v_value' => '0<x<=6',
        'v_content' => '{"MBBOND": {"value":"100","color": "#B9CDE5"},"MBVF": {"value":"0","color": "#BD9976"}}',
        'order_number' => '6'
    ], [
        'type' => 24,
        'v_key' => 'expected_return_value',
        'v_value' => '6<x<=8',
        'v_content' => '{"MBBOND": {"value":"90","color": "#B9CDE5"},"MBVF": {"value":"10","color": "#BD9976"}}',
        'order_number' => '8'
    ], [
        'type' => 24,
        'v_key' => 'expected_return_value',
        'v_value' => '8<x<=9',
        'v_content' => '{"MBBOND": {"value":"80","color": "#B9CDE5"},"MBVF": {"value":"20","color": "#BD9976"}}',
        'order_number' => '9'
    ], [
        'type' => 24,
        'v_key' => 'expected_return_value',
        'v_value' => '9<x<=10',
        'v_content' => '{"MBBOND": {"value":"70","color": "#B9CDE5"},"MBVF": {"value":"30","color": "#BD9976"}}',
        'order_number' => '10'
    ], [
        'type' => 24,
        'v_key' => 'expected_return_value',
        'v_value' => '10<x<=11',
        'v_content' => '{"MBBOND": {"value":"50","color": "#B9CDE5"},"MBVF": {"value":"50","color": "#BD9976"}}',
        'order_number' => '11'
    ], [
        'type' => 24,
        'v_key' => 'expected_return_value',
        'v_value' => '11<x<=12',
        'v_content' => '{"MBBOND": {"value":"20","color": "#B9CDE5"},"MBVF": {"value":"80","color": "#BD9976"}}',
        'order_number' => '12'
    ], [
        'type' => 24,
        'v_key' => 'expected_return_value',
        'v_value' => '12<x<=25',
        'v_content' => '{"MBBOND": {"value":"0","color": "#B9CDE5"},"MBVF": {"value":"100","color": "#BD9976"}}',
        'order_number' => '25'
    ],
];
