<?php
return [
    [
        'type' => 3,
        'v_key' => App\Enums\ECommon::INVESTMENT_STEP,
        'v_value' => 'Tư vấn',
        'v_value_en' => 'Advise',
        'v_content' => 'NĐT liên hệ trực tiếp với MBCapital hoặc các chuyên viên tư vấn của MBBank để được hỗ trợ',
        'v_content_en'=> "Investors directly contact MBCapital or MBBank's consultants for support",
        'order_number' => 1
    ],
    [
        'type' => 3,
        'v_key' => App\Enums\ECommon::INVESTMENT_STEP,
        'v_value' => 'Mở tài khoản',
        'v_value_en' => 'Open Account',
        'v_content' => 'Nhà đầu tư thực hiện mở TK qua App MBBANK hoặc qua Website Online MBCapital',
        'v_content_en'=> 'Investors open an account via the MBBANK App or the MBCapital Online Website',
        'order_number' => 2
    ],
    [
        'type' => 3,
        'v_key' => App\Enums\ECommon::INVESTMENT_STEP,
        'v_value' => 'Đặt lệnh',
        'v_value_en' => 'Set command',
        'v_content' => 'Đặt lệnh trực tuyến qua App MBBANK hoặc qua Website MBCapital Online',
        'v_content_en'=> 'Place orders online via MBBANK App or MBCapital Online Website',
        'order_number' => 3
    ],
    [
        'type' => 3,
        'v_key' => App\Enums\ECommon::INVESTMENT_STEP,
        'v_value' => 'Chuyển tiền mua',
        'v_value_en' => 'Transfer money to buy',
        'v_content' => 'Nhà đầu tư chuyển tiền mua chứng chỉ quỹ theo hướng dẫn',
        'v_content_en'=> 'Investors transfer money to buy fund certificates according to instructions',
        'order_number' => 4
    ],
    [
        'type' => 3,
        'v_key' => App\Enums\ECommon::INVESTMENT_STEP,
        'v_value' => 'Xác nhận',
        'v_value_en' => 'Confirm',
        'v_content' => 'Nhận SMS và Email xác nhận lệnh đặt thành công',
        'v_content_en'=> 'Receive SMS and Email to confirm successful order',
        'order_number' => 5
    ],
];
