<?php
return [
    [
        'v_key' => 'organization_trust',
        'v_value' => 'Thông tin ủy thác đầu tư',
        'type' => 11,
        'note' => '<p style="color: #4C4C4E; font-family: Avert;">Trong bối cảnh cạnh tranh toàn cầu tăng nhanh và môi trường đầu tư liên tục biến đổi phức tạp, các tổ chức và định chế tài chính đang cần thêm các phương án để hoàn thành mục tiêu đầu tư, đồng thời vẫn kiểm soát rủi ro và chi phí.</p><p style="color: #4C4C4E; font-family: Avert;">Thông qua quy trình chặt chẽ và chi tiết, MB Capital sẽ làm việc với các khách hàng tổ chức để thiết kế một giải pháp đầu tư chuyên biệt, linh hoạt, được quản trị nghiêm ngặt và đáp ứng các yêu cầu của Khách hàng về lợi nhuận kì vọng, mức độ rủi ro, nhu cầu thanh khoản và đặc điểm dòng tiền.</p><p style="color: #4C4C4E; font-family: Avert;">Với mạng lưới rộng khắp, đội ngũ phân tích đầu tư chuyên nghiệp và kinh nghiệm quản lý tài  sản cho các định chế tài chính, công ty bảo hiểm , MB Capital mong muốn mang lại cho Khàng hàng tổ chức những cơ hội đầu tư hiệu quả ở tất cả các loại tài sản từ sản phẩm tiền tệ rủi ro thấp, trái phiếu đến cổ phiếu và doanh nghiệp chưa niêm yết.</p>',
        'order_number' => 1,
    ],
    [
        'v_key' => 'organization_trust',
        'v_value' => 'Thông tin liên hệ',
        'type' => 11,
        'note' => '<p style="color: #4C4C4E; font-family: Avert;"><i>Vui lòng liên hệ</i></p><h5 style="font-family: Avert-Regular; color:  #4C4C4E;">(Ông)&nbsp;<span class="fw-bolder color-second" style="font-family: Avert-Semibold; color: rgb(10, 30, 64); font-weight: bolder !important;">Giang Trung Kiên</span></h5><p class="fw-bolder color-g" style="font-family: Avert-Semibold; font-weight: bolder !important; color: color: #4C4C4E !important;">Phó tổng Giám đốc phụ trác Kinh doanh</p><p style="color:  #4C4C4E; font-family: Avert;">Email:&nbsp;<span class="fw-bolder color-second" style="font-family: Avert-Semibold; color: rgb(10, 30, 64); font-weight: bolder !important;">kien.giang@mbcapital.com.vn</span></p>',
        'order_number' => 2,
    ],
];
