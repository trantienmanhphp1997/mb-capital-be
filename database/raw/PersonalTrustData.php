<?php
return [
    [
        'v_key' => 'what_personal_trust',
        'v_value' => 'Dịch vụ ủy thác đầu tư - MB Capital Private',
        'v_content' => 'Tại sao nên Ủy thác đầu tư?',
        'note' => '<p style="color: #4C4C4E; font-family: Avert;">Quý khách đã nỗ lực tạo dựng và gia tăng tài sản, bây giờ là lúc để tài sản làm việc cho mình. MB Capital vinh dự được cung cấp dịch vụ ủy thác đầu tư cho các khách hàng cá nhân giàu có. Chúng tôi mong muốn được đồng hành và giúp Quý khách hàng đạt được các mục tiêu tài chính quan trọng nhất, thông qua cách tiếp cận và chiến lược quản lý tài sản được cá nhân hóa, linh hoạt cùng sự tư vấn kịp thời để thích ứng tối ưu với các thay đổi về nhu cầu và diễn biến thị trường.</p>',
        'type' => 12,
    ],
    [
        'v_key' => 'why_personal_trust',
        'v_value' => 'Tối ưu lợi nhuận tài sản',
        'v_content' => 'Tối ưu hóa lợi nhuận dựa trên khẩu vị rủi ro của khách hàng',
        'url' => 'assets/img/ut3.png',
        'type' => 13,
    ],
    [
        'v_key' => 'why_personal_trust',
        'v_value' => 'Minh bạch - tin cậy',
        'v_content' => 'Danh mục vận hành dưới sự giám sát của ngân hàng lưu ký và UBCKNN',
        'url' => 'assets/img/ut2.png',
        'type' => 13,
    ],
    [
        'v_key' => 'why_personal_trust',
        'v_value' => 'Chăm sóc chuyên biệt',
        'v_content' => 'Mọi yêu cầu của Quý khách sẽ được phục vụ chuyên biệt',
        'url' => 'assets/img/ut1.png',
        'type' => 13,
    ],
];
