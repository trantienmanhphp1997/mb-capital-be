<?php
return [
    [
        'title' => 'Chuyên Viên Phát Triển Kinh Doanh',
        'conttent' => "Nghiên cứu và xây dựng báo cáo cơ bản đánh giá thị trường, đánh giá về sản phẩm, dich vụ của công ty/khách hàng/ các đơn vị cung ứng dịch vụ thanh toán trên thị trường , các xu hướng thanh toán mới trên thế giới (thanh toán thẻ, di động, thanh toán điện tử…).",
        'url' => 'https://www.careerlink.vn/tim-viec-lam-tai/ha-noi/HN',
    ],
    [
        'title' => 'PHP Laravel (MVC, MySQL)',
        'conttent' => 'Thành thạo kỹ năng code HTML, CSS, JavaScript là một lợi thế.Tốt nghiệp ngành CNTT, hoặc các chuyên ngành liên quan. Có khả năng đọc hiểu các tài liệu tiếng Anh kỹ thuật. Ưu tiên ứng viên Nam, độ tuổi 9x. Có kinh nghiệm làm việc trên môi trường Linux, MongoDB, Firebase... là một lợi thế.',
        'url' => 'https://www.careerlink.vn/tim-viec-lam-tai/ha-noi/HN',
    ],
    [
        'title' => 'Senior PHP Software Engineer',
        'conttent' => 'Tham gia phát triển các dự án xây dựng, phát triển website thương mại điện tử trên nền tảng chính là Magento, Shopware. Phối hợp với team phát triển và các bộ phận liên quan để đảm bảo project được thực hiện đúng yêu cầu, đảm bảo chất lượng và deadline. Xây dựng, phát triển các phần mềm mở rộng trên nền tảng Magento. Sẵn sàng tham gia các dự án mới với nhiều thử thách của công ty. Các công việc khác theo yêu cầu.',
        'url' => 'https://www.careerlink.vn/tim-viec-lam-tai/ha-noi/HN',
    ],
    [
        'title' => 'Chuyên Viên Phát Triển Kinh Doanh',
        'conttent' => "Nghiên cứu và xây dựng báo cáo cơ bản đánh giá thị trường, đánh giá về sản phẩm, dich vụ của công ty/khách hàng/ các đơn vị cung ứng dịch vụ thanh toán trên thị trường , các xu hướng thanh toán mới trên thế giới (thanh toán thẻ, di động, thanh toán điện tử…).",
        'url' => 'https://www.careerlink.vn/tim-viec-lam-tai/ha-noi/HN',
    ],
    [
        'title' => 'PHP Laravel (MVC, MySQL)',
        'conttent' => 'Thành thạo kỹ năng code HTML, CSS, JavaScript là một lợi thế.Tốt nghiệp ngành CNTT, hoặc các chuyên ngành liên quan. Có khả năng đọc hiểu các tài liệu tiếng Anh kỹ thuật. Ưu tiên ứng viên Nam, độ tuổi 9x. Có kinh nghiệm làm việc trên môi trường Linux, MongoDB, Firebase... là một lợi thế.',
        'url' => 'https://www.careerlink.vn/tim-viec-lam-tai/ha-noi/HN',
    ],
    [
        'title' => 'Senior PHP Software Engineer',
        'conttent' => 'Tham gia phát triển các dự án xây dựng, phát triển website thương mại điện tử trên nền tảng chính là Magento, Shopware. Phối hợp với team phát triển và các bộ phận liên quan để đảm bảo project được thực hiện đúng yêu cầu, đảm bảo chất lượng và deadline. Xây dựng, phát triển các phần mềm mở rộng trên nền tảng Magento. Sẵn sàng tham gia các dự án mới với nhiều thử thách của công ty. Các công việc khác theo yêu cầu.',
        'url' => 'https://www.careerlink.vn/tim-viec-lam-tai/ha-noi/HN',
    ],
];