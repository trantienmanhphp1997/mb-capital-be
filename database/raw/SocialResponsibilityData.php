<?php
return [
    [
        'v_key' => 'social_responsibility',
        'v_value' => 'Trách nhiệm xã hội',
        'type' => 9,
        'note' => '<div class="slogan text-center">
        <h3 class="mt-md-5">MB Capital cam kết</h3>
        <p class="mt-4">Trách Nhiệm Xã Hội là một phần trong hoạt động kinh doanh hàng ngày để kiến tạo những giá trị tốt đẹp, góp phần mang lại lợi ích chung cho xã hội 
        </p>
      </div>
      <div class="env-characteristics d-flex mt-5">
        <div class="env-item text-center" data-aos="zoom-in-down" data-aos-delay="200">
          <img src="assets/icon/customer.png" alt="customer">
          <div class="env-title mt-3">
            <p class="fw-bolder color-second">Khách hàng</p>
          </div>
          <div class="env-description mt-4">
            <p>Cung cấp những sản phẩm dịch vụ tài chính chất lượng, vì lợi ích khách hàng.
            </p>
          </div>
        </div>
        <div class="env-item text-center" data-aos="zoom-in-down" data-aos-delay="400">
          <img src="assets/icon/government.png" alt="government">
          <div class="env-title mt-3">
            <p class="fw-bolder color-second">Pháp lý</p>
          </div>
          <div class="env-description mt-4">
            <p>Tuân thủ các chính sách, quy định của Nhà nước và các quy định của ngành.
            </p>
          </div>
        </div>
        <div class="env-item text-center" data-aos="zoom-in-down" data-aos-delay="600">
          <img src="assets/icon/community.png" alt="community">
          <div class="env-title mt-3">
            <p class="fw-bolder color-second">Cộng đồng</p>
          </div>
          <div class="env-description mt-4">
            <p>Chia sẻ giá trị và có trách nhiệm chung tay phát triển vì cộng đồng.</p>
          </div>
        </div>
      </div>',
    ],
];