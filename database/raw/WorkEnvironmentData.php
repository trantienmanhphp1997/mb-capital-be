<?php
return [
    [
        'v_key' => 'work_environment',
        'v_value' => 'Môi trường làm việc',
        'type' => 10,
        'v_content' => 'Tuyển dụng',
        'note' => '<div class="slogan text-center">
        <h3 class="mt-md-5">"Vững niềm tin bền giá trị"</h3>
        <p class="mt-4">Song hành với quá trình hình thành và phát triển của doanh nghiệp, MB Capital luôn tự hào về
          văn hóa doanh nghiệp mang bản sắc riêng của
          Công ty bên cạnh những thành tựu đã đạt được trong hoạt động sản xuất kinh doanh</p>
      </div>
      <div class="env-characteristics d-flex justify-content-between mt-5">
        <div class="env-item text-center" data-aos="zoom-in-down" data-aos-delay="200">
          <img src="assets/icon/works.png" alt="works">
          <div class="env-title mt-3">
            <p class="fw-bolder color-second">Môi trường làm việc chuyên nghiệp</p>
          </div>
          <div class="env-description mt-4">
            <p>Tại MB Capital, con người chính là tài sản quý giá nhất, là nền tảng cho sự phát triển của công ty. MB
              Capital luôn quan tâm, đầu tư vào việc phát triển con người, xem đây cũng là một trong những chiến lược
              phát triển lâu dài của công ty </p>
          </div>
        </div>
        <div class="env-item text-center" data-aos="zoom-in-down" data-aos-delay="400">
          <img src="assets/icon/career-advancement.png" alt="career-advancement">
          <div class="env-title mt-3">
            <p class="fw-bolder color-second">Cơ hội thăng tiến nghề nghiệp</p>
          </div>
          <div class="env-description mt-4">
            <p>Ghi nhận những đóng góp của CBNV là điều MB Capital đặc biệt quan tâm, thông qua mức lương thưởng hấp dẫn đánh giá đúng hiệu quả làm việc của từng thành viên.</p>
          </div>
        </div>
        <div class="env-item text-center" data-aos="zoom-in-down" data-aos-delay="600">
          <img src="assets/icon/welfare.png" alt="welfare">
          <div class="env-title mt-3">
            <p class="fw-bolder color-second">Chế độ phúc lợi tốt</p>
          </div>
          <div class="env-description mt-4">
            <p>Bên cạnh các quyền lợi nhân viên được hưởng theo quy định của Pháp luật, MB Capital luôn có các các chính sách phúc lợi và đãi ngộ riêng phù hợp để thu hút và giữ chân nhân tài.</p>
          </div>
        </div>
      </div>',
    ],
];