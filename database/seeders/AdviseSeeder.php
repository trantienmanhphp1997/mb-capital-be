<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker;
use Illuminate\Support\Facades\DB;

class AdviseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 50;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('advise_list')->insert([
                'name' => $faker->name,
                'email_phone' => $faker->email,
                'advise_content' => $faker->text,
                'IP' => $faker->randomNumber(),
                'request_time' => \Carbon\Carbon::now('Asia/Ho_Chi_Minh'),
                'created_at' => \Carbon\Carbon::now('Asia/Ho_Chi_Minh')
            ]);
        }
    }
}
