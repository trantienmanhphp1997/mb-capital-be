<?php

namespace Database\Seeders;

use App\Models\SurveyDetail;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //\App\Models\User::factory(1)->create();

        $this->call([

            // ProductSeeder::class,
//            CreateAdminSeeder::class,
//            RecruitmentSeeder::class,
//            PersonalTrustSeeder::class,
//            WorkEnvironmentSeeder::class,
//            OrganizationTrustSeeder::class,
            ConfigTrustSeeder::class,
//            AboutSeeder::class,
//            SocialResponsibilitySeeder::class,
//            jambfSeeder::class
            // FundMasterSeeder::class
            // AdviseSeeder::class
            // MasterDataSeeder::class
            // FunMasterDataSeeder::class
//             SurveySeeder::class,
//             SurveyDetailSeeder::class
        ]);
    }
}
