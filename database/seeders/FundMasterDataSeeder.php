<?php

namespace Database\Seeders;

use App\Models\FundMaster;
use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FundMasterDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //--------Seed dữ liệu cho trang chủ---------------------------------------------------------------------------------
        //Tại sao nên đầu tư MbBond
        /*DB::table('fund_master')->insert([
            'type' => 1,
            'v_key' => 'investment_reason1',
            'order_number' => 1,
            'title' => 'Lợi nhuận vượt trội',
            'title_en' => '',
            'content' => 'Lên đến 8%/năm',
            'content_en' => '',
            'url' => 'https://link_to_investment_reason1',
        ]);
        DB::table('fund_master')->insert([
            'type' => 1,
            'v_key' => 'investment_reason2',
            'order_number' => 2,
            'title' => 'Sản phẩm của MB Group',
            'title_en' => '',
            'content' => 'Sản phẩm hợp tác giữa MB Group và công ty thành viên MB Capital',
            'content_en' => '',
            'url' => 'https://link_to_investment_reason2',
        ]);
        DB::table('fund_master')->insert([
            'type' => 1,
            'v_key' => 'investment_reason3',
            'order_number' => 3,
            'title' => 'An toàn',
            'title_en' => '',
            'content' => 'Quỹ MBBOND chỉ đầu tư vào các tài sản có thu nhập cố định như Trái phiếu DN, Chứng chỉ tiền gửi và tiền gửi',
            'content_en' => '',
            'url' => 'https://link_to_investment_reason3',
        ]);
        DB::table('fund_master')->insert([
            'type' => 1,
            'v_key' => 'investment_reason4',
            'order_number' => 4,
            'title' => 'Linh hoạt',
            'title_en' => '',
            'content' => 'NĐT có thể rút vốn bất kỳ lúc nào và hưởng lãi suất thực theo thời gian nắm',
            'content_en' => '',
            'url' => 'https://link_to_investment_reason4',
        ]);
        DB::table('fund_master')->insert([
            'type' => 1,
            'v_key' => 'investment_reason5',
            'order_number' => 5,
            'title' => 'Thanh khoản cao',
            'title_en' => '',
            'content' => 'T+1: Quy trình giao dịch chỉ mất 1 ngày, nhanh nhất trên thị trường',
            'content_en' => '',
            'url' => 'https://link_to_investment_reason5',
        ]);*/

        //Lợi nhuận kì vọng MbBond
        DB::table('fun_expected_profit')->insert([
            'fund_id' => 3,
            'period' => 1,
            'period_name' => '0-29 ngày',
            'period_name_en' => '',
            'percent' => -1.71,
            'period_time_min' => 0,
            'period_time_max' => 29
        ]);
        DB::table('fun_expected_profit')->insert([
            'fund_id' => 3,
            'period' => 1,
            'period_name' => '1 tháng',
            'period_name_en' => '',
            'percent' => -1.71,
            'period_time_min' => 30,
            'period_time_max' => 59
        ]);
        DB::table('fun_expected_profit')->insert([
            'fund_id' => 3,
            'period' => 2,
            'period_name' => '2 tháng',
            'period_name_en' => '',
            'percent' => 1.81,
            'period_time_min' => 60,
            'period_time_max' => 89
        ]);
        DB::table('fun_expected_profit')->insert([
            'fund_id' => 3,
            'period' => 3,
            'period_name' => '3 tháng',
            'period_name_en' => '',
            'percent' => 4.85,
            'period_time_min' => 90,
            'period_time_max' => 119
        ]);
        DB::table('fun_expected_profit')->insert([
            'fund_id' => 3,
            'period' => 4,
            'period_name' => '4 tháng',
            'period_name_en' => '',
            'percent' => 4.95,
            'period_time_min' => 120,
            'period_time_max' => 159
        ]);
        DB::table('fun_expected_profit')->insert([
            'fund_id' => 3,
            'period' => 5,
            'period_name' => '5 tháng',
            'period_name_en' => '',
            'percent' => 5.00,
            'period_time_min' => 150,
            'period_time_max' => 179
        ]);
        DB::table('fun_expected_profit')->insert([
            'fund_id' => 3,
            'period' => 6,
            'period_name' => '6 tháng',
            'period_name_en' => '',
            'percent' => 5.10,
            'period_time_min' => 180,
            'period_time_max' => 209
        ]);
        DB::table('fun_expected_profit')->insert([
            'fund_id' => 3,
            'period' => 7,
            'period_name' => '7 tháng',
            'period_name_en' => '',
            'percent' => 5.15,
            'period_time_min' => 210,
            'period_time_max' => 239
        ]);
        DB::table('fun_expected_profit')->insert([
            'fund_id' => 3,
            'period' => 8,
            'period_name' => '8 tháng',
            'period_name_en' => '',
            'percent' => 5.28,
            'period_time_min' => 240,
            'period_time_max' => 269
        ]);
        DB::table('fun_expected_profit')->insert([
            'fund_id' => 3,
            'period' => 9,
            'period_name' => '9 tháng',
            'period_name_en' => '',
            'percent' => 5.35,
            'period_time_min' => 270,
            'period_time_max' => 299
        ]);
        DB::table('fun_expected_profit')->insert([
            'fund_id' => 3,
            'period' => 10,
            'period_name' => '10 tháng',
            'period_name_en' => '',
            'percent' => 5.42,
            'period_time_min' => 300,
            'period_time_max' => 329
        ]);
        DB::table('fun_expected_profit')->insert([
            'fund_id' => 3,
            'period' => 11,
            'period_name' => '11 tháng',
            'period_name_en' => '',
            'percent' => 5.48,
            'period_time_min' => 330,
            'period_time_max' => 359
        ]);
        DB::table('fun_expected_profit')->insert([
            'fund_id' => 3,
            'period' => 12,
            'period_name' => '12 tháng',
            'period_name_en' => '',
            'percent' => 5.50,
            'period_time_min' => 360
        ]);

        //--------Seed dữ liệu cho ban đại diện quỹ MB bond---------------------------------------------------------------------------------

        /*DB::table('employee')->insert([
            'id' => 1,
            'firstname' => 'Phú',
            'lastname' => 'Vũ Hồng',
            'fullname' => 'Vũ Hồng Phú',
            'sex' => '1',
            'title' => '',
            'type' => 1
        ]);
        DB::table('fun_employee_xrf')->insert([
            'fund_id' => 3,
            'employee_id' => 1,
            'role_name' => 'Chủ tịch Ban đại diện',
        ]);

        DB::table('employee')->insert([
            'id' => 2,
            'firstname' => 'Hà',
            'lastname' => 'Nguyễn Việt',
            'fullname' => 'Nguyễn Việt Hà',
            'sex' => '1',
            'title' => '',
            'type' => 1
        ]);
        DB::table('fun_employee_xrf')->insert([
            'fund_id' => 3,
            'employee_id' => 2,
            'role_name' => 'Thành viên',
        ]);


        DB::table('employee')->insert([
            'id' => 3,
            'firstname' => 'Thúy',
            'lastname' => 'Lâm Thị Minh',
            'fullname' => 'Lâm Thi Minh Thúy',
            'sex' => '0',
            'title' => '',
            'type' => 1
        ]);
        DB::table('fun_employee_xrf')->insert([
            'fund_id' => 3,
            'employee_id' => 3,
            'role_name' => 'Thành viên',
        ]);


        DB::table('employee')->insert([
            'id' => 4,
            'firstname' => 'Xuyến',
            'lastname' => 'Nguyễn Thị',
            'fullname' => 'Nguyễn Thị Xuyến',
            'sex' => '0',
            'title' => '',
            'type' => 1
        ]);
        DB::table('fun_employee_xrf')->insert([
            'fund_id' => 3,
            'employee_id' => 4,
            'role_name' => 'Thành viên',
        ]);

        //--------Seed dữ liệu cho ban đại diện quỹ MBVF---------------------------------------------------------------------------------
        DB::table('employee')->insert([
            'id' => 5,
            'firstname' => 'Bé',
            'lastname' => 'Lê Văn',
            'fullname' => 'Lê Văn Bé',
            'sex' => '1',
            'title' => '',
            'type' => 1
        ]);
        DB::table('fun_employee_xrf')->insert([
            'fund_id' => 2,
            'employee_id' => 5,
            'role_name' => 'Chủ tịc ban đại diện',
        ]);


        DB::table('employee')->insert([
            'id' => 6,
            'firstname' => 'Trung',
            'lastname' => 'Lương Văn',
            'fullname' => 'Lương Văn Trung',
            'sex' => '1',
            'title' => '',
            'type' => 1
        ]);
        DB::table('fun_employee_xrf')->insert([
            'fund_id' => 2,
            'employee_id' => 6,
            'role_name' => 'Thành viên',
        ]);

        DB::table('employee')->insert([
            'id' => 7,
            'firstname' => 'Dương',
            'lastname' => 'Đào Thùy',
            'fullname' => 'Đào Thùy Dương',
            'sex' => '0',
            'title' => '',
            'type' => 1
        ]);
        DB::table('fun_employee_xrf')->insert([
            'fund_id' => 2,
            'employee_id' => 7,
            'role_name' => 'Thành viên',
        ]);

        DB::table('employee')->insert([
            'id' => 8,
            'firstname' => 'Yến',
            'lastname' => 'Lê Hoàng',
            'fullname' => 'Lê Hoàng Yến',
            'sex' => '0',
            'title' => '',
            'type' => 1
        ]);
        DB::table('fun_employee_xrf')->insert([
            'fund_id' => 2,
            'employee_id' => 8,
            'role_name' => 'Thành viên',
        ]);*/
        //--------Seed dữ liệu cho mb bond---------------------------------------------------------------------------------

        //---Seed dữ liệu cho mb hưu trí
        DB::table('fund_master')->insert(
            [
                'fund_id'=>4,
                'type' => 4,
                'v_key' => '',
                'order_number' => 1,
                'title' => 'Thời gian giao dịch',
                'title_en' => '',
                'note' => '<section id="skills" class="skills transaction-time mt-5">
          <div class="" data-aos="fade-up">
            <div class="paragraph item-three ">
              <section class="ps-timeline-sec">
                <div class="container">
                  <h3 class="title-bond">Thời gian giao dịch</h3>
                  <ol class="ps-timeline">
                    <li data-aos="zoom-in-up" data-aos-delay="200">
                      <div class="img-handler-top">

                      </div>
                      <div class="ps-bot">
                        <p>Tháng</p>
                      </div>
                      <span class="ps-sp-top">
                      </span>
                    </li>
                    <li data-aos="zoom-in-up" data-aos-delay="200">
                      <div class="img-handler-top">

                      </div>
                      <div class="ps-bot">
                        <p>Thứ 05 lần 1</p>
                      </div>
                      <span class="ps-sp-top"></span>
                    </li>
                    <li data-aos="zoom-in-up" data-aos-delay="200">
                      <div class="img-handler-top">
                        <div class="content-step active-trans d-flex">
                          <p>Giao dịch</p>
                        </div>
                      </div>
                      <div class="ps-bot active-trans">
                        <p>Thứ 05 lần 2</p>
                      </div>
                      <span class="ps-sp-top transaction"></span>
                    </li>
                    <li data-aos="zoom-in-up" data-aos-delay="200">
                      <div class="img-handler-top">

                      </div>
                      <div class="ps-bot">
                        <p>Thứ 05 lần 3</p>
                      </div>
                      <span class="ps-sp-top"></span>
                    </li>
                    <li data-aos="zoom-in-up" data-aos-delay="200">
                      <div class="img-handler-top">
                        <div class="content-step active-trans d-flex">
                          <p>Giao dịch</p>
                        </div>
                      </div>
                      <div class="ps-bot active-trans">
                        <p>Thứ 05 lần 4</p>
                      </div>
                      <span class="ps-sp-top transaction"></span>
                    </li>
                  </ol>
                </div>
              </section>
            </div>
          </div>
        </section>',
                'content_en' => '',
                'url' => '',
                'v_key_en' => '',
        ],);
        DB::table('fund_master')->insert(
            [
                'fund_id'=>4,
                'type' => 5,
                'v_key' => '',
                'order_number' => 1,
                'title' => 'Quá trình tham gia',
                'title_en' => '',
                'note' => '<section id="skills" class="skills participation-process mt-5">
           <div class="" data-aos="fade-up">
             <div class="paragraph item-three ">
               <section class="ps-timeline-sec">
                 <div class="container">
                   <h3 class="title-bond">Quy trình tham gia</h3>
                   <ol class="ps-timeline">
                     <li data-aos="zoom-in-up" data-aos-delay="200">
                       <div class="img-handler-top">
                         <div class="content-step d-flex">
                           <div class="step-number">
                             <p>1</p>
                           </div>
                           <div class="content-s">
                             <p>MBCapital tư vấn xây dựng chương trình Hưu trí cho <span class="fw-bolder">doanh nghiệp</span></p>
                           </div>
                         </div>
                       </div>
                       <div class="ps-bot">
                       </div>
                       <span class="ps-sp-top"></span>
                     </li>
                     <li data-aos="zoom-in-up" data-aos-delay="200">
                       <div class="img-handler-top">
                         <div class="content-step d-flex">
                           <div class="step-number">
                             <p>2</p>
                           </div>
                           <div class="content-s">
                             <p><span class="fw-bolder">Doanh nghiệp</span> ký hợp đồng tham gia chương trình Hưu trí với MBCapital</p>
                           </div>
                         </div>
                       </div>
                       <div class="ps-bot">
                       </div>
                       <span class="ps-sp-top"></span>
                     </li>
                     <li data-aos="zoom-in-up" data-aos-delay="200">
                       <div class="img-handler-top">
                         <div class="content-step d-flex">
                           <div class="step-number">
                             <p>3</p>
                           </div>
                           <div class="content-s">
                             <p><span class="fw-bolder">Doanh nghiệp</span> thu thập thông tin CBNV</p>
                           </div>
                         </div>
                       </div>
                       <div class="ps-bot">
                       </div>
                       <span class="ps-sp-top"></span>
                     </li>
                     <li data-aos="zoom-in-up" data-aos-delay="200">
                       <div class="img-handler-top">
                         <div class="content-step d-flex">
                           <div class="step-number">
                             <p>4</p>
                           </div>
                           <div class="content-s">
                             <p>MBCapital mở tài khoản Hưu trí cho CBNV</p>
                           </div>
                         </div>
                       </div>
                       <div class="ps-bot">
                       </div>
                       <span class="ps-sp-top"></span>
                     </li>
                     <li data-aos="zoom-in-up" data-aos-delay="200">
                       <div class="img-handler-top">
                         <div class="content-step d-flex">
                           <div class="step-number">
                             <p>5</p>
                           </div>
                           <div class="content-s">
                             <p><span class="fw-bolder">Doanh nghiệp</span> chốt danh sách và chuyển tiền đóng góp Quỹ</p>
                           </div>
                         </div>
                       </div>
                       <div class="ps-bot">
                       </div>
                       <span class="ps-sp-top"></span>
                     </li>
                   </ol>
                 </div>
               </section>
             </div>
           </div>
         </section>',
                'content_en' => '',
                'url' => '',
                'v_key_en' => '',
        ],
        );

        FundMaster::create([
            'fund_id' => '5',
            'title' => 'Cơ cấu danh mục',
            'content' =>'{"Tài sản thu nhập cố định": {"value":"65","color": "#B9CDE5"}, "Quỹ đầu tư cổ phiếu": {"value":"35","color": "#0A1E40"}}',
            'type' =>'8',
            'img' => '',
            'v_key' => '',
            'v_key_en' => '',
            'order_number' => 1,
            'number_value' => 0.00,
        ]);

        FundMaster::create([
            'fund_id' => '6',
            'title' => 'Cơ cấu danh mục',
            'content' =>'{"Tài sản thu nhập cố định": {"value":"50","color": "#B9CDE5"},"Quỹ đầu tư cổ phiếu": {"value":"50","color": "#0A1E40"}}',
            'type' =>'8',
            'img' => '',
            'v_key' => '',
            'v_key_en' => '',
            'order_number' => 1,
            'number_value' => 0.00,
        ]);
    }
}
