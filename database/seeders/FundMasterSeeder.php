<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker;
use Illuminate\Support\Facades\DB;
use App\Models\Fund;

class FundMasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 50;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('fund_master')->insert([
                'fund_id' => Fund::pluck('id')->random(),
                'title' => $faker->name,
                'content' => $faker->text,
                'type' => $faker->randomNumber('1,2,3,4,5'),
                'img' => $faker->imageUrl(),
                'url' => $faker->url(),
                'v_key' => $faker->name,
                'v_key_en' => $faker->name,
                'number_value' => $faker->randomNumber() 
            ]);
        }
    }
}
