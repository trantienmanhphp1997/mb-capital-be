<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use DateTime;
use Illuminate\Support\Str;

class GuidelineDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('guideline_detail')->insert([
            'name' => 'bước 1',
            'name_en' => 'step 1',
            'guideline_id' => 1,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'status' => 1,
            'content' => Str::random(100),
            'content_en' => Str::random(100)
        ]);
        DB::table('guideline_detail')->insert([
            'name' => 'bước 2',
            'name_en' => 'step 2',
            'guideline_id' => 1,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'status' => 1,
            'content' => Str::random(100),
            'content_en' => Str::random(100)
        ]);
        DB::table('guideline_detail')->insert([
            'name' => 'bước 3',
            'name_en' => 'step 3',
            'guideline_id' => 1,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'status' => 1,
            'content' => Str::random(100),
            'content_en' => Str::random(100)
        ]);
        DB::table('guideline_detail')->insert([
            'name' => 'bước 4',
            'name_en' => 'step 4',
            'guideline_id' => 1,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'status' => 1,
            'content' => Str::random(100),
            'content_en' => Str::random(100)
        ]);


        //guideline_id=2
        DB::table('guideline_detail')->insert([
            'name' => 'bước 1.2',
            'name_en' => 'step 1.2',
            'guideline_id' => 2,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'status' => 1,
            'content' => Str::random(100),
            'content_en' => Str::random(100)
        ]);
        DB::table('guideline_detail')->insert([
            'name' => 'bước 2.2',
            'name_en' => 'step 2.2',
            'guideline_id' => 2,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'status' => 1,
            'content' => Str::random(100),
            'content_en' => Str::random(100)
        ]);
        DB::table('guideline_detail')->insert([
            'name' => 'bước 3.2',
            'name_en' => 'step 3.2',
            'guideline_id' => 2,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'status' => 1,
            'content' => Str::random(100),
            'content_en' => Str::random(100)
        ]);
        DB::table('guideline_detail')->insert([
            'name' => 'bước 4.2',
            'name_en' => 'step 4.2',
            'guideline_id' => 2,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'status' => 1,
            'content' => Str::random(100),
            'content_en' => Str::random(100)
        ]);
        

        //guideline_id=3

        DB::table('guideline_detail')->insert([
            'name' => 'bước 1.3',
            'name_en' => 'step 1.3',
            'guideline_id' => 3,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'status' => 1,
            'content' => Str::random(100),
            'content_en' => Str::random(100)
        ]);
        DB::table('guideline_detail')->insert([
            'name' => 'bước 2.3',
            'name_en' => 'step 2.3',
            'guideline_id' => 3,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'status' => 1,
            'content' => Str::random(100),
            'content_en' => Str::random(100)
        ]);
        DB::table('guideline_detail')->insert([
            'name' => 'bước 3.3',
            'name_en' => 'step 3.3',
            'guideline_id' => 3,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'status' => 1,
            'content' => Str::random(100),
            'content_en' => Str::random(100)
        ]);
        DB::table('guideline_detail')->insert([
            'name' => 'bước 4.3',
            'name_en' => 'step 4.3',
            'guideline_id' => 3,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'status' => 1,
            'content' => Str::random(100),
            'content_en' => Str::random(100)
        ]);

        //guideline_id=4

        DB::table('guideline_detail')->insert([
            'name' => 'bước 1.4',
            'name_en' => 'step 1.4',
            'guideline_id' => 4,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'status' => 1,
            'content' => Str::random(100),
            'content_en' => Str::random(100)
        ]);
        DB::table('guideline_detail')->insert([
            'name' => 'bước 2.4',
            'name_en' => 'step 2.4',
            'guideline_id' => 4,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'status' => 1,
            'content' => Str::random(100),
            'content_en' => Str::random(100)
        ]);
        DB::table('guideline_detail')->insert([
            'name' => 'bước 3.4',
            'name_en' => 'step 3.4',
            'guideline_id' => 4,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'status' => 1,
            'content' => Str::random(100),
            'content_en' => Str::random(100)
        ]);
        DB::table('guideline_detail')->insert([
            'name' => 'bước 4.4',
            'name_en' => 'step 4.4',
            'guideline_id' => 4,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'status' => 1,
            'content' => Str::random(100),
            'content_en' => Str::random(100)
        ]);
    }
}
