<?php

namespace Database\Seeders;

use DateTime;
use Illuminate\Database\Seeder;
use DB;
class GuidelineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('guideline')->insert([
            'name' => 'Mở tài khoản',
            'name_en' => 'Open an account',
            'type' => 1,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'status' => 1
        ]);
        DB::table('guideline')->insert([
            'name' => 'Đặt lệnh thông thường',
            'name_en' => 'Place command normally',
            'type' => 1,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'status' => 1
        ]);
        DB::table('guideline')->insert([
            'name' => 'Đặt lệnh định kỳ',
            'name_en' => 'Place command periodically',
            'type' => 1,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'status' => 1
        ]);
        DB::table('guideline')->insert([
            'name' => 'Truy vấn',
            'name_en' => 'Query',
            'type' => 1,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime(),
            'status' => 1
        ]);
    }
}
