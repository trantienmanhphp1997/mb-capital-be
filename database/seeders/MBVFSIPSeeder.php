<?php

namespace Database\Seeders;

use App\Models\Fund;
use App\Models\FundMaster;
use Illuminate\Database\Seeder;

class MBVFSIPSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mbvf = Fund::query()->where('slug', 'MBVF')->first();
        if (empty($mbvf)) {return;}

        $mbvfSip = Fund::query()->firstOrCreate([
            'slug' => 'MBVF-SIP'
        ], [
            'shortname' => 'MBVF-SIP',
            'shortname_en' => 'MBVF-SIP',
            'fullname' => 'Chương trình đầu tư định kỳ',
            'fullname_en' => 'Chương trình đầu tư định kỳ',
            'description' => 'Chương trình đầu tư định kỳ',
            'type' => $mbvf->type,
            'slug' => 'MBVF-SIP',
            'slug_en' => 'MBVF-SIP',
            'parent_id' => $mbvf->id,
            'priority' => 1
        ]);

        $data = $this->data($mbvfSip->id);
        foreach ($data as $item) {
            FundMaster::query()->create($item);
        }
    }

    public function data($mbvfSipId)
    {
        return [
            [
                'fund_id' => $mbvfSipId,
                'title' => 'Lợi nhuận hấp dẫn',
                'content' => 'Lợi nhuận kì vọng lên đến 15%/năm',
                'type' => '1',
                'img' => '/assets/icon/safe-retirement.png',
                'v_key' => 'Tạo sao nên đầu tư MBVF định kỳ (MBVF-SIP)',
                'v_key_en' => '',
                'order_number' => 1,
                'number_value' => 0.00,
            ], [
                'fund_id' => $mbvfSipId,
                'title' => 'Tận dụng lãi kép',
                'content' => 'Tận dụng được lãi kép từ sự tăng trưởng của thị trường chứng khoán',
                'type' => '1',
                'img' => '/assets/icon/safe-retirement.png',
                'v_key' => 'Tạo sao nên đầu tư MBVF định kỳ (MBVF-SIP)',
                'v_key_en' => '',
                'order_number' => 2,
                'number_value' => 0.00,
            ], [
                'fund_id' => $mbvfSipId,
                'title' => 'Hạn chế rủi ro',
                'content' => 'Không bị ảnh hưởng bởi các biến động của thị trường',
                'type' => '1',
                'img' => '/assets/icon/safe-retirement.png',
                'v_key' => 'Tạo sao nên đầu tư MBVF định kỳ (MBVF-SIP)',
                'v_key_en' => '',
                'order_number' => 3,
                'number_value' => 0.00,
            ], [
                'fund_id' => $mbvfSipId,
                'title' => 'Kỷ luật đầu tư',
                'content' => 'Cải thiện thói quen đầu tư và chi tiêu',
                'type' => '1',
                'img' => '/assets/icon/flexible.png',
                'v_key' => 'Tạo sao nên đầu tư MBVF định kỳ (MBVF-SIP)',
                'v_key_en' => '',
                'order_number' => 4,
                'number_value' => 0.00,
            ], [
                'fund_id' => $mbvfSipId,
                'title' => 'Linh hoạt thân thiện',
                'content' => 'Vốn đầu tư thấp và linh hoạt, thuận tiện',
                'type' => '1',
                'img' => '/assets/icon/flexible.png',
                'v_key' => 'Tạo sao nên đầu tư MBVF định kỳ (MBVF-SIP)',
                'v_key_en' => '',
                'order_number' => 5,
                'number_value' => 0.00,
            ], [
                'fund_id' => $mbvfSipId,
                'title' => 'Biểu giá dịch vụ',
                'content' => '
                    <table class="table table-bond table-bordered">
                        <tr>
                            <th>Phí phát hành</th>
                            <th colspan="4">Phí mua lại</th>
                            <th>Phí chuyển đổi</th>
                            <th>Thuế TNCN</th>
                            <th>Phí chuyển bán</th>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Dưới 1 tháng</td>
                            <td>Từ 6 đến dưới 12 tháng</td>
                            <td>Từ 12 đến dưới 18 tháng</td>
                            <td>Từ 18 tháng</td>
                            <td></td>
                            <td></td>
                            <td>Theo biểu phí của Ngân hàng giám sát</td>
                        </tr>
                        <tr>
                            <td>Miễn phí cho đến 31/12</td>
                            <td>1.4%</td>
                            <td>1.05%</td>
                            <td>0.5%</td>
                            <td>Miễn phí</td>
                            <td>0.3%</td>
                            <td>0.1%</td>
                            <td></td>
                        </tr>
                    </table>
                ',
                'type' => '2',
                'img' => '',
                'v_key' => '',
                'v_key_en' => '',
                'order_number' => 1,
                'number_value' => 0.00,
            ], [
                'fund_id' => $mbvfSipId,
                'title' => 'Lợi suất đầu tư',
                'content' => 'Hello',
                'type' => '3',
                'img' => '',
                'v_key' => '',
                'v_key_en' => '',
                'order_number' => 1,
                'number_value' => 0.00,
            ], [
                'fund_id' => $mbvfSipId,
                'title' => 'Thời gian giao dịch',
                'content' => 'Hello',
                'type' => '4',
                'img' => '',
                'v_key' => '',
                'v_key_en' => '',
                'order_number' => 1,
                'number_value' => 0.00,
            ], [
                'fund_id' => $mbvfSipId,
                'title' => 'Phân bổ ngành',
                'content' => '{\"Các ngành\":12,\"Trái phiếu chưa liêm yết  9.7%\":12,\"Tiền gửi có kỳ hạn và chứng chỉ tiền gửi 26.2%\":10,\"Tiền và tương đương tiền \":4}',
                'type' => '8',
                'img' => '',
                'v_key' => '',
                'v_key_en' => '',
                'order_number' => 1,
                'number_value' => 0.00,
            ], [
                'fund_id' => $mbvfSipId,
                'title' => 'Top 5 cổ phiếu có tỉ trọng cao nhất',
                'content' => '<table class=\"table table-bond table-bordered\">\r\n                <thead>\r\n                  <tr>\r\n                    <th scope=\"col\" class=\"text-center\">Loại cổ phiếu</th>\r\n                    <th scope=\"col\" class=\"text-center\">Sàn giao dịch</th>\r\n                    <th scope=\"col\" class=\"text-center\">Tên doanh nghiệp</th>\r\n                    <th scope=\"col\" class=\"text-center\">Ngành</th>\r\n                    <th scope=\"col\" class=\"text-center\">Tỷ trọng</th>\r\n                  </tr>\r\n                </thead>\r\n                <tbody>\r\n                  <tr>\r\n                    <td class=\"text-center\">VNM</td>\r\n                    <td class=\"text-center\">VNM</td>\r\n                    <td class=\"text-center\">VNM</td>\r\n                    <td class=\"text-center\">VNM</td>\r\n		<td class=\"text-center\">VNM</td>\r\n                  </tr>\r\n                </tbody>\r\n              </table>',
                'type' => '9',
                'img' => '',
                'v_key' => '',
                'v_key_en' => '',
                'order_number' => 1,
                'number_value' => 0.00,
            ], [
                'fund_id' => $mbvfSipId,
                'title' => 'Báo cáo nhà đầu tư',
                'content' => 'hello',
                'type' => '11',
                'img' => '',
                'v_key' => '',
                'v_key_en' => '',
                'order_number' => 1,
                'number_value' => 0.00,
            ],

        ];
    }
}
