<?php

namespace Database\Seeders;

use App\Models\Fund;
use App\Models\FundMaster;
use Illuminate\Database\Seeder;

class MBVFSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mbvf = Fund::query()->firstOrCreate([
            'slug' => 'MBVF'
        ], [
            'shortname' => 'MBVF',
            'shortname_en' => 'Quỹ đầu tư năm 2021',
            'fullname' => 'Quỹ đầu tư giá trị MBCAPITAL',
            'fullname_en' => 'Quỹ đầu tư giá trị MBCAPITAL',
            'description' => 'Trải nghiệm đầu tư, sinh lời vượt trội với Lợi nhuận kỳ vọng lên đến 15%/năm',
            'created_by' => 1,
            'type' => 4,
            'slug' => 'MBVF',
            'slug_en' => 'MBVF',
            'priority' => 1,
            'shortname2' => 'Quỹ MBVF',
            'shortname2_en' => 'Quỹ MBVF',
            'fullname2' => 'Quỹ đầu tư giá trị MBCAPITAL',
            'fullnam2e_en' => 'Quỹ đầu tư giá trị MBCAPITAL',
        ]);

        $data = $this->data($mbvf->id);
        foreach($data as $item) {
            FundMaster::query()->updateOrCreate([
                'fund_id' => $mbvf->id,
                'title' => $item['title'] ?? null,
                'order_number' => $item['order_number'] ?? null,
                'v_key' => $item['v_key'] ?? null,
                'type' => $item['type'] ?? null
            ], $item);
        }
    }

    public function data($fundId)
    {
        return [
            [
                'fund_id' => $fundId,
                'title' => 'Lợi nhuận hấp dẫn',
                'content' => 'Lợi nhuận kì vọng lên đến 15%/năm',
                'type' => '1',
                'img' => '/assets/icon/safe-retirement.png',
                'v_key' => 'Tạo sao nên đầu tư MBVF định kỳ (MBVF-SIP)',
                'v_key_en' => '',
                'order_number' => 1,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'Tận dụng lãi kép',
                'content' => 'Tận dụng được lãi kép từ sự tăng trưởng của thị trường chứng khoán',
                'type' => '1',
                'img' => '/assets/icon/safe-retirement.png',
                'v_key' => 'Tạo sao nên đầu tư MBVF định kỳ (MBVF-SIP)',
                'v_key_en' => '',
                'order_number' => 2,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'Hạn chế rủi ro',
                'content' => 'Không bị ảnh hưởng bởi các biến động của thị trường',
                'type' => '1',
                'img' => '/assets/icon/safe-retirement.png',
                'v_key' => 'Tạo sao nên đầu tư MBVF định kỳ (MBVF-SIP)',
                'v_key_en' => '',
                'order_number' => 3,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'Kỷ luật đầu tư',
                'content' => 'Cải thiện thói quen đầu tư và chi tiêu',
                'type' => '1',
                'img' => '/assets/icon/flexible.png',
                'v_key' => 'Tạo sao nên đầu tư MBVF định kỳ (MBVF-SIP)',
                'v_key_en' => '',
                'order_number' => 4,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'Linh hoạt thân thiện',
                'content' => 'Vốn đầu tư thấp và linh hoạt, thuận tiện',
                'type' => '1',
                'img' => '/assets/icon/flexible.png',
                'v_key' => 'Tạo sao nên đầu tư MBVF định kỳ (MBVF-SIP)',
                'v_key_en' => '',
                'order_number' => 5,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'Biểu giá dịch vụ',
                'content' => '
                <thead>
                      <tr>
                        <th scope="col" class="text-center">Loại hình</th>
                        <th scope="col" class="text-center">Tài sản đầu tư</th>
                        <th scope="col" class="text-center">Mức độ an toàn</th>
                        <th scope="col" class="text-center">Quy mô quỹ</th>
                        <th scope="col" class="text-center">Mục tiêu đầu tư</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Quỹ mở trái phiếu</td>
                        <td>Quỹ mở trái phiếu</td>
                        <td>Quỹ mở trái phiếu</td>
                        <td>@Quỹ mở trái phiếu</td>
                      </tr>
                    </tbody>
                </table>',
                'type' => '2',
                'img' => '',
                'v_key' => '',
                'v_key_en' => '',
                'order_number' => 1,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'Thời gian giao dịch',
                'content' => '
                    <ul class="investment-list">
                        <li class="size20 fw-normal">Đầu tư thông thường</li>
                        <li class="size20 fw-normal">Đầu tư thông thường</li>
                        <li class="size20 fw-normal">Đầu tư thông thường</li>
                    </ul>
                    <p class="size20">- Tần suất giao dịch: Hàng ngày (Các ngày làm việc)</p>
                    <p class="size20"> - Thời gian thanh toán: Sau ngày khớp lệnh 1 ngày làm việc</p>',
                'type' => '4',
                'img' => '',
                'v_key' => '',
                'v_key_en' => '',
                'order_number' => 1,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'Quy trình giao dịch',
                'content' => 'Đặt lệnh mua',
                'type' => 5,
                'img' => '',
                'v_key' => 'Quy trình giao dịch',
                'v_key_en' => '',
                'order_number' => 1,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'Quy trình giao dịch',
                'content' => 'Chuyển tiền',
                'type' => 5,
                'img' => '',
                'v_key' => 'Quy trình giao dịch',
                'v_key_en' => '',
                'order_number' => 2,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'Quy trình giao dịch',
                'content' => 'Ngày giao dịch (ngày T)',
                'type' => 5,
                'img' => '',
                'v_key' => 'Quy trình giao dịch',
                'v_key_en' => '',
                'order_number' => 3,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'Quy trình giao dịch',
                'content' => 'Ngày CCQ mua hoặc tiền bán về( T+1)',
                'type' => 5,
                'img' => '',
                'v_key' => 'Quy trình giao dịch',
                'v_key_en' => '',
                'order_number' => 4,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'Tổ chức cung cấp dịch vụ',
                'content' => '
                <table class="table table-services">
                    <thead>
                        <tr>
                            <th scope="col">Ngân hàng giám sát</th>
                            <th scope="col">Ngân hàng TMCP Đầu tư và Phát triển Việt Nam -Chi nhánh Hà Thành</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Quỹ mở trái phiếu</td>
                            <td>Quỹ mở trái phiếu</td>
                        </tr>
                    </tbody>
                </table>',
                'type' => 6,
                'img' => '',
                'v_key' => 'Quy trình giao dịch',
                'v_key_en' => '',
                'order_number' => 1,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'Đầu tư thông thường',
                'content' => 'Đầu tư từng lần, linh hoạt theo nhu cầu',
                'type' => 7,
                'img' => '',
                'v_key' => 'Hình thức đầu tư',
                'v_key_en' => '',
                'order_number' => 1,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'Đầu tư định kỳ MBVF-SIP',
                'content' => 'Đầu tư định kỳ hàng tháng, khớp lệnh 1 lần 1 tháng',
                'type' => 7,
                'img' => '',
                'v_key' => 'Hình thức đầu tư',
                'v_key_en' => '',
                'order_number' => 2,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'Phân bổ ngành',
                'content' => '{
                    "Các ngành": 12,
                    "Trái phiếu chưa niêm yết": 12,
                    "Tiền gửi có kỳ hạn và chứng chỉ tiền gửi": 10,
                    "Tiền và tương đương tiền": 4
                }',
                'type' => 8,
                'img' => '',
                'v_key' => 'Phân bổ ngành',
                'v_key_en' => '',
                'order_number' => 1,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'Top 5 cổ phiếu có tỷ trọng cao nhất',
                'content' => '<table class=\"table table-bond table-bordered\"><thead>                  <tr>                    <th scope=\"col\" class=\"text-center\">Loại cổ phiếu</th>                    <th scope=\"col\" class=\"text-center\">Sàn giao dịch</th>                    <th scope=\"col\" class=\"text-center\">Tên doanh nghiệp</th>                    <th scope=\"col\" class=\"text-center\">Ngành</th>                    <th scope=\"col\" class=\"text-center\">Tỷ trọng</th>                  </tr>                </thead>                <tbody>                  <tr>                    <td class=\"text-center\">VNM</td>                    <td class=\"text-center\">VNM</td>                    <td class=\"text-center\">VNM</td>                    <td class=\"text-center\">VNM</td>		<td class=\"text-center\">VNM</td>                  </tr>                </tbody>              </table>',
                'type' => 9,
                'img' => '',
                'v_key' => 'Top 5 cổ phiếu có tỷ trọng cao nhất',
                'v_key_en' => '',
                'order_number' => 1,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'Tài liệu quỹ',
                'content' => '',
                'type' => 10,
                'img' => '',
                'v_key' => 'Tài liệu quỹ',
                'v_key_en' => '',
                'order_number' => 1,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'App MBBANK',
                'content' => '',
                'url' => 'https://apps.apple.com/VN/app/id1205807363?mt=8',
                'type' => 12,
                'img' => '',
                'v_key' => 'Link truy cập',
                'v_key_en' => '',
                'order_number' => 1,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'MBCapital Online',
                'content' => '',
                'url' => 'https://online.mbcapital.com.vn/sso/login',
                'type' => 12,
                'img' => '',
                'v_key' => 'Link truy cập',
                'v_key_en' => '',
                'order_number' => 1,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'Đại lý phân phối',
                'content' => 'https://www.vndirect.com.vn/',
                'url' => 'https://www.vndirect.com.vn/',
                'type' => 13,
                'img' => 'assets/img/image6.png',
                'v_key' => 'Đại lý phân phối',
                'v_key_en' => '',
                'order_number' => 1,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'Đại lý phân phối',
                'content' => 'https://www.vdsc.com.vn/vn/home.rv',
                'url' => 'https://www.vdsc.com.vn/vn/home.rv',
                'type' => 13,
                'img' => 'assets/img/image7.png',
                'v_key' => 'Đại lý phân phối',
                'v_key_en' => '',
                'order_number' => 2,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'Đại lý phân phối',
                'content' => 'https://www.japan-sec.vn/',
                'url' => 'https://www.japan-sec.vn/',
                'type' => 13,
                'img' => 'assets/img/image10.png',
                'v_key' => 'Đại lý phân phối',
                'v_key_en' => '',
                'order_number' => 3,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'Đại lý phân phối',
                'content' => 'https://mbs.com.vn/',
                'url' => 'https://mbs.com.vn/',
                'type' => 13,
                'img' => 'assets/img/image11.png',
                'v_key' => 'Đại lý phân phối',
                'v_key_en' => '',
                'order_number' => 4,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'Đại lý phân phối',
                'content' => 'https://www.vietinbank.vn',
                'url' => 'https://www.vietinbank.vn',
                'type' => 13,
                'img' => 'assets/img/image12.png',
                'v_key' => 'Đại lý phân phối',
                'v_key_en' => '',
                'order_number' => 5,
                'number_value' => 0.00,
            ], [
                'fund_id' => $fundId,
                'title' => 'Đại lý phân phối',
                'content' => 'https://www.mbbank.com.vn/',
                'url' => 'https://www.mbbank.com.vn/',
                'type' => 13,
                'img' => 'assets/img/agent7.png',
                'v_key' => 'Đại lý phân phối',
                'v_key_en' => '',
                'order_number' => 5,
                'number_value' => 0.00,
            ],
        ];
    }
}
