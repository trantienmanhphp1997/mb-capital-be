<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\MasterData;

class MasterDBSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // $this->companyAffiliation();
        $this->investmentStep();
        $this->contentToAdvise();
    }

    protected function uniqueInsertMany($data)
    {
        foreach ($data as $item) {
            // if(!isset($item['v_value_en']) || !isset($item['v_key']) || !isset($item['v_value'])) continue;
            // MasterData::query()->updateOrCreate(
            //     [
            //         'type' => $item['type'],
            //         'v_value' => $item['v_value'],
            //         'v_key'=>$item['v_key'],
            //         'order_number'=>$item['order_number'],
            //         'parent_id'=>$item['parent_id'] ?? null,
            //     ],
            //     ['v_value_en' => $item['v_value_en']]
            // );

            MasterData::query()->firstOrCreate(
                [
                    'type' => $item['type'],
                    'v_key' => $item['v_key'] ?? '',
                    'v_value' => $item['v_value'] ?? '',
                    'v_value_en' => $item['v_value_en'] ?? '',
                    'v_content' => $item['v_content'] ?? null,
                    'v_content_en' => $item['v_content_en'] ?? null,
                    'order_number' => $item['order_number'] ?? null,
                    'parent_id' => $item['parent_id'] ?? null,
                    'image' => $item['image'] ?? null,
                    'url' => $item['url'] ?? null,
                    'note' => $item['note'] ?? null,
                    'note_en' => $item['note_en'] ?? null,
                ]
            );
        }
    }

    public function companyAffiliation()
    {
        $data = require_once(database_path('raw/CompanyAffiliation.php'));
        $this->uniqueInsertMany($data);
        echo "Seeded: CompanyAffiliation" . PHP_EOL;
    }

    public function investmentStep()
    {
        $data = require_once(database_path('raw/InvestmentStep.php'));
        $this->uniqueInsertMany($data);
        echo "Seeded: InvestmentStep" . PHP_EOL;
    }

    public function contentToAdvise()
    {
        $data = require_once(database_path('raw/ContentToAdvise.php'));
        $this->uniqueInsertMany($data);
        echo "Seeded: ContentToAdvise" . PHP_EOL;
    }
}
