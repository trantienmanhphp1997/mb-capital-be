<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MasterDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //--------Seed dữ liệu cho trang chủ---------------------------------------------------------------------------------
        //banner
        DB::table('master_data')->insert([
            'type' => 1,
            'v_key' => 'banner1',
            'v_value' => '',
            'v_value_en' => '',
            'v_content' => '',
            'v_content_en' => '',
            'url' => 'https://link_to_banner1',
        ]);
        DB::table('master_data')->insert([
            'type' => 1,
            'v_key' => 'banner2',
            'v_value' => '',
            'v_value_en' => '',
            'v_content' => '',
            'v_content_en' => '',
            'url' => 'https://link_to_banner2',
        ]);
        DB::table('master_data')->insert([
            'type' => 1,
            'v_key' => 'banner3',
            'v_value' => '',
            'v_value_en' => '',
            'v_content' => '',
            'v_content_en' => '',
            'url' => 'https://link_to_banner3',
        ]);

        //Bạn đang có nhu cầu đầu tư
        DB::table('master_data')->insert([
            'type' => 2,
            'v_key' => 'investment_demand1',
            'v_value' => 'NHÀ ĐẦU TƯ MỚI',
            'v_value_en' => '',
            'v_content' => 'Tôi muốn tham gia đầu tư',
            'v_content_en' => '',
            'url' => 'https://link_to_investment_demand_icon_1',
        ]);
        DB::table('master_data')->insert([
            'type' => 2,
            'v_key' => 'investment_demand2',
            'v_value' => 'NHÀ ĐẦU TƯ CHUYÊN NGHIỆP',
            'v_value_en' => '',
            'v_content' => 'Tôi muốn tìm kiếm cơ hội đầu tư',
            'v_content_en' => '',
            'url' => 'https://link_to_investment_demand_icon_2',
        ]);
        DB::table('master_data')->insert([
            'type' => 2,
            'v_key' => 'investment_demand3',
            'v_value' => 'KHÁCH HÀNG',
            'v_value_en' => '',
            'v_content' => 'Tôi là khách hàng của MBCapital',
            'v_content_en' => '',
            'url' => 'https://link_to_investment_demand_icon_3',
        ]);

        //--------Seed dữ liệu cho mb bond---------------------------------------------------------------------------------
    }
}
