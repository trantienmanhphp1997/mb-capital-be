<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MasterDataType20Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('master_data')->insert([
            'type' => 20,
            'v_key' => 'important_milestone',
            'order_number' => '2006',
            'note' => '• MB thành lập công ty TNHH một thành viên quản lý quỹ đầu tư chứng khoán Hà Nội (HFM). <br>
                        • Thành lập quỹ đầu tư chứng khoán Hà Nội (Hanoi Fund) với quy mô 200 tỷ đồng. Quỹ kết thúc hoạt động vào năm 2014.',
        ]);

        DB::table('master_data')->insert([
            'type' => 20,
            'v_key' => 'important_milestone',
            'order_number' => '2007',
            'note' => '• Chuyển đổi mô hình hoạt động thành công ty cổ phần <br>
                        • Thành lập quỹ đầu tư thành viên Vietnam Tiger Fund với quy mô vốn 500 tỷ đồng.Quỹ kết thúc hoạt động vào năm 2015.<br>',
        ]);

        DB::table('master_data')->insert([
            'type' => 20,
            'v_key' => 'important_milestone',
            'order_number' => '2009',
            'note' => 'Đổi tên công ty thành Công ty cổ phần quản lý quỹ đầu tư MB (MB Capital)',
        ]);

        DB::table('master_data')->insert([
            'type' => 20,
            'v_key' => 'important_milestone',
            'order_number' => '2010',
            'note' => 'Hợp tác với tập đoàn Japan Asia Holding (Nhật Bản) và thành lập Quỹ mở Vietnam Dream Fund tại Nhật và Quỹ đầu tư Japan Asia MB Capital (JAMBF).',
        ]);

        DB::table('master_data')->insert([
            'type' => 20,
            'v_key' => 'important_milestone',
            'order_number' => '2013',
            'note' => 'Thành lập quỹ đầu tư trái phiếu MB Capital Việt Nam (MBBF). Quỹ kết thúc hoạt động vào năm 2018.',
        ]);

        DB::table('master_data')->insert([
            'type' => 20,
            'v_key' => 'important_milestone',
            'order_number' => '2014',
            'note' => 'Thành lập Quỹ đầu tư giá trị MB Capital (MBVF)',
        ]);

        DB::table('master_data')->insert([
            'type' => 20,
            'v_key' => 'important_milestone',
            'order_number' => '2015',
            'note' => 'Quản lý danh mục đầu tư cho tổng công ty cổ phần Bảo hiểm Quân đội',
        ]);

        DB::table('master_data')->insert([
            'type' => 20,
            'v_key' => 'important_milestone',
            'order_number' => '2016',
            'note' => 'Nhận Bằng khen của Bộ tài chính và ủy ban chứng khoán nhà nước cho đóng góp tich cực cho ngành chứng khoán 
                        và quản lý quỹ giai đoạn 2006-2016',
        ]);

        DB::table('master_data')->insert([
            'type' => 20,
            'v_key' => 'important_milestone',
            'order_number' => '2018',
            'note' => 'Thành lập Quỹ đầu tư tăng trưởng MB Capital (MBGF)',
        ]);

        DB::table('master_data')->insert([
            'type' => 20,
            'v_key' => 'important_milestone',
            'order_number' => '2019',
            'note' => 'Quản lý danh mục đầu tư cho công ty Bảo hiểm nhân thọ MB Ages',
        ]);

        DB::table('master_data')->insert([
            'type' => 20,
            'v_key' => 'important_milestone',
            'order_number' => '2020',
            'note' => 'Thành lập quỹ đầu tư trái phiếu MB (MBBond) - chuyển đổi từ MBGF',
        ]);

        DB::table('master_data')->insert([
            'type' => 20,
            'v_key' => 'important_milestone',
            'order_number' => '2021',
            'note' => '• Khai Trương trụ sở mới tại 21 Cát Linh, Đống Đa, Hà Nội. <br>
                        • Triển khai Chương trình Hưu trí An Thịnh. <br>
                        • Phối hợp với công ty Bảo hiểm nhân thọ MB Ages ra mắt sản phẩm bảo hiểm liên kết đầu tư "Kiến tạo tương lai"',
        ]);
    }
}
