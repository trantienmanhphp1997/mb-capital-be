<?php

namespace Database\Seeders;

use App\Models\MasterData;
use Illuminate\Database\Seeder;

class MasterDataType24Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $this->expectedReturnValue();
    }

    protected function uniqueInsertMany($data)
    {
        foreach ($data as $item) {

            MasterData::query()->firstOrCreate(
                [
                    'type' => $item['type'],
                    'v_key' => $item['v_key'] ?? '',
                    'v_value' => $item['v_value'] ?? '',
                    'v_value_en' => $item['v_value_en'] ?? '',
                    'v_content' => $item['v_content'] ?? null,
                    'v_content_en' => $item['v_content_en'] ?? null,
                    'order_number' => $item['order_number'] ?? null,
                    'parent_id' => $item['parent_id'] ?? null,
                    'image' => $item['image'] ?? null,
                    'url' => $item['url'] ?? null,
                    'note' => $item['note'] ?? null,
                    'note_en' => $item['note_en'] ?? null,
                ]
            );
        }
    }

    public function expectedReturnValue()
    {
        $data = require_once(database_path('raw/ExpectedReturnValue.php'));
        $this->uniqueInsertMany($data);
        echo "Seeded: ExpectedReturnValue" . PHP_EOL;
    }
}
