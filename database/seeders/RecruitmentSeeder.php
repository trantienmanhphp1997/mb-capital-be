<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Recruitment;

class RecruitmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $data = require_once(database_path('raw/RecruitmentData.php'));
        Recruitment::query()->delete();
        foreach($data as $value){
            // dd($value);
            Recruitment::create(
                $value,
            );
        }
    }
}