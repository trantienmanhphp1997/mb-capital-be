<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
class SurveyDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('survey_detail')->insert([
            'name' => 'Ít hơn 1 năm',
            'name_en' => 'Ít hơn 1 năm',
            'survey_id' => 1,
            'point' => 1,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Từ 1 đến 2 năm',
            'name_en' => 'Từ 1 đến 2 năm',
            'survey_id' => 1,
            'point' => 2,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Từ 2 đến 5 năm',
            'name_en' => 'Từ 2 đến 5 năm',
            'survey_id' => 1,
            'point' => 3,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Từ 5 đến 7 năm',
            'name_en' => 'Từ 5 đến 7 năm',
            'survey_id' => 1,
            'point' => 4,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Trên 7 năm',
            'name_en' => 'Trên 7 năm',
            'survey_id' => 1,
            'point' => 5,
        ]);
        DB::table('survey_detail')->insert([
            'name' => '0%',
            'name_en' => '0%',
            'survey_id' => 2,
            'point' => 1,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Giảm từ 1-6%',
            'name_en' => 'Giảm từ 1-6%',
            'survey_id' => 2,
            'point' => 2,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Giảm từ 6-11%',
            'name_en' => 'Giảm từ 6-11%',
            'survey_id' => 2,
            'point' => 3,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Giảm từ 11-20%',
            'name_en' => 'Giảm từ 11-20%',
            'survey_id' => 2,
            'point' => 4,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Giảm trên 20%',
            'name_en' => 'Giảm trên 20%',
            'survey_id' => 2,
            'point' => 5,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Dưới 20%',
            'name_en' => 'Dưới 20%',
            'survey_id' => 3,
            'point' => 1,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Từ 20%-40%',
            'name_en' => 'Từ 20%-40%',
            'survey_id' => 3,
            'point' => 2,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Từ 40%-60%',
            'name_en' => 'Từ 40%-60%',
            'survey_id' => 3,
            'point' => 3,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Từ 60%-80%',
            'name_en' => 'Từ 60%-80%',
            'survey_id' => 3,
            'point' => 4,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Trên 80%',
            'name_en' => 'Trên 80%',
            'survey_id' => 3,
            'point' => 5,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Bảo toàn tài sản và sức mua',
            'name_en' => 'Bảo toàn tài sản và sức mua',
            'survey_id' => 4,
            'point' => 1,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Tạo ra thu nhập cao',
            'name_en' => 'Tạo ra thu nhập cao',
            'survey_id' => 4,
            'point' => 2,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Cân bằng giữa thu nhập và tăng trưởng tài sản',
            'name_en' => 'Cân bằng giữa thu nhập và tăng trưởng tài sản',
            'survey_id' => 4,
            'point' => 3,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Tăng trưởng tài sản cao',
            'name_en' => 'Tăng trưởng tài sản cao',
            'survey_id' => 4,
            'point' => 4,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Chấp nhận rủi ro để tạo ra tăng trưởng tài sản tối đa',
            'name_en' => 'Chấp nhận rủi ro để tạo ra tăng trưởng tài sản tối đa',
            'survey_id' => 4,
            'point' => 5,
        ]);
        DB::table('survey_detail')->insert([
            'name' => '0-10%',
            'name_en' => '0-10%',
            'survey_id' => 5,
            'point' => 1,
        ]);
        DB::table('survey_detail')->insert([
            'name' => '11-20%',
            'name_en' => '11-20%',
            'survey_id' => 5,
            'point' => 2,
        ]);
        DB::table('survey_detail')->insert([
            'name' => '21-30%',
            'name_en' => '21-30%',
            'survey_id' => 5,
            'point' => 3,
        ]);
        DB::table('survey_detail')->insert([
            'name' => '31%-40%',
            'name_en' => '31%-40%',
            'survey_id' => 5,
            'point' => 4,
        ]);DB::table('survey_detail')->insert([
            'name' => 'Hơn 40%',
            'name_en' => 'Hơn 40%',
            'survey_id' => 5,
            'point' => 5,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Bằng lãi suất tiết kiệm + 1%',
            'name_en' => 'Bằng lãi suất tiết kiệm + 1%',
            'survey_id' => 6,
            'point' => 1,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Bằng lãi suất tiết kiệm +1% đến 5%',
            'name_en' => 'Bằng lãi suất tiết kiệm +1% đến 5%',
            'survey_id' => 6,
            'point' => 2,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Bằng lãi suất tiết kiệm +5% đến 10%',
            'name_en' => 'Bằng lãi suất tiết kiệm +5% đến 10%',
            'survey_id' => 6,
            'point' => 3,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Bằng lãi suất tiết kiệm +10% đến 15%',
            'name_en' => 'Bằng lãi suất tiết kiệm +10% đến 15%',
            'survey_id' => 6,
            'point' => 4,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Bằng lãi suất tiết kiệm + hơn 15%',
            'name_en' => 'Bằng lãi suất tiết kiệm + hơn 15%',
            'survey_id' => 6,
            'point' => 5,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Bán toàn bộ danh mục',
            'name_en' => 'Bán toàn bộ danh mục',
            'survey_id' => 7,
            'point' => 1,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Bán 1 phần danh mục',
            'name_en' => 'Bán 1 phần danh mục',
            'survey_id' => 7,
            'point' => 2,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Liên hệ MBCapital để xem xét lại mức độ rủi ro',
            'name_en' => 'Liên hệ MBCapital để xem xét lại mức độ rủi ro',
            'survey_id' => 7,
            'point' => 3,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Nắm giữ danh mục, chờ giá hồi phục trở lại',
            'name_en' => 'Nắm giữ danh mục, chờ giá hồi phục trở lại',
            'survey_id' => 7,
            'point' => 4,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Mua vào để bình quân giá',
            'name_en' => 'Mua vào để bình quân giá',
            'survey_id' => 7,
            'point' => 5,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Dưới 1 tỷ',
            'name_en' => 'Dưới 1 tỷ',
            'survey_id' => 8,
            'point' => 1,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Từ 1 đến 5 tỷ',
            'name_en' => 'Từ 1 đến 5 tỷ',
            'survey_id' => 8,
            'point' => 2,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Từ 5 đến 10 tỷ',
            'name_en' => 'Từ 5 đến 10 tỷ',
            'survey_id' => 8,
            'point' => 3,
        ]);
        DB::table('survey_detail')->insert([
            'name' => 'Từ 10 tỷ đến 20 tỷ',
            'name_en' => 'Từ 10 tỷ đến 20 tỷ',
            'survey_id' => 8,
            'point' => 4,
        ]);DB::table('survey_detail')->insert([
            'name' => 'Trên 20 tỷ',
            'name_en' => 'Trên 20 tỷ',
            'survey_id' => 8,
            'point' => 5,
        ]);
    }
}
