<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class SurveySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('survey')->insert([
            'name' => 'Thời gian Quý khách dự định đầu tư',
            'name_en' => 'Thời gian Quý khách dự định đầu tư',
            'order_number' => 1,
            'multi_select' => 0,
        ]);
        DB::table('survey')->insert([
            'name' => 'Quý khách có thể chấp nhận được sự sụt giảm bao nhiêu % với danh mục đầu tư của mình',
            'name_en' => 'Quý khách có thể chấp nhận được sự sụt giảm bao nhiêu % với danh mục đầu tư của mình',
            'order_number' => 1,
            'multi_select' => 0,
        ]);
        DB::table('survey')->insert([
            'name' => ' Bao nhiêu % tổng tài sản của Quý khách được sử dụng để đầu tư',
            'name_en' => ' Bao nhiêu % tổng tài sản của Quý khách được sử dụng để đầu tư',
            'order_number' => 1,
            'multi_select' => 0,
        ]);
        DB::table('survey')->insert([
            'name' => 'Mục tiêu đầu tư của Quý khách là gì',
            'name_en' => 'Mục tiêu đầu tư của Quý khách là gì',
            'order_number' => 1,
            'multi_select' => 0,
        ]);
        DB::table('survey')->insert([
            'name' => 'Tỷ lệ thu nhập hàng tháng có thể dành để đầu tư của Quý khách',
            'name_en' => 'Tỷ lệ thu nhập hàng tháng có thể dành để đầu tư của Quý khách',
            'order_number' => 1,
            'multi_select' => 0,
        ]);
        DB::table('survey')->insert([
            'name' => 'Mức lợi nhuận bình quân hàng năm Quý khách mong muốn từ danh mục đầu tư của mình',
            'name_en' => 'Mức lợi nhuận bình quân hàng năm Quý khách mong muốn từ danh mục đầu tư của mình',
            'order_number' => 1,
            'multi_select' => 0,
        ]);
        DB::table('survey')->insert([
            'name' => 'Trong trường hợp danh mục đầu tư giảm 10%, Quý khách sẽ làm gì:',
            'name_en' => 'Trong trường hợp danh mục đầu tư giảm 10%, Quý khách sẽ làm gì:',
            'order_number' => 1,
            'multi_select' => 0,
        ]);
        DB::table('survey')->insert([
            'name' => 'Tổng tài sản ròng của Quý khách là',
            'name_en' => 'Tổng tài sản ròng của Quý khách là',
            'order_number' => 1,
            'multi_select' => 0,
        ]);
    }
}
