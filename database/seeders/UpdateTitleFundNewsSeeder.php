<?php

namespace Database\Seeders;

use App\Models\FundNews;
use Illuminate\Database\Seeder;

class UpdateTitleFundNewsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // mbvf
        $data = FundNews::where('type', 1)->where('fund_id', 2)->where('title_vi','like', '%Báo cáo NAV ngày%')->orderBy('public_date', 'desc')->get();
        if ($data) {
            foreach ($data as $row) {
                if (strpos($row->title_vi, 'BÁO CÁO NAV NGÀY')) {
                    $row->title_vi = str_replace('BÁO CÁO NAV NGÀY', 'Báo cáo NAV ngày', $row->title_vi);
                }
                $value = FundNews::findorfail($row->id);
                $value->update([
                    'title_vi' => str_replace('Báo cáo NAV', 'Báo cáo NAV phiên GD', $row->title_vi),
                    'title_en' => 'MBVF - NAV Report for Trading date ' . substr($row->title_vi, strpos($row->title_vi, 'ngày') + 6, 10),
                ]);
                $value->save();
            }
        }

        // mbbond
        $data = FundNews::where('type', 1)->where('fund_id', 3)->where('title_vi','like', '%Báo cáo NAV ngày%')->orderBy('public_date', 'desc')->get();
        if ($data) {
            foreach ($data as $row) {
                if (strpos($row->title_vi, 'BÁO CÁO NAV NGÀY')) {
                    $row->title_vi = str_replace('BÁO CÁO NAV NGÀY', 'Báo cáo NAV ngày', $row->title_vi);
                }
                $value = FundNews::findorfail($row->id);
                $value->update([
                    'title_vi' => str_replace('Báo cáo NAV', 'Báo cáo NAV phiên GD', $row->title_vi),
                    'title_en' => 'MBBOND - NAV Report for Trading date ' . substr($row->title_vi, strpos($row->title_vi, 'ngày') + 6, 10),
                ]);
                $value->save();
            }
        }
    }
}
