<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker;
use Illuminate\Support\Facades\DB;
use App\Models\Fund;
class jambfSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        DB::table('fund_news')->insert([
            [
                'title_vi' => 'Năm 2018, Quỹ đầu tư JAMBFđạt lợi nhuận 258 tỷ đồng',
                'content_vi' => '<p>(ĐTCK) Quỹ đầu tư Japan Asia MB Capital (JAMBF) đã tổ chức thành công đại hội quỹ ngày 1/4/2019 tại trụ sở của Công ty cổ phần Quản lý quỹ đầu tư MB (MB Capital). Đây là quỹ thành viên được ra đời từ năm 2010, dưới sự hợp tác chặt chẽ giữa Japan Asia Group và MB Group.</p>',
                'title_en' => 'Năm 2018, Quỹ đầu tư JAMBFđạt lợi nhuận 258 tỷ đồng',
                'content_en' => '<p>(ĐTCK) Quỹ đầu tư Japan Asia MB Capital (JAMBF) đã tổ chức thành công đại hội quỹ ngày 1/4/2019 tại trụ sở của Công ty cổ phần Quản lý quỹ đầu tư MB (MB Capital). Đây là quỹ thành viên được ra đời từ năm 2010, dưới sự hợp tác chặt chẽ giữa Japan Asia Group và MB Group.</p>',
                'type' => 5,
                'public_date' => '2021-10-12',
                'fund_id' => Fund::pluck('id')->random()
            ],

            [
                'title_vi' => 'Quỹ JAMBF tăng trưởng 109% trong năm 2018',
                'content_vi' => '<p>(ĐTCK) Trong khi chỉ số chứng khoán Việt Nam giảm 9,3% năm 2018 thì Quỹ đầu tư JAMBF của MB Capital - quỹ đầu tư dài hạn vào các cổ phiếu niêm yết và chuẩn bị niêm yết lại ghi nhận mức tăng trưởng cao nhất trong lịch sử Quỹ với mức tăng 109%. Ông Ngô Long Giang, Giám đốc điều hành Quỹ JAMBF chia sẻ với Báo Đầu tư Chứng khoán góc nhìn về thị trường chứng khoán 2019 và câu chuyện đầu tư tại MB Capital.</p>',
                'title_en' => 'Quỹ JAMBF tăng trưởng 109% trong năm 2018',
                'content_en' => '<p>(ĐTCK) Trong khi chỉ số chứng khoán Việt Nam giảm 9,3% năm 2018 thì Quỹ đầu tư JAMBF của MB Capital - quỹ đầu tư dài hạn vào các cổ phiếu niêm yết và chuẩn bị niêm yết lại ghi nhận mức tăng trưởng cao nhất trong lịch sử Quỹ với mức tăng 109%. Ông Ngô Long Giang, Giám đốc điều hành Quỹ JAMBF chia sẻ với Báo Đầu tư Chứng khoán góc nhìn về thị trường chứng khoán 2019 và câu chuyện đầu tư tại MB Capital.</p>',
                'type' => 5,
                'public_date' => '2021-10-12',
                'fund_id' => Fund::pluck('id')->random()
            ],

            [
                'title_vi' => 'Qũy đầu tư JAMBF - Tăng trưởng ấn tượng đạt lợi nhuận 258 tỷ năm 2018',
                'content_vi' => '<p>(ĐTCK) Trong khi chỉ số chứng khoán Việt Nam giảm 9,3% năm 2018 thì Quỹ đầu tư JAMBF của MB Capital - quỹ đầu tư dài hạn vào các cổ phiếu niêm yết và chuẩn bị niêm yết lại ghi nhận mức tăng trưởng cao nhất trong lịch sử Quỹ với mức tăng 109%. Ông Ngô Long Giang, Giám đốc điều hành Quỹ JAMBF chia sẻ với Báo Đầu tư Chứng khoán góc nhìn về thị trường chứng khoán 2019 và câu chuyện đầu tư tại MB Capital.</p>',
                'title_en' => 'Qũy đầu tư JAMBF - Tăng trưởng ấn tượng đạt lợi nhuận 258 tỷ năm 2018',
                'content_en' => '<p>(ĐTCK) Trong khi chỉ số chứng khoán Việt Nam giảm 9,3% năm 2018 thì Quỹ đầu tư JAMBF của MB Capital - quỹ đầu tư dài hạn vào các cổ phiếu niêm yết và chuẩn bị niêm yết lại ghi nhận mức tăng trưởng cao nhất trong lịch sử Quỹ với mức tăng 109%. Ông Ngô Long Giang, Giám đốc điều hành Quỹ JAMBF chia sẻ với Báo Đầu tư Chứng khoán góc nhìn về thị trường chứng khoán 2019 và câu chuyện đầu tư tại MB Capital.</p>',
                'type' => 5,
                'public_date' => '2021-10-12',
                'fund_id' => Fund::pluck('id')->random()
            ]
        ]);
    }
}
