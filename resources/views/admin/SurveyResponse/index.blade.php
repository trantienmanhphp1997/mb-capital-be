@extends('layouts.master')
@section('title')
    <title>SurveyResponse</title>
@endsection
@section('content')
    @livewire('admin.survey-response.index')
@endsection