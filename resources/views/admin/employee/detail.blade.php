@extends('layouts.master')
@section('title')
    <title>{{\Request::is('employee/create')?"Thêm nhân sự": 'Chỉnh sửa thông tin nhân sự'}}</title>
@endsection
@section('content')
    <div class="container-fluild">
        <div class="card">
            <div class="card-default">
                <div class="card-header">
                    <h6>{{\Request::is('employee/create')?"Thêm nhân sự": 'Chỉnh sửa thông tin nhân sự'}}</h6>
                </div>
                <div class="card-body">
                    <div class="form">
                        <div class="lang-option">
                            <form action="{{ $routeAction }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @if ($employee)
                                    @method('PUT')
                                @endif

                              <div class="div container mt-2">
                                  <div class="form-group">
                                      <label for="exampleInputShortname">Họ (<span style="color:red">*</span>)</label>
                                      <input type="text" class="form-control" id="exampleInputShortname" name="lastname"
                                             placeholder="Nhập họ" value="{{$employee ?  $employee->lastname : null}}">
                                      @error('firstname')
                                      @include('layouts.partials.text._error')
                                      @enderror
                                  </div>
                                  <div class="form-group">
                                      <label for="exampleInputFullname">Tên (<span style="color:red">*</span>)</label>
                                      <input type="text" class="form-control" id="exampleInputFullname" placeholder="Nhập tên" name="firstname" value="{{$employee ? $employee->firstname : null}}">
                                      @error('lastname')
                                      @include('layouts.partials.text._error')
                                      @enderror
                                  </div>
                                  <div class="form-group">
                                      <label for="exampleInputDescription">Giới tính</label>
                                      <select name="sex" class="form-control" >
                                          <option {{$employee ?  ($employee->sex == 1 ? 'selected' : '') : ''}} value="1">Nam</option>
                                          <option {{$employee ?  ($employee->sex == 2 ? 'selected' : '') : ''}} value="2">Nữ</option>
                                      </select>
                                  </div>

                                  <div class="form_title">
                                      <label for="exampleInputContent">Chức danh 1(EN)</label>
                                      <div class="row">
                                          <div class="col">
                                              <div class="input-group form-group">
                                                  <div class="input-group-prepend">
                                                    <span class="input-group-text text-xs" id="metaDesViPrepend">
                                                        vi
                                                    </span>
                                                  </div>
                                                  <input type="text" class="form-control" placeholder="Nhập chức danh 1(VI)" name="title" value="{{$employee ? $employee->title : null}}">
                                              </div>
                                          </div>
                                          <div class="col">
                                              <div class="input-group form-group">
                                                  <div class="input-group-prepend">
                                                    <span class="input-group-text text-xs" id="metaDesEnPrepend">
                                                        en
                                                    </span>
                                                  </div>
                                                  <input type="text" class="form-control" placeholder="Nhập chức danh 1(EN)" name="title_en" value="{{$employee ? $employee->title_en : null}}">
                                              </div>
                                          </div>
                                      </div>
                                  </div>

                                  <div class="form_title">
                                    <label for="exampleInputContent">Chức danh 2(EN)</label>
                                    <div class="row">
                                        <div class="col">
                                            <div class="input-group form-group">
                                                <div class="input-group-prepend">
                                                  <span class="input-group-text text-xs" id="metaDesViPrepend">
                                                      vi
                                                  </span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Nhập chức danh 2(VI)" name="title_2" value="{{$employee ? $employee->title_2 : null}}">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="input-group form-group">
                                                <div class="input-group-prepend">
                                                  <span class="input-group-text text-xs" id="metaDesEnPrepend">
                                                      en
                                                  </span>
                                                </div>
                                                <input type="text" class="form-control" placeholder="Nhập chức danh 2(EN)" name="title_2_en" value="{{$employee ? $employee->title_2_en : null}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                  <div class="form-group">
                                      <label for="exampleInputContent">Vai trò</label>
                                      <select name="type" class="form-control" >
                                          @foreach ( $roles as $key => $item)
                                              <option {{$employee ?  ($employee->type == $key ? 'selected' : '') : ''}} value="{{$key}}">{{$item}}</option>
                                          @endforeach
                                      </select>
                                  </div>

                                  <div class="form_title">
                                      <label for="exampleInputContent">Tiểu sử</label>
                                      <div class="row">
                                          <div class="col">
                                              <div class="input-group form-group">
                                                  <div class="input-group-prepend">
                                                    <span class="input-group-text text-xs" id="metaDesViPrepend">
                                                        vi
                                                    </span>
                                                  </div>
                                                  <textarea aria-describedby="metaDesViPrepend" class="border form-control" name="content_vi" rows="5">{{$employee ?  $employee->content : null}}</textarea>
                                              </div>
                                          </div>
                                          <div class="col">
                                              <div class="input-group form-group">
                                                  <div class="input-group-prepend">
                                                    <span class="input-group-text text-xs" id="metaDesEnPrepend">
                                                        en
                                                    </span>
                                                  </div>
                                                  <textarea aria-describedby="metaDesEnPrepend" class="border form-control" name="content_en" rows="5">{{$employee ?  $employee->content_en : null}}</textarea>
                                              </div>
                                          </div>
                                      </div>
                                  </div>

                                  <div class="form_title">
                                      <label for="exampleInputContent">Quỹ</label>
                                      <select name="fund_id" class="form-control" >
                                          <option value="">--Chọn--</option>
                                          @foreach ( $funds as $key => $item)
                                              <option {{$employee ?  ($employee->fundid == $item->id ? 'selected' : '') : ''}} value="{{$item->id}}">{{$item->shortname}}</option>
                                          @endforeach
                                      </select>
                                  </div>
                                  <div class="form-group">
                                      <label for="exampleInputFullname">Độ ưu tiên</label>
                                      <input type="number" min="1" class="form-control" name="priority" value="{{$employee ? $employee->priority : null}}">
                                      @error('priority')
                                      @include('layouts.partials.text._error')
                                      @enderror
                                  </div>
                                  <div class="form-group row">
                                    <div class="col-md-2">
                                      <label for="active">Ẩn nhân sự </label>
                                    </div>
                                    <div>
                                      <input type="checkbox" {{isset($employee)&&$employee->active==-1?'checked':''}} name="active" >
                                    </div>
                                  </div>

                                  <div class="form_title">
                                      <div class="border mt-4 px-3 rounded py-2 bg-light position-absolute">
                                          <i class="fa fa-image mr-2"></i>
                                          Chọn ảnh đại diện
                                      </div>
                                      <input accept="image/*" class="border mt-4 px-3 rounded py-2 bg-light position-absolute" type="file" style="opacity: 0" name="img" id="choseFile"/>

                                      @if(isset($employee))
                                          <div style="margin-left: 200px; margin-top: 40px;">
                                              <img height="109rem" src="{{asset($employee->img)}}">
                                          </div>
                                      @endif

                                  </div>
                              </div>
                              <div class="container text-end" style="margin-top: 100px">
                                  <button type="submit" class="btn btn-primary text-end mr-2">Lưu</button>
                                  <a href="{{ route('admin.employee.index') }}" class="btn btn-default">Quay lại</a>
                              </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
