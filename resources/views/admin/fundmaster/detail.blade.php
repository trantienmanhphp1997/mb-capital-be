@extends('layouts.master')
@section('title')
    <title>Cấu hình quỹ đầu tư</title>
@endsection
@section('content')
    <div class="container-fluild">
        <div class="card">
            <div class="card-default">
                <div class="card-header">
                    <h6>{{ \Request::is('fundmaster/create') ? 'Thêm cấu hình quỹ đầu tư' : 'Chỉnh sửa thông tin cấu hình quỹ đầu tư' }}</h6>
                </div>
                <div class="card-body">
                    <div class="form">
                        <form action="{{ $routeAction }}" method="POST" class="container mt-2" enctype="multipart/form-data">
                            @csrf
                            @if ($fundms)
                                @method('PUT')
                            @endif

                            <div class="form-group">
                                <label for="fullname">Tên quỹ đầu tư (<span style='color:red;'>*</span>)</label>
                                <select name="fund_id" class="form-control select_box col-md-12" onchange="showFundMasterTypes(this.options[this.selectedIndex].getAttribute('data-type'))">
                                    <option value="" data-type="">Chọn</option>
                                    @foreach ($fundList as $fund)
                                        <option value="{{ $fund->id }}" data-type="{{ $fund->type }}" {{ $fund->id == ( old('fund_id', null) ?? $fundms->fund_id ?? '') ? 'selected' : '' }}>{{ $fund->shortname }}</option>
                                    @endforeach
                                </select>
                                @error('fundid')
                                    @include('layouts.partials.text._error')
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="fullname">Phân loại</label>
                                {!! Form::select('type', $fundMasterTypes[$fundms->fund->type ?? null] ?? [], $fundms->type ?? null, ['class' => 'form-control select-box col-md-12', 'id' => 'fund-master-type']) !!}
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <h4>Tiếng Việt</h4>
                                    <x-form-info-fundmaster :lang="'vi'" :fundms="$fundms" :fundList="$fundList" :modelName="$model_name" :modelId="$model_id" :fundMasterTypes="$fundMasterTypes" />
                                </div>
                                <div class="col-md-6">
                                    <h4>Tiếng Anh</h4>
                                    <x-form-info-fundmaster :lang="'en'" :fundms="$fundms" :fundList="$fundList" :modelName="$model_name" :modelId="$model_id" :fundMasterTypes="$fundMasterTypes" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputContent">File/Hình ảnh</label>
                                <input type="file" name="img" class="form-control">
                                @error('img')
                                    <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="URL">URL</label>
                                {{ Form::text('url', isset($fundms->url) ? $fundms->url : null, ['class' => 'form-control', 'placeholder' => 'Nhập url']) }}
                                {{-- <input type="text" name="url" id="" value="{{ isset($fundms->url) ? $fundms->url : null }}" class="form-control" placeholder="Nhập url..."> --}}
                            </div>

                            <div class="form-group">
                                <label for="URL">Giá trị số</label>
                                {{ Form::text('number_value', isset($fundms->number_value) ? $fundms->number_value : null, ['class' => 'form-control', 'placeholder' => 'Nhập giá trị số']) }}
                                @error('number_value')
                                    @include('layouts.partials.text._error')
                                @enderror
                            </div>

                            <div class="form-group">
                                <!-- <label>{{ __('common.file') }}</label> -->
                                <label>File đính kèm</label>
                                @livewire('component.files', [
                                    'model_name' => $model_name,
                                    'model_id' => $model_id ?? null,
                                    'type' => config('common.type_upload.FundMaster'),
                                    'folder' => 'fundMaster',
                                    'acceptMimeTypes' => config('common.mime_type.general')
                                ])
                            </div>

                            <input type="hidden" id="fund-master-value" value="{{ json_encode($fundMasterTypes) }}">

                            <button type="submit" class="btn btn-primary text-end mr-2">Lưu</button>
                            <a href="{{ url()->previous() }}" class="btn btn-default">Quay lại</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if ($fundms)
        <script>
            $("document").ready(() => {
                $('#summernote_vi').val('')
                $('#summernote_en').val('')
                $('#summernote_vi').summernote('code', {!! json_encode(chuanHoa($fundms->note)) !!} );
                $('#summernote_en').summernote('code', {!! json_encode(chuanHoa($fundms->note_en)) !!});
            });
        </script>
    @else
        <script>
            $("document").ready(() => {
                $('#summernote_vi').val('')
                $('#summernote_en').val('')
                $('#summernote_vi').summernote('code', null);
                $('#summernote_en').summernote('code', null);
            });
        </script>
    @endif

    <script>
        var fundMasterTypes = JSON.parse(document.getElementById('fund-master-value').value);

        function showFundMasterTypes(fundType) {
            let $select = document.getElementById('fund-master-type');
            if (!fundType) return;
            $select.innerHTML = '';

            let types = fundMasterTypes[fundType];
            for (let key in types) {
                $select.innerHTML += `
                            <option value="${key}">${types[key]}</option>
                        `;
            }
        }
    </script>
@endsection
