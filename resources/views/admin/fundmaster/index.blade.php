@extends('layouts.master')
@section('title')
<title>Cấu hình quỹ đầu tư</title>
@endsection
@section('css')
<style type="text/css">
    .read-more-show{
      cursor:pointer;
      color: #5213a5;
    }
    .read-more-hide{
      cursor:pointer;
      color: #ed8323;
    }

    .hide_content{
      display: none;
    }
</style>
@endsection
@section('content')
    
    @livewire('fund-master.fund-master-list');

@endsection