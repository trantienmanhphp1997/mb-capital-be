<div class="card py-2 px-3">
    <form>
        <div class="form_title">
            <label>Tên quỹ đầu tư <span class="text-danger">(*)</span> </label>
            <div class="row">
                <div class="col">
                    <div class="input-group form-group">
                        {{-- {!! Form::select('fundid',$fundid,isset($fundnav)?$fundnav->fund_id: null, array('class' => 'form_control select_box col-md-2')) !!} --}}
                        <select name="fundid" class="form_control select_box col-md-2">
                            <option value=''>VNINDEX</option>
                            @foreach ($fundid as $fund)
                                <option value="{{ $fund->id }}" @if(isset($fundnav->fund_id) && ($fundnav->fund_id == $fund->id)) selected @endif>{{ $fund->shortname }}</option>
                            @endforeach
                        </select>
                        @error('fundid')
                            @include('layouts.partials.text._error')
                        @enderror
                    </div>
                </div>
            </div>
        </div>

        <div class="form_title">
            <label>Số tiền <span class="text-danger">(*)</span> </label>
            <div class="row">
                <div class="col">
                    <div class="input-group form-group">
                        {!! Form::text('amount',null, array('maxlength' => '20', 'placeholder' => 'Số tiền', 'class' => 'form-control col-md-2', 'id' => 'money')) !!}
                        @error('amount')
                            @include('layouts.partials.text._error')
                        @enderror
                    </div>
                </div>
            </div>
        </div> 

        <div class="form_title">
            <label>Ngày công bố </label>
            <div class="row">
                <div class="col">
                    <div class="input-group form-group">
                        {!! Form::date('trading_session_time', null, array('class' => 'form-control col-md-2')) !!}
                    </div>
                </div>
            </div>
        </div>
        
        <div class="w-100 clearfix my-2">
            <button name="submit" type="submit" id="save" class="float-right btn ml-1 btn-primary">Lưu lại</button>
            <a href="{{ route('admin.fundnav') }}" class="btn btn-secondary float-right mr-1">Hủy</a>          
        </div>
    </form>
</div>

<script>
document.addEventListener('DOMContentLoaded', function(){   
    function formatNumber(n) {
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
    function formatNumber2(n) {
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, "")
    }

    function formatCurrency(input, blur) {
        // appends $ to value, validates decimal side
        // and puts cursor back in right position.

        // get input value
        let input_val = $(input).val();
        // don't validate empty input
        if (input_val === "") { return; }
        // original length
        let original_len = input_val.length;
        // initial caret position
        let caret_pos = input.prop("selectionStart");
        if(input_val.length>1&&input_val[0]=='0'&&input_val[1]!='.'){
            input_val = input_val.substring(1)
        }
        // check for decimal
        if (input_val.indexOf(".") >= 0) {
            // get position of first decimal
            // this prevents multiple decimals from
            // being entered
            let decimal_pos = input_val.indexOf(".");

            // split number by decimal point
            let left_side = input_val.substring(0, decimal_pos);
            let right_side = input_val.substring(decimal_pos);

            // add commas to left side of number
            left_side = formatNumber(left_side);
            // add commas to left side of number
            right_side = formatNumber2(right_side);
            // On blur make sure 3 numbers after decimal
            if (blur === "blur") {
                right_side += "000";
            }
            // alert(right_side)
            // Limit decimal to only 3 digits
            right_side = right_side.substring(0, 2);
            // join number by .
            input_val = left_side + "." + right_side;

        } else {
            // no decimal entered
            // add commas to number
            // remove all non-digits
            input_val = formatNumber(input_val);
            input_val = input_val;

            // final formatting
            if (blur === "blur") {
                input_val += ".000";
            }
        }
        // send updated string to input
        input.val(input_val);
        // put caret back in the right position
        let updated_len = input_val.length;
        caret_pos = updated_len - original_len + caret_pos;
        input[0].setSelectionRange(caret_pos, caret_pos);
    }
    $('#money').keyup(function(){
        formatCurrency($(this));
    })
    formatCurrency($('#money'))
})
</script>