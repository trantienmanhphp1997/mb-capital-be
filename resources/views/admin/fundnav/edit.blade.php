@extends('layouts.master')

@section('title')
    <title>Cập nhật NAV quỹ</title>
{{-- @section('css')
    <link href="{{ asset('assets/css/datatables.min.css') }}" rel="stylesheet" />
    <style>
        .custom-select {
            width: 100%;
        }

    </style>
@endsection --}}
@section('content')
<div class="body-content p-2">
    <div class="p-2 pb-3 d-flex align-items-center justify-content-between">
        <div class="">
            <h4 class="m-0">
                Cập nhật NAV quỹ
            </h4>
        </div>
        <div class="paginate">
            <div class="d-flex">
                <div class="">
                    <a href="{{ route('home') }}"><i class="fa fa-home"></i> Trang chủ</a>
                </div>
                <span class="px-2">/</span>
                <div class="">
                    <div class="disable">Quản lý NAV quỹ</div>
                </div>
            </div>
        </div>
    </div>
</div>
    {!! Form::model($fundnav, ['method' => 'POST', 'autocomplete' => "off",'route' => ['admin.fundnav.update', $fundnav->id]]) !!}
        @include('admin.fundnav._formFundNAV')
    {!! Form::close() !!}
@endsection