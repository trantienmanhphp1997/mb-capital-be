<div class="card py-2 px-3">
    <form>
        <div class="form_content">
            <label>Tải tệp lên </label>
            <div class="row">
                <div class="col-12">
                    <div class="input-group form-group">
                        <input type="file"  id="file-upload" name="file-upload">
                        @error('file-upload')
                            @include('layouts.partials.text._error')
                        @enderror
                    </div>
                </div>
                <div class="preview-file">
                    @if($data['file_path'])
                        <div class="form_content ml-2 form-group preview-data" data="{{ '/storage/public/'. $data['file_path'] }}">
                            <div class="py-2 px-3 bg-light d-flex align-items-center" id="preview-item">
                                <a href="{{ '/storage/'. $data['file_path'] }}" target="_blank">
                                    <i class="fas fa-file mr-2"></i> {{$data['file_name'] ? $data['file_name']: $data['file_path'] }}
                                </a>
                                <div class="border-left ml-2 px-2 btn btn-md" id="removeFile">
                                    <i class="fa fa-times mr-1"></i> Xóa
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <input class="form-control" hidden name="remove_path" id="remove_path"/>
            </div>
        </div>

        <div class="form_title">
            <div class="row">
                <div class="col">
                    <div class="form_content">
                        <label>Quỹ đầu tư </label>
                        <div class="input-group form-group">
                            {{-- {!! Form::select('fund_id', $fundid  , null, ['class' => 'form-control', 'class' => 'form-control-sm form-control custom-input-control']) !!} --}}
                            <select name="fund_id" class="form-control-sm form-control custom-input-control">
                                <option value=""></option>
                                @foreach ($funds as $fund)
                                <option value="{{ $fund->id }}" @if(isset($data->fund_id) && ($data->fund_id == $fund->id)) selected @endif>{{ $fund->shortname }}</option>
                                    {{-- <option value="{{ $fund->id }}">{{ $fund->fullname }}</option> --}}
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form_title">
            <div class="row">
                <div class="col">
                    <label>Loại <span class="text-danger">(*)</span> </label>
                    <div class="row">
                        <div class="col">
                            <div class="input-group form-group">
                                {!! Form::select('type', \App\Enums\EFundNews::getListData(),
                                                null, ['class' => 'form-control', 'class' => 'form-control-sm form-control custom-input-control article-type']) !!}
                                @error('type')
                                    @include('layouts.partials.text._error')
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form_content">
                        <label>Ngày công bố </label>
                        <div class="form-group">
                            {!! Form::date('public_date', null, array('class' => 'form-control-sm form-control custom-input-control')) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Title -->
        <div class="form_title">
            <label>Tiêu đề <span class="text-danger">(*)</span> </label>
            <div class="row">
                <div class="col">
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text text-xs" id="nameViPrepend">
                                vi
                            </span>
                        </div>
                        {!! Form::text('title_vi', null, array('class' => 'form-control-sm form-control custom-input-control')) !!}
                    </div>
                    @error('title_vi')
                        @include('layouts.partials.text._error')
                    @enderror
                </div>
                <div class="col">
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text text-xs" id="nameEnPrepend">
                                en
                            </span>
                        </div>
                        {!! Form::text('title_en', null, array('class' => 'form-control-sm form-control custom-input-control')) !!}
                    </div>
                    @error('title_en')
                        @include('layouts.partials.text._error')
                    @enderror
                </div>
            </div>
        </div>

        <!-- Content -->
        <div class="form_content">
            <label>Nội dung (vi) </label>
            <div class="form-group">
                {!! Form::textarea('content_vi', null, array('class' => 'textarea', 'id' => 'content_vi', 'name' => 'content_vi')) !!}
            </div>
        </div>

        <div class="form_content">
            <label>Nội dung (en) </label>
            <div class="form-group">
                {!! Form::textarea('content_en', null, array('class' => 'textarea', 'id' => 'content_en', 'name' => 'content_en')) !!}
            </div>
        </div>
        <div class="has-url-form form-group">
            <div class="form_content">
                <label>Link bài viết </label>
                <div class="form-group">
                    {!! Form::text('url', null, array('placeholder' => 'Nhập link bài viết', 'class' => 'form-control', 'id' => 'url', 'name' => 'url')) !!}
                </div>
            </div>
            @error('url')
                @include('layouts.partials.text._error')
            @enderror
        </div>
        <div class="w-100 clearfix my-2">
            <button name="submit" type="submit" class="float-right btn ml-1 btn-primary">Lưu lại</button>
            <a href="{{ route('admin.fundnews') }}" type="submit" class="btn btn-secondary float-right mr-1">Hủy</a>
        </div>

    </form>
</div>
@section('js')
    <script>
        $(function(){
            $('#content_vi').val('')
            $('#content_en').val('')
            $('#content_vi').summernote('code', {!! json_encode(chuanHoa($data->content_vi ))!!} );
            $('#content_en').summernote('code', {!! json_encode(chuanHoa($data->content_en ))!!} );
            $(document).on('click', '#removeFile', function(){
                var x = $(this);
                    parent = x.parents(".preview-data");
                    path = parent.attr('data');
                    i = parent.parent().next();
                if(path&&!i.val()){
                    i.val(path);
                }
                parent.parent().empty();
                $("input[name='file-upload']").val('');
            });

            $("input[name='file-upload']").change(function(e){
                var f = this.files;
                if(f.length){
                    f = f[0];
                    var x = $('#removeFile');
                    parent = x.parents(".preview-data");
                    path = parent.attr('data');
                    i = parent.parent().next();
                    if(path&&!i.val()){
                        i.val(path);
                    }
                    // $("input[name='file-upload']").val('');
                    $(".preview-file").empty().html(
                        '<div class="form_content ml-2 form-group preview-data">'
                            +'<div class="py-2 px-3 bg-light d-flex align-items-center" id="preview-item">'
                                +'<div>'
                                    +'<i class="fas fa-file mr-2"></i>' + f.name
                                +'</div>'
                                +'<div class="border-left ml-2 px-2 btn btn-md" id="removeFile">'
                                    +'<i class="fa fa-times mr-1"></i> Xóa'
                                +'</div>'
                            +'</div>'
                        +'</div>'
                    );
                }
            });

        });
    </script>
@endsection
