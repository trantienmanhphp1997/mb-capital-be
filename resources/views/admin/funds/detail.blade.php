@extends('layouts.master')
@section('title')
    <title>Chi tiết quỹ đầu tư</title>
@endsection
@section('content')
    <div class="container-fluild">
        <div class="card">
            <div class="card-default">
                <div class="card-header">
                    <h6>{{\Request::is('funds/create')?"Thêm quỹ đầu tư": 'Chỉnh sửa thông tin quỹ đầu tư'}}</h6>
                </div>
                <div class="card-body">
                    <div class="form">
                        <div class="lang-option">
                            <form action="{{ $routeAction }}" method="POST">
                                @csrf
                                @if ($fund)
                                    @method('PUT')
                                @endif
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                  <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Tiếng việt</a>
                                  <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Tiếng Anh</a>
                                </div>
                              </nav>
                              <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <x-form-info-fund :lang="'vi'" :fund="$fund"/>
                                    <div class="div container mt-2 form-group">
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label for="exampleInputDescription">Nav Quỹ </label>
                                                {!! Form::text('current_nav',$fund->current_nav??null, array('maxlength' => '20', 'placeholder' => 'Nav quỹ', 'class' => 'form-control number_input', 'id' => 'money')) !!}
                                            </div>
                                            <div class="col-md-6">
                                                <label for="exampleInputDescription">Tăng trưởng (<span style="color:red">*</span>)</label>
                                                {!! Form::text('growth',$fund->growth??null, array('maxlength' => '20', 'placeholder' => 'Tăng trưởng', 'class' => 'form-control', 'id' => 'growth')) !!}
                                                @error('growth')
                                                    @include('layouts.partials.text._error')
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label>Loại quỹ (<span style="color:red">*</span>)</label>
                                                {!! Form::select("type", \App\Enums\EFundList::getListData(), $fund ? ($fund->type) : null, ['class'=>'form-control','placeholder'=>'--Chọn--']) !!}
                                                @error('type')
                                                @include('layouts.partials.text._error')
                                                @enderror
                                            </div>
                                            <div class="col-md-6">
                                                <label>Trực thuộc quỹ đầu tư</label>
                                                {!! Form::select('parent_id', $allfunds, $fund ? ($fund->parent_id) : null, ['class'=>'form-control','placeholder'=>'--Chọn--']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-4">
                                                <label for="priority">Thứ tự hiển thị</label>
                                                {!! Form::text('priority',$fund->priority??null, array('maxlength' => '20', 'placeholder' => 'Thứ tự hiển thị', 'class' => 'form-control')) !!}
                                            </div>
                                            <div class="col-md-4">
                                                <label>Hiển thị tab Kết quả hoạt động</label>
                                                {!! Form::select('enable_performance', [1 => 'Hiển thị', 0 => 'Không hiển thị'], $fund ? ($fund->enable_performance) : 1, ['class'=>'form-control']) !!}
                                            </div>
                                            <div class="col-md-4">
                                                <label>Hiển thị tab Công bố thông tin</label>
                                                {!! Form::select('enable_info_disclosure', [1 => 'Hiển thị', 0 => 'Không hiển thị'], $fund ? ($fund->enable_info_disclosure) : 1, ['class'=>'form-control']) !!}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-2">
                                                <label for="active">Ẩn Quỹ</label>
                                            </div>
                                            <div>
                                                <input type="checkbox" {{isset($fund)?($fund->active==-1?'checked':''):''}} name="active" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                    <x-form-info-fund :lang="'en'" :fund="$fund"/>
                                </div>
                                <div class="container text-end">
                                    <button type="submit" class="btn btn-primary text-end mr-2">Lưu</button>
                                    <a href="{{ route('admin.funds') }}" class="btn btn-default">Quay lại</a>
                                </div>
                              </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script>
document.addEventListener('DOMContentLoaded', function(){
    @if(isset($fund))   
        $('#content_vi').val('')
        $('#content_mobile_vi').val('')
        $('#content_en').val('')
        $('#content_mobile_en').val('')

        $('#content_vi').summernote('code', {!!  json_encode(chuanHoa($fund->content)) !!});
        $('#content_mobile_vi').summernote('code', {!! json_encode (chuanHoa($fund->content_mobile ))!!});
        $('#content_en').summernote('code', {!!  json_encode(chuanHoa($fund->content_en)) !!});
        $('#content_mobile_en').summernote('code', {!! json_encode (chuanHoa($fund->content_mobile_en ))!!});
    @else
        $('#content_vi').val('')
        $('#content_mobile_vi').val('')
        $('#content_en').val('')
        $('#content_mobile_en').val('')
        $('#content_vi').summernote('code', '');
        $('#content_mobile_vi').summernote('code', '');
        $('#content_en').summernote('code', '');
        $('#content_mobile_en').summernote('code', '');  
    @endif
    function formatNumber(n) {
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
    function formatNumber2(n) {
        return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, "")
    }

    function formatCurrency(input, blur) {
        // appends $ to value, validates decimal side
        // and puts cursor back in right position.

        // get input value
        let input_val = $(input).val();
        // don't validate empty input
        if (input_val === "") { return; }
        // original length
        let original_len = input_val.length;
        // initial caret position
        let caret_pos = input.prop("selectionStart");
        if(input_val.length>1&&input_val[0]=='0'&&input_val[1]!='.'){
            input_val = input_val.substring(1)
        }
        // check for decimal
        if (input_val.indexOf(".") >= 0) {
            // get position of first decimal
            // this prevents multiple decimals from
            // being entered
            let decimal_pos = input_val.indexOf(".");

            // split number by decimal point
            let left_side = input_val.substring(0, decimal_pos);
            let right_side = input_val.substring(decimal_pos);

            // add commas to left side of number
            left_side = formatNumber(left_side);
            // add commas to left side of number
            right_side = formatNumber2(right_side);
            // On blur make sure 3 numbers after decimal
            if (blur === "blur") {
                right_side += "000";
            }
            // alert(right_side)
            // Limit decimal to only 3 digits
            right_side = right_side.substring(0, 2);
            // join number by .
            input_val = left_side + "." + right_side;

        } else {
            // no decimal entered
            // add commas to number
            // remove all non-digits
            input_val = formatNumber(input_val);
            input_val = input_val;

            // final formatting
            if (blur === "blur") {
                input_val += ".000";
            }
        }
        // send updated string to input
        input.val(input_val);
        // put caret back in the right position
        let updated_len = input_val.length;
        caret_pos = updated_len - original_len + caret_pos;
        input[0].setSelectionRange(caret_pos, caret_pos);
    }
    $('.number_input').keyup(function(){
        formatCurrency($(this));
    })
    formatCurrency($('#money'))
    // formatCurrency($('#growth'))
})
</script>
