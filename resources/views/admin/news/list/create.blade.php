@extends('layouts.master')
@section('title')
    <title>Danh sách tin tức</title>
@endsection
@section('content')
    <div class="body-content p-2">
        <div class="p-2 pb-3 d-flex align-items-center justify-content-between">
            <div class="">
                <h4 class="m-0">
                    Tạo mới bài viết
                </h4>
            </div>
            <div class="paginate">
                <div class="d-flex">
                    <div>
                        <a href="{{ route('home') }}">
                            <i class="fa fa-home"></i>
                        </a>
                    </div>

                    <span class="px-2">/</span>

                    <div class="">
                        <a href="{{ route('admin.new.list.index') }}">Danh sách</a>
                    </div>
                    <span class="px-2">/</span>
                    <div class="">
                        <div class="disable">Tạo mới tin tức</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card py-2 px-3">
            {{ Form::open(['url' => route('admin.new.store'), 'method' => 'PUT', 'enctype' => 'multipart/form-data']) }}
                @include('admin.news.list.field', ['errors' => $errors, 'info' => null])
            {{ Form::close() }}
        </div>
    </div>
@endsection

