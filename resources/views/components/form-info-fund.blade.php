<div class="div container mt-2">
        <div class="form-group">
            <label for="exampleInputShortname">Tên rút gọn (<span style="color:red">*</span>)</label>
            {{-- <input type="text" class="form-control" id="exampleInputShortname" name="shortname_{{$lang}}"
                placeholder="Nhập tên" value="{{$fund ? ($lang == 'en' ? $fund->shortname_en : $fund->shortname) : null}}"> --}}

            {!! Form::text("shortname_$lang", $fund ? ($lang == 'en' ? $fund->shortname_en : $fund->shortname) : null, ['class'=>'form-control','id'=>'exampleInputShortname','placeholder'=>'Nhập tên']) !!}
            {{-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                else.</small> --}}
            @error('shortname_'.$lang)
            @include('layouts.partials.text._error')
            @enderror
        </div>
        <div class="form-group">
            <label for="exampleInputFullname">Tên đầy đủ (<span style="color:red">*</span>)</label>
            {{-- <input type="text" class="form-control" id="exampleInputFullname" placeholder="Nhập tên" name="fullname_{{$lang}}" value="{{$fund ? ($lang == 'en' ? $fund->fullname_en : $fund->fullname) : null}}"> --}}
            {!! Form::text("fullname_$lang", $fund ? ($lang == 'en' ? $fund->fullname_en : $fund->fullname) : null, ['class'=>'form-control','id'=>'exampleInputFullname','placeholder'=>'Nhập tên']) !!}
            @error('fullname_'.$lang)
            @include('layouts.partials.text._error')
            @enderror
        </div>
        <div class="form-group">
            <label for="exampleInputFullname">Slug (<span style="color:red">*</span>)</label>
            {!! Form::text("slug_$lang", $fund ? ($lang == 'en' ? $fund->slug_en : $fund->slug) : null, ['class'=>'form-control','placeholder'=>'Slug']) !!}
            @error('slug_'.$lang)
            @include('layouts.partials.text._error')
            @enderror
        </div>
        <div class="form-group">
            <label for="exampleInputFullname">Tên quỹ (Header) (<span style="color:red">*</span>)</label>
            {!! Form::text("fullname2_$lang", $fund ? ($lang == 'en' ? $fund->fullname2_en : $fund->fullname2) : null, ['class'=>'form-control','id'=>'exampleInputFullname','placeholder'=>'Nhập tên']) !!}
            @error('fullname2_'.$lang)
            @include('layouts.partials.text._error')
            @enderror
        </div>
        <div class="form-group">
            <label for="exampleInputFullname">Tên quỹ (Footer) (<span style="color:red">*</span>)</label>
            {!! Form::text("shortname2_$lang", $fund ? ($lang == 'en' ? $fund->shortname2_en : $fund->shortname2) : null, ['class'=>'form-control','id'=>'exampleInputFullname','placeholder'=>'Nhập tên']) !!}
            @error('shortname2_'.$lang)
            @include('layouts.partials.text._error')
            @enderror
        </div>
        <div class="form-group">
            <label for="exampleInputFullname">Ngày thành lập (<span style="color:red">*</span>)</label>
            {!! Form::text("public_date_$lang", $fund ? ($lang == 'en' ? $fund->public_date_en : $fund->public_date) : null, ['class'=>'form-control','id'=>'exampleInputPublicDate','placeholder'=>'Nhập ngày thành lập']) !!}
            @error('public_date_'.$lang)
            @include('layouts.partials.text._error')
            @enderror
        </div>
        <div class="form-group">
            <label for="exampleInputDescription">Mô tả</label>

            {{-- <textarea class="form-control" rows="5" name="description_{{$lang}}">{{old('description_'.$lang)}}</textarea> --}}
            {{-- <textarea class="form-control" rows="5" name="description_{{$lang}}">{{$fund ? ($lang == 'en' ? $fund->description_en : $fund->description) : null}}</textarea> --}}
            {!! Form::textarea("description_$lang",$fund ? ($lang == 'en' ? $fund->description_en : $fund->description) : null , ['class'=>'form-control']) !!}
        </div>
        <div class="form-group">
            <label for="exampleInputContent">Nội dung</label>
            {{-- <textarea id="summernote" class="textarea" name="content_{{$lang}}">{{$fund ? ($lang == 'en' ? $fund->content_en : $fund->content) : null}}</textarea> --}}
            {!! Form::textarea("content_$lang",$fund ? ($lang == 'en' ? $fund->content_en : $fund->content) : null , ['class'=>'textarea','id' => "content_$lang"]) !!}
        </div>
        <div class="form-group">
            <label for="exampleInputContent">Nội dung (mobile)</label>
            {!! Form::textarea("content_mobile_$lang",$fund ? ($lang == 'en' ? $fund->content_mobile_en : $fund->content_mobile) : null , ['class'=>'textarea','id' => "content_mobile_$lang"]) !!}
        </div>
    </form>
</div>
