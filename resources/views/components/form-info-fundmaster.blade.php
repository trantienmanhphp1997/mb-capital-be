<div class="div container mt-2">

    <div class="form-group">
        <label for="URL">Tên cấu hình</label>
        {{ Form::text("v_key_$lang", $fundms ? ($lang == 'en' ? $fundms->v_key_en : $fundms->v_key) : null, ['class' => 'form-control', 'placeholder' => 'Nhập tên cấu hình...']) }}
        {{-- <input type="text" class="form-control" id="exampleInputShortname" name="v_key_{{ $lang }}" placeholder="Nhập tên cấu hình..." value="{{ $fundms ? ($lang == 'en' ? $fundms->v_key_en : $fundms->v_key) : null }}"> --}}
        @error('v_key_' . $lang)
            @include('layouts.partials.text._error')
        @enderror
    </div>
    <div class="form-group">
        <label for="exampleInputShortname">Tiêu đề (<span style='color:red;'>*</span>)</label>
        {{ Form::text("title_$lang", $fundms ? ($lang == 'en' ? $fundms->title_en : $fundms->title) : null, ['class' => 'form-control', 'placeholder' => 'Nhập tên']) }}
        {{-- <input type="text" class="form-control" id="exampleInputShortname" name="title_{{ $lang }}" placeholder="Nhập tên" value="{{ $fundms ? ($lang == 'en' ? $fundms->title_en : $fundms->title) : null }}"> --}}
        @error('title_' . $lang)
            @include('layouts.partials.text._error')
        @enderror
    </div>
    <div class="form-group">
        <label for="exampleInputContent">Nội dung (<span style='color:red;'>*</span>)</label>
        {{ Form::textarea("content_$lang", $fundms ? ($lang == 'en' ? $fundms->content_en : $fundms->content) : null, ['class' => 'form-control']) }}
        {{-- <textarea id="summernote" class="form-control" name="content_{{ $lang }}">{{ $fundms ? ($lang == 'en' ? $fundms->content_en : $fundms->content) : null }}</textarea> --}}
        @error('content_' . $lang)
            @include('layouts.partials.text._error')
        @enderror
    </div>
    <div class="form-group">
        <label for="exampleInputContent">Nội dung (mobile)</label>
        {{ Form::textarea("content_mobile_$lang", $fundms ? ($lang == 'en' ? $fundms->content_mobile_en : $fundms->content_mobile) : null, ['class' => 'form-control textarea']) }}
        {{-- <textarea id="summernote" class="form-control" name="content_{{ $lang }}">{{ $fundms ? ($lang == 'en' ? $fundms->content_en : $fundms->content) : null }}</textarea> --}}
        @error('content_mobile_' . $lang)
            @include('layouts.partials.text._error')
        @enderror
    </div>

    <div class="form-group">
        <label for="exampleInputContent">Ghi chú</label>
        {{ Form::textarea("note_$lang", $fundms ? ($lang == 'en' ? $fundms->note_en : $fundms->note) : null, ['class' => 'form-control textarea text_summernote', 'id' => "summernote_$lang"]) }}
        {{-- <textarea id="summernote" class="textarea" name="note_{{ $lang }}">{{ $fundms ? ($lang == 'en' ? $fundms->note_en : $fundms->note) : null }}</textarea> --}}
        @error('note_' . $lang)
            @include('layouts.partials.text._error')
        @enderror
    </div>

</div>
