@extends('layouts.master')

@section('content')

@if(isset($data))
    {!! Form::model($data, ['method' => 'PATCH', 'autocomplete' => "off",'route' => ['guideline_detail.update', $data->id]]) !!}
@else
    {!! Form::model(null, ['method' => 'POST', 'autocomplete' => "off",'route' => ['guideline_detail.store', ['guideid' => $guideid]]]) !!}
@endif
<div class="card py-2 px-3">
    <div class="form_title">
        <label>Tên các bước <span class="text-danger">(*)</span> </label>
        <div class="row">
            <div class="col">
                <div class="input-group form-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text text-xs" id="nameViPrepend">
                            vi
                        </span>
                    </div>
                    {!! Form::text('name', null, array('class' => 'form-control-sm form-control custom-input-control')) !!}
                </div>
                @error('name')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
            <div class="col">
                <div class="input-group form-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text text-xs" id="nameEnPrepend">
                            en
                        </span>
                    </div>
                    {!! Form::text('name_en', null, array('class' => 'form-control-sm form-control custom-input-control')) !!}
                </div>
                @error('name_en')
                    @include('layouts.partials.text._error')
                @enderror
            </div>
        </div>
    </div>

    <!-- Content -->
    <div class="form_content">
        <label>Nội dung các bước hướng dẫn(vi) </label>
        <div class="form-group">
            {!! Form::textarea('content', null, array('class' => 'textarea', 'id' => 'content_vi', 'name' => 'content_vi')) !!}
        </div>
    </div>

    <div class="form_content">
        <label>Nội dung các bước hướng dẫn(en) </label>
        <div class="form-group">
            {!! Form::textarea('content_en', null, array('class' => 'textarea', 'id' => 'content_en', 'name' => 'content_en')) !!}
        </div>
    </div>

    {{-- Content Mobile --}}
    <div class="form_content">
        <label>Nội dung cho mobile(vi) </label>
        <div class="form-group">
            {!! Form::textarea('content_mobile', null, array('class' => 'textarea', 'id' => 'content_mobile_vi', 'name' => 'content_mobile')) !!}
        </div>
    </div>

    <div class="form_content">
        <label>Nội dung cho mobile(en) </label>
        <div class="form-group">
            {!! Form::textarea('content_mobile_en', null, array('class' => 'textarea', 'id' => 'content_mobile_en', 'name' => 'content_mobile_en')) !!}
        </div>
    </div>




        <div class="w-100 clearfix my-2">
            <button name="submit" type="submit" id="save" class="float-right btn ml-1 btn-primary">Lưu lại</button>
            <a href="{{ route('guideline.detail', ['id' => isset($guideid) ? $guideid : $data->guideline_id ]) }}" class="btn btn-secondary float-right mr-1">Hủy</a>
        </div>
</div>
{!! Form::close() !!}
<script>
    $("document").ready(() => {
        $('#content_vi').val('')
        $('#content_en').val('')
        $('#content_mobile_vi').val('')
        $('#content_mobile_en').val('')
        $('#content_vi').summernote('code', {!! json_encode(chuanHoa($data->content)) !!} );
        $('#content_en').summernote('code', {!! json_encode(chuanHoa($data->content_en)) !!});
        $('#content_mobile_vi').summernote('code', {!! json_encode(chuanHoa($data->content_mobile)) !!} );
        $('#content_mobile_en').summernote('code', {!! json_encode(chuanHoa($data->content_mobile_en)) !!});
    });
</script>
@endsection


