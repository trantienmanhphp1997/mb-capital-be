@extends('layouts.master')


@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="card-title my-3">Quản lý hướng dẫn</h3>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group search-expertise">
                            <div class="search-expertise inline-block">
                                <input type="text" placeholder="{{__('common.button.search')}}" name="search"
                                    class="form-control" id='input_vn_name' autocomplete="off" wire:model="">
                            </div>


                        </div>
                    </div>


                    <div class="col-md-9">
                        <a href="{{route('guideline_detail.create', ['guideid' => $id])}}" class="float-right"
                            style="border-radius: 11px; border:none;">
                            <button class="btn-sm btn-primary">
                                <i class="fa fa-plus"></i> Create
                            </button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="example2" class="table table-bordered table-hover dataTable dtr-inline"
                                        role="grid" aria-describedby="example2_info">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="example2"
                                                    rowspan="1" colspan="1">STT</th>
                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="example2"
                                                    rowspan="1" colspan="1">
                                                    Tên các bước (vi)
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1">
                                                    Tên các bước (en)
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1">
                                                    Nội dung (vi)
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1">
                                                    Nội dung (en)
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1">
                                                    Nội dung Mobile (vi)
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1">
                                                    Nội dung Mobile (en)
                                                </th>
                                                <th colspan="1">
                                                    Hành động
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($data->details as $i => $row)
                                            {{-- {{dd($row)}} --}}
                                            <tr class="odd">
                                                <td class="dtr-control sorting_1">{{ $i +1 }}</td>
                                                <td>{{$row->name}}</td>
                                                <td>{{$row->name_en}}</td>
                                                <td>{{$row->content}}</td>
                                                <td>{{$row->content_en}}</td>
                                                <td>{{$row->content_mobile}}</td>
                                                <td>{{$row->content_mobile_en}}</td>
                                                <td>
                                                    <a href="{{route('guideline_detail.edit',['id'=>$row->id])}}"
                                                        class="float-right"
                                                        style="border-radius: 11px; border:none;">
                                                        <img src="/images/pent2.svg" alt="pent">
                                                    </a>
                                                    <a href="{{route('guideline_detail.destroy',['id'=>$row->id])}}" data-toggle="modal" data-target="#deleteModal"
                                                       data-toggle="tooltip" data-original-title="Xóa"
                                                       class="btn-sm border-0 bg-transparent">
                                                        <img src="/images/trash.svg" alt="Delete">
                                                    </a>
                                                </td>
                                            </tr>
                                            @empty
                                                <td colspan='12' class='text-center'>Không tìm thấy dữ liệu</td>
                                            @endforelse

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('livewire.common._modalDelete')
</section>
@endsection
