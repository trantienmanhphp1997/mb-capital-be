<div class="body-content p-2">
    <div class="p-2 pb-3 d-flex align-items-center justify-content-between">
        <div class="">
            <h4 class="m-0">
                Fund Employee XRF
            </h4>
        </div>
        <div class="paginate">
            <div class="d-flex">
                <div class="">
                    <a href="{{ route('home') }}"><i class="fa fa-home"></i> Trang chủ</a>
                </div>
                <span class="px-2">/</span>
                <div class="">
                    <div class="disable">Fund Employee XRF</div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body p-2">
                        <div class="form-group row">
                <label for="" class="col-2 col-form-label">Search</label>
                <div class="col-4">
                    <input wire:model.debounce.1000ms="search" placeholder="Search"type="text" class="form-control">
                </div>
            </div>


            <div class="filter d-flex align-items-center justify-content-between mb-2">
                <button type="button" class="btn btn-primary" wire:click="resetSearch()"><i class="fa fa-undo"></i> Làm mới</button>
                <div>
                    <div style="float: left;text-align: center;">
                        <a href="#" data-toggle="modal" data-target="#modelCreateEdit" wire:click='create'>
                            <div class="btn-sm btn-primary">
                                <i class="fa fa-plus"></i> Tạo mới
                            </div>
                        </a>
                    </div>
                    <div style="margin-left:5px;float: left;text-align: center;">
                        <a href="#" data-toggle="modal" data-target="#modelExport" wire:click='create'>
                            <div class="btn-sm btn-success">
                                <i class="fa fa-download"></i> Export
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div wire:loading class="loader"></div>
            <table class="table table-bordered table-hover dataTable dtr-inline" role="grid" aria-describedby="example2_info">
                <thead>
                    <tr role='row'>
                        <th>STT</th>
                        <th>Nhân sự</th>
                        <th>Quỹ</th>
                        <th>Vai trò</th>
                        <th>Vai trò (EN)</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($data as $key => $row)
                        <tr>
                            <td>{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                            {{-- <td>{{$row->employee->fullname ?? ''}}</td>
                            <td>{{$row->fund->fullname ?? ''}}</td>
                            <td>{{$row->role_name}}</td> --}}
                            <td>{{$row->employee_name ?? ''}}</td>
                            <td>{{$row->fund_name ?? ''}}</td>
                            <td>{{$row->title}}</td>
                            <td>{{$row->title_en}}</td>
                            <td>
                                <button type="button" data-toggle="modal" data-target="#modelCreateEdit"  class="btn par6" title="update" wire:click='edit({{$row}})'>
                                    <img src="/images/pent2.svg" alt="pent">
                                </button>
                                @include('livewire.common.buttons._delete')
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @if(!isset($data) || !count($data))
                <div class="pb-2 pt-3 text-center">Không tìm thấy dữ liệu</div>
            @endif
        </div>
        {{$data->links()}}
    </div>
    {{--Start modal--}}
    <div wire:ignore.self class="modal fade" id="modelCreateEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{$this->mode=='create'?"Thêm mới":"Chỉnh sửa"}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" wire:click="resetValidate()">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label>Nhân sự <span class="text-danger">*</span> </label>
                        <select class="form-control" wire:model.lazy="employee_id">
                            @if ($this->mode=='create')
                            <option value="">--Chọn--</option>
                            @endif
                            @foreach ($employees as $value)
                                <option value="{{$value->id}}">{{$value->fullname}}</option>
                            @endforeach
                        </select>
                        @error("employee_id")
                            @include("layouts.partials.text._error")
                        @enderror
                    </div>

                    <div class="form_title">
                        <label>Vai trò</label>
                        <div class="row">
                            <div class="col">
                                <div class="input-group form-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text text-xs" id="nameViPrepend">
                                            vi
                                        </span>
                                    </div>
                                    <input wire:model.lazy="title_vi" type='text' class="form-control-sm form-control custom-input-control" disabled>
                                </div>
                                @error('title_vi')
                                    @include('layouts.partials.text._error')
                                @enderror
                            </div>
                            <div class="col">
                                <div class="input-group form-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text text-xs" id="nameEnPrepend">
                                            en
                                        </span>
                                    </div>
                                    <input wire:model.lazy="title_en" type='text' class="form-control-sm form-control custom-input-control" disabled>
                                </div>
                                @error('title_en')
                                    @include('layouts.partials.text._error')
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Quỹ <span class="text-danger">*</span></label>
                        <select class="form-control" wire:model.lazy="fund_id">
                            <option value="">--Chọn--</option>
                            @foreach ($funds as $value)
                                <option value="{{$value->id}}">{{$value->shortname}}</option>
                            @endforeach
                        </select>
                        @error("fund_id")
                            @include("layouts.partials.text._error")
                        @enderror
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" wire:click="resetValidate()">Đóng</button>
                    <button type="button" class="btn btn-primary" wire:click='saveData'>Lưu</button>
                </div>
            </div>
        </div>
    </div>
    @include('livewire.common._modalDelete')
    {{--end modal--}}

</div>

<script>
    $("document").ready(() => {
        window.livewire.on('closeModalCreateEdit', () => {
            $('#modelCreateEdit').modal('hide');
        });
    });
</script>