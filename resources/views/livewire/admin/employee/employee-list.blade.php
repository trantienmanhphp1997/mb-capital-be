<div class="body-content p-2">
    <div class="p-2 pb-3 d-flex align-items-center justify-content-between">
        <div class="">
            <h4 class="m-0">
                Danh sách nhân sự
            </h4>
        </div>
        <div class="paginate">
            <div class="d-flex">
                <div class="">
                    <a href="{{ route('home') }}"><i class="fa fa-home"></i> Trang chủ</a>
                </div>
                <span class="px-2">/</span>
                <div class="">
                    <div class="disable">Quản lý nhân sự</div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body p-2">
            <div class="filter d-flex align-items-center justify-content-between mb-2">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group search-expertise">
                            <div class="search-expertise inline-block">
                                <input type="text" placeholder="Tìm kiếm theo tên" name="search"
                                    class="form-control" id='input_vn_name' autocomplete="off" wire:model.debounce.1000ms="searchName">
                            </div>
                        </div>
                    </div>

                    <div wire:ignore class="col-md-3">
                        <select wire:model.debounce.1000ms="searchFund" class="form-control">
                            <option value=''>
                                --Chọn tên quỹ--
                            </option>
                            <option value="-1">Nhân sự công ty MB Capital</option>
                            @foreach($funds as $fund)
                                <option value="{{ $fund->id }}">
                                    {{ $fund->shortname }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div wire:ignore class="col-md-3">
                        <select wire:model.debounce.1000ms="searchType" class="form-control">
                            <option value="">
                                --Chọn thể loại--
                            </option>
                            @foreach ( $roles as $key => $item)
                                <option value="{{ $key }}">
                                    {{ $item }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div>
                    <div class="input-group">
                        <a href="{{route('admin.employee.create')}}">
                            <div class="btn-sm btn-primary">
                                <i class="fa fa-plus"></i> TẠO MỚI
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            @if (Session::has('error'))
                <div class="alert alert-danger" role="alert" style="width:300px">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ Session::get('error') }}</strong>
                </div>
            @endif

            @if (Session::has('message'))
                <div class="alert alert-success" role="alert" style="width:300px">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ Session::get('message') }}</strong>
                </div>
            @endif
            <table class="table table-striped">
                <thead class="">
                    <tr>
                        <th>STT</th>
                        <th>Họ và tên</th>
                        <th>Giới tính</th>
                        <th>Loại</th>
                        <th>Quỹ</th>
                        <th>Độ ưu tiên</th>
                        <th>Chức danh 1</th>
                        <th>Chức danh 2</th>
                        <th>Type Value</th>
                        <th>Tiểu sử</th>
                        <th>Ảnh</th>
                        <th>Hành động</th>
                    </tr>
                </thead>
                <tbody wire:sortable="updateOrder" >
                    @forelse($data as $key => $row)
                        <tr @if($searchType )wire:sortable.item="{{$row->priority}}" wire:sortable.handle @endif>
                            <td>{{ ($data->currentPage() - 1) * $data->perPage() + $loop->iteration }}</td>
                            <td>{!! boldTextSearch($row->fullname, $searchName) !!}</td>
                            <td>{{ ($row->sex == 1) ? 'Nam' : 'Nữ' }}</td>
                            <td>{{ $row->type }}</td>
                            <td>{{ empty($row->fund->shortname) ? '' : $row->fund->shortname }}</td>
                            <td>{{ $row->priority }}</td>
                            <td>{{ $row->title }}</td>
                            <td>{{ $row->title_2 }}</td>
                            <td>{{ $row->type_value }}</td>
                            <td>{{ $row->content }}</td>
                            <td>{{ stringLimit($row->img) }}</td>
                            <td>
                                <a href="{{route('admin.employee.edit', ["id"=>$row->id])}}" class="btn-sm border-0 bg-transparent">
                                        <img src="/images/pent2.svg" alt="Edit">
                                </a>
                                @include('livewire.common.buttons._delete')
                            </td>
                        </tr>
                    @empty
                        <td colspan='12' class='text-center'>Không tìm thấy dữ liệu</td>
                    @endforelse
                </tbody>
            </table>
        </div>
        @if(count($data))
            {{ $data->links() }}
        @endif
    </div>
    @include('livewire.common._modalDelete')
</div>
