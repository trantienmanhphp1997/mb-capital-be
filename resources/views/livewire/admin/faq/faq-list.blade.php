<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                 <h3 class="card-title my-3">QL câu hỏi Faq</h3>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group search-expertise">
                            <div class="search-expertise inline-block">
                                <input type="text" placeholder="{{__('common.button.search')}}" name="search" class="form-control" wire:model.debounce.1000ms="searchTerm"  id='input_vn_name' autocomplete="off">
                            </div>
                            
                        </div>
                    </div>
                    <div wire:ignore class="col-md-2">
                        <select wire:model.lazy="filterFundId" class="form-control">
                            <option value='-1'>
                                Chọn quỹ đầu tư
                            </option>
                            <option value=''>
                                Thường gặp
                            </option>
                            @foreach($listFund as $fund)
                                <option value='{{$fund->id}}'>
                                    {{$fund->fullname}}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-7">
                        <button type="button" class="float-right btn-sm btn-primary" style="border:none;" data-toggle="modal" data-target="#create_update_modal" wire:click="resetform()" >
                                <i class="fa fa-plus"></i> Tạo mới
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="example2" class="table table-bordered table-hover dataTable dtr-inline" role="grid" aria-describedby="example2_info">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">STT</th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Câu hỏi(VI)</th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Câu hỏi(EN)</th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Câu trả lời(VI)</th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Câu trả lời(EN)</th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Câu hỏi thường gặp</th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Quỹ</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if($data)
                                                @foreach($data as $row)
                                                    <tr class="odd">
                                                        <td class="dtr-control sorting_1">{{$loop->index + 1}}</td>
                                                        <td>{{$row->question}}</td>
                                                        <td>{{$row->question_en}}</td>
                                                        <td>{!! chuanHoa($row->content)!!}</td>
                                                        <td>{!! chuanHoa($row->content_en)!!}</td>
                                                        <td>{{$row->type}}</td>
                                                        <td>{{$row->fund->fullname ?? ''}}</td>
                                                        <td>
                                                        <button type="button" data-toggle="modal" data-target="#create_update_modal"  class="btn par6" title="update" wire:click="edit({{$row->id}})">
                                                             <img src="/images/pent2.svg" alt="pent">
                                                            </button>
                                                        @include('livewire.common.buttons._delete')

                                                        </td>
                                                    </tr>
                                                @endforeach

                                                </tbody>
                                            </table>
                                           {{$data->links()}}
                                           @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('livewire.common.modal._modalDelete')
            <div wire:ignore.self class="modal fade" id="create_update_modal" role="dialog" >
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">
                                @if(!$isEdit) Thêm mới FAQ
                                @else Chỉnh sửa FAQ
                                @endif
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true close-btn">×</span>
                            </button>
                        </div>
                        <div class="modal-body container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>
                                            Câu hỏi(VI) <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" name="question" class="form-control" wire:model.defer="question"
                                               placeholder="Câu hỏi(VI)">
                                        @error('question')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>
                                            Câu hỏi(EN) <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" name="question" class="form-control" wire:model.defer="question_en"
                                               placeholder="Câu hỏi(EN)">
                                        @error('question_en')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                        @enderror
                                    </div>
                                    <div wire:ignore class="form-group">
                                        <label>
                                            Câu trả lời(VI) <span class="text-danger">*</span>
                                        </label>
                                        <textarea id="content" class="form-control textarea"> </textarea>
                                        @error('content')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                        @enderror
                                    </div>
                                    <div wire:ignore class="form-group">
                                        <label>
                                            Câu trả lời(EN) <span class="text-danger">*</span>
                                        </label>
                                        <textare id="content_en" class="form-control textarea"></textarea>
                                        @error('content_en')
                                        <div class="text-danger mt-1">{{$message}}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Quỹ đầu tư</label>
                                        <select  wire:model.defer="fund_id" class="form-control">
                                            <option value={{null}}>Chọn quỹ đầu tư</option>
                                            @foreach($listFund as $item)
                                                <option value="{{$item->id}}">{{$item->fullname}}</option>
                                            @endforeach
                                        </select>
                                        @error('fund_id')
                                            <div class="text-danger mt-1">{{$message}}</div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Loại câu hỏi<span class="text-danger">&nbsp*</span></label>
                                        <select class="form-control form-control-lg" name="type" id="type" wire:model.defer="type">
                                            <option value={{null}}>Chọn loại câu hỏi</option>
                                            <option value={{App\Enums\EFaqType::FREQUENT}}>
                                                Thường gặp
                                            </option>
                                            <option value={{App\Enums\EFaqType::LESS_COMMON}}>
                                                Ít gặp
                                            </option>
                                        </select>
                                        @error('type') <span class="text-danger">{{ $message }}</span> @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" id="close-modal" wire:click.prevent="resetform()"
                                    class="btn btn-secondary close-btn" data-dismiss="modal">Đóng
                            </button>
                            <button type="button" id="btn-save" class="btn btn-primary close-modal">Lưu
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
     $("document").ready(() => {
        // window.livewire.on('categoryCreate', () => {
        //     $('#create_modal').modal('hide');
        //     $('#create_modal').modal('hide').data('bs.modal', null);
        //     $('#create_modal').remove();
        //     $('.modal-backdrop').remove();
        // });
        // window.livewire.on('categoryUpdate', () => {
        //     $('#updateCategory').modal('hide');
        //     $('#updateCategory').modal('hide').data('bs.modal', null);
        //     $('#updateCategory').remove();
        //     $('.modal-backdrop').remove();
        // });

        window.livewire.on('resetContent', () => {
            $('#content').summernote('code', '');
            $('#content_en').summernote('code', '');
        });

        window.livewire.on('closeModal', () => {
            $('#close-modal').click();
        });

        window.livewire.on('setContentEdit', (content, content_en) => {
            console.log('xxxx');
            $('#content').summernote('code', content);
            $('#content_en').summernote('code', content_en);
        });

        $('#btn-save').click(function() {
            window.livewire.emit('set-content', $('#content').summernote('code'), $('#content_en').summernote('code'));
        })
    });
</script>
</section>
