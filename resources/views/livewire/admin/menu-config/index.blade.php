<section class="content">
    <div class="container-fluid">
        <div class="row mx-2">
            <div class="col-md-12">
                <h3 class="card-title my-3">{{__('admin/menu-config.title')}}</h3>
            </div>
            <div class="col-md-12">
                <button type="button" class="btn btn-primary my-3 float-right" data-toggle="modal" data-target="#exampleModal">
                    {{__('admin/menu-config.create')}}
                </button>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">{{__('admin/menu-config.create')}}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="name">{{__('admin/menu-config.table.name')}}</label>
                                            <input type="text" name="name" class="form-control" id="name" placeholder="{{__('admin/menu-config.table.name')}}" wire:model="name">
                                        </div>
                                        <div class="form-group">
                                            <label for="name">{{__('admin/menu-config.table.name')}} (En)</label>
                                            <input type="text" name="name" class="form-control" id="name" placeholder="{{__('admin/menu-config.table.name')}}" wire:model="name_en">
                                        </div>
                                        <div class="form-group">
                                            <label for="name">{{__('admin/menu-config.table.type')}}</label>
                                            <select class="form-control" wire:model="type">
                                                <option value="{{\App\ENums\EMenuConfigType::TOP}}">{{
                                                \App\ENums\EMenuConfigType::valueToName(\App\ENums\EMenuConfigType::TOP)}}</option>
                                                <option value="{{\App\ENums\EMenuConfigType::FOOTER}}">{{
                                                \App\ENums\EMenuConfigType::valueToName(\App\ENums\EMenuConfigType::FOOTER)}}</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="menu_name">{{__('admin/menu-config.table.menu_name')}}</label>
                                            <select class="form-control" wire:model="menu_id">
                                                <option value="">{{__('admin/menu-config.choose_menu')}}</option>
                                                @foreach($menuList as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="parent">{{__('admin/menu-config.table.parent')}}</label>
                                            <select class="form-control" wire:model="parent_id">
                                                <option value=''>{{__('admin/menu-config.not_selected')}}</option>
                                                @foreach($data as $item)
                                                    <option value="{{$item->id}}">{{$item->name}} - {{__('admin/menu-config.table.level')}} {{$item->level}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('common.button.cancel')}}</button>
                                        <button type="button" class="btn btn-primary" wire:click="saveData()">{{__('common.button.save')}}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="example2" class="table table-bordered table-hover dataTable dtr-inline" role="grid" aria-describedby="example2_info">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">{{__('admin/menu-config.table.name')}}</th>
                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">{{__('admin/menu-config.table.name')}} (En)</th>

                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">{{__('admin/menu-config.table.level')}}</th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">{{__('admin/menu-config.table.type')}}</th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">{{__('admin/menu-config.table.menu_name')}}</th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">{{__('admin/menu-config.table.parent')}}</th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">{{__('admin/menu-config.table.action')}}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($data as $row)
                                                <tr class="odd">
                                                    <td>{{$row->name}}</td>
                                                    <td>{{$row->name_en}}</td>
                                                    <td>{{$row->level}}</td>
                                                    <td>{{\App\ENums\EMenuConfigType::valueToName($row->type)}}</td>
                                                    <td>{{$row->menu_id}}</td>
                                                    <td>{{$row->parent_name}}</td>
                                                    <td>
                                                        <button type="button" data-toggle="modal" data-target="#exampleModal"  class="btn par6" title="update" wire:click="edit({{$row}})">
                                                             <img src="/images/pent2.svg" alt="pent">
                                                            </button>
                                                        @include('livewire.common.buttons._delete')
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {{$data->links()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('livewire.common.modal._modalDelete')
</section>
<script>
    $("document").ready(() => {
        window.livewire.on('closeModal', () => {
            $('#exampleModal').modal('hide');
        });
    });
</script>