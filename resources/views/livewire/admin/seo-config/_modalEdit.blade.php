<div wire:ignore.self class="modal fade" id="edit-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    <label>CẬP NHẬT</label>
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true close-btn">×</span>
                </button>
            </div>
            <div class="modal-body container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label>Name</label>
                                    <input type="text" id="name" class="form-control" wire:model.lazy="name" placeholder="Name">
                                </div>
                                <div class="col">
                                    <label>Alias</label>
                                    <input type="text" class="form-control" wire:model.lazy="alias" placeholder="Alias" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col">
                                    <label>Note</label>
                                    <input type="text" class="form-control" wire:model.lazy="note" placeholder="Note">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Meta</label>
                            <div class="row">
                                <div class="col">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text text-xs" id="nameViPrepend">
                                                vi
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" wire:model.lazy="meta" placeholder="Meta (vi)">
                                    </div>
                                </div>
                                <div class="col mb-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text text-xs" id="nameEnPrepend">
                                                en
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" wire:model.lazy="meta_en" placeholder="Meta (en)">
                                    </div>
                                </div>
                            </div>
                            <label>Title</label>
                            <div class="row">
                                <div class="col">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text text-xs" id="nameViPrepend">
                                                vi
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" wire:model.lazy="title" placeholder="Title (vi)">
                                    </div>
                                </div>
                                <div class="col mb-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text text-xs" id="nameEnPrepend">
                                                en
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" wire:model.lazy="title_en" placeholder="Title (en)">
                                    </div>
                                </div>
                            </div>
                            <label>Description</label>
                            <div class="row">
                                <div class="col">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text text-xs" id="nameViPrepend">
                                                vi
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" wire:model.lazy="description" placeholder="Description (vi)">
                                    </div>
                                </div>
                                <div class="col mb-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text text-xs" id="nameEnPrepend">
                                                en
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" wire:model.lazy="description_en" placeholder="Description (en)">
                                    </div>
                                </div>
                            </div>
                            <label>Keywords</label>
                            <div class="row">
                                <div class="col">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text text-xs" id="nameViPrepend">
                                                vi
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" wire:model.lazy="keywords" placeholder="Keywords (vi)">
                                    </div>
                                </div>
                                <div class="col mb-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text text-xs" id="nameEnPrepend">
                                                en
                                            </span>
                                        </div>
                                        <input type="text" class="form-control" wire:model.lazy="keywords_en" placeholder="Keywords (en)">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="close-modal-edit" class="btn btn-secondary close-btn" data-dismiss="modal">Đóng</button>
                <button type="button" wire:click="update" class="btn btn-primary close-modal">Lưu</button>
            </div>
        </div>
    </div>
</div>