<div class="body-content p-2">
    <div class="card">
        <div class="card-body p-2">
            <div class="filter d-flex align-items-center justify-content-between mb-2">
                <div class="row">
                    <div class="col">
                        <div class="form-group search-expertise">
                            <div class="search-expertise inline-block">
                                <input type="text" placeholder="Tìm kiếm" name="search"
                                    class="form-control" id='input_vn_name' autocomplete="off" wire:model.debounce.500ms="searchTerm">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <table class="table table-bordered table-hover dataTable dtr-inline">
                <thead class="">
                    <tr>
                        <th>STT</th>
                        <th>Tên</th>
                        <th>Alias</th>
                        <th>Meta</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Keywords</th>
                        <th>Hành động</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($data as $row)
                        <tr>
                            <td>{{ ($data->currentPage() - 1) * $data->perPage() + $loop->iteration }}</td>
                            <td>{!! boldTextSearch($row->name, $searchTerm) !!}</td>
                            <td>{!! boldTextSearch($row->alias, $searchTerm) !!}</td>
                            <td>{!! boldTextSearch($row->meta, $searchTerm) !!}</td>
                            <td>{!! boldTextSearch($row->title, $searchTerm) !!}</td>
                            <td>{!! boldTextSearch($row->description, $searchTerm) !!}</td>
                            <td>{!! boldTextSearch($row->keywords, $searchTerm) !!}</td>
                            <td>
                                <a href="#" data-toggle="modal" data-target="#edit-modal" wire:click="edit({{ $row->id }})">
                                    <img src="/images/pent2.svg" alt="pent">
                                </a>
                            </td>
                        </tr>
                    @empty
                        <td colspan='12' class='text-center'>Không tìm thấy dữ liệu</td>
                    @endforelse
                </tbody>
            </table>
        </div>
        @if(count($data))
            {{ $data->links() }}
        @endif
    </div>
    @include('livewire.admin.seo-config._modalEdit')
</div>

<script>
    window.livewire.on('close-modal-edit', () => {
        $('#close-modal-edit').click();
    });
</script>