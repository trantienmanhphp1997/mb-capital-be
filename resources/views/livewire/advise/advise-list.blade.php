<div class="body-content p-2">
    <div class="p-2 pb-3 d-flex align-items-center justify-content-between">
        <div class="">
            <h4 class="m-0">
                Quản lý tư vấn
            </h4>
        </div>
        <div class="paginate">
            <div class="d-flex">
                <div class="">
                    <a href="{{ route('home') }}"><i class="fa fa-home"></i> Trang chủ</a>
                </div>
                <span class="px-2">/</span>
                <div class="">
                    <div class="disable">Quản lý tư vấn</div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-body p-2">
            {{-- <div class="filter d-flex align-items-center justify-content-between mb-2"> --}}
                {{-- <a href="{{route('admin.new.create')}}">
                    <div class="btn-sm btn-primary">
                        <i class="fa fa-plus"></i> Create
                    </div>
                </a> --}}
                {{-- <div class="">
                    <form class="form-inline">
                        <div class="input-group">
                            <input type="text" class="form-control-sm form-control custom-input-control" id="keyword" placeholder="Search" aria-describedby="keywordAppend" name="keyword">
                            <div class="input-group-append">
                                <span class="input-group-text" id="keywordAppend">
                                    <div class="text-xs">
                                        <i class="fa fa-search"></i>
                                    </div>
                                </span>
                            </div>
                        </div>
                    </form>
                </div> --}}

                <form>
                    <div class="form-group row">
                        <label for="Name" class="col-2 col-form-label">Họ và tên</label>
                        <div class="col-4">
                            <input wire:model.debounce.1000ms="searchName" type="text" class="form-control" >
                        </div>
                        <label for="Email_Phone" class="col-2 col-form-label ">Email/Số điện thoại</label>
                        <div class="col-4">
                            <input wire:model.debounce.1000ms="searchEmailOrPhone" type="text" class="form-control" >
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="Content" class="col-2 col-form-label ">Nội dung cần tư vấn</label>
                        <div class="col-4">
                            <input wire:model.debounce.1000ms="searchContent" type="text" class="form-control" >
                        </div>

                        <label for="IP" class="col-2 col-form-label ">IP</label>
                        <div class="col-4">
                            <input wire:model.debounce.1000ms="searchIP" type="text" class="form-control" >
                        </div>
                    </div>
                   <div class="form-group row justify-content-center">
                        @include('layouts.partials.button._reset')  
                    </div>
                </form>
            {{-- </div> --}}
            
            <table class="table table-striped">
                <thead class="">
                    <tr>
                        <th>STT</th>
                        <th>Họ và tên</th>
                        <th>Email/Số điện thoại</th>
                        <th>Nội dung cần tư vấn</th>
                        <th>IP của người dùng</th>
                        <th>Thời gian gửi yêu cầu</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $html = '';?>
                    @if(isset($data) && count($data) > 0)
                        @foreach($data as $key => $dt)
                            <tr>
                                <td>{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                                <td>{{$dt->name}}</td>
                                <td>{{$dt->email_phone}}</td>
                                <td>{{$dt->advise_content}}</td>
                                <td>{{$dt->IP}}</td>
                                <td>{{reformatDate($dt->created_at, 'd-m-Y h:m:s')}}</td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
            @if(!isset($data) || !count($data))
                <div class="pb-2 pt-3 text-center">Không tìm thấy dữ liệu</div>
            @endif
        </div>
        {{$data->links()}}
    </div>
</div>