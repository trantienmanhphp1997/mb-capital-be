
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                 <h3 class="card-title my-3">Danh sách log truy cập</h3>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group search-expertise">
                            <div class="search-expertise inline-block">
                                <input type="text" placeholder="{{__('common.button.search')}}" name="search" class="form-control" wire:model.debounce.1000ms="search"  id='input_vn_name' autocomplete="off">
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-md-12" style="overflow-x: scroll;">
                                    <table id="" class="table table-bordered table-hover dataTable dtr-inline" role="grid" aria-describedby="example2_info">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">STT</th>
                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Địa chỉ IP</th>
                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">URL nguồn</th>
                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">URL đang truy cập</th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Thiết bị</th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Trình duyệt</th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Cookies</th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Ghi chú</th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Tác nhân người dùng</th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1">Thời gian truy cập</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             @if($data)
                                                @foreach($data as $row)
                                                    <tr class="odd">
                                                        <td class="dtr-control sorting_1">{{($data->currentPage() - 1) * $data->perPage() + $loop->iteration}}</td>
                                                        <td>{{$row->ip_address}}</td>
                                                        <td>{{$row->url_previous}}</td>
                                                        <td>{{$row->url_current}}</td>
                                                        <td>{{$row->device}}</td>
                                                        <td>{{$row->browser}}</td>
                                                        <td>{{$row->cookies}}</td>
                                                        <td>{{$row->note}}</td>
                                                        <td>{{$row->user_agent}}</td>  
                                                        <td>{{reformatDate($row->created_at, 'd-m-Y h:m:s')}}</td>                                                   
                                                    </tr>
                                                @endforeach

                                                </tbody>
                                            </table>
                                           {{$data->links()}}
                                           @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

