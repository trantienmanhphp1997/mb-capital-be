<div>
    @if (session()->has('message'))
        <div class="flex mx-auto">
            <div class="alert alert-success">
                <span class="inline-block align-middle mr-8">
                <b class="capitalize">{{ __('Success') }}!</b> {{ session('message') }}
                </span>
                <button wire:click="clearFlash()"
                        class="float-right btn-xs btn btn-outline-success">
                    <span>×</span>
                </button>
            </div>
        </div>
    @endif
    <div>
        <div class="row">
            <div class="col-md-3">
                <input wire:model="search"
                       class="form-control"
                       id="search" type="text" name="search" wire:model="search" required="required"
                       autofocus="autofocus"/>
            </div>
            <div class="col-md-9 float-right">
                <button type="button"
                        class="btn btn-primary float-right"
                        wire:click="create">
                   {{ __('Add New Record') }}
                </button>
            </div>
        </div>
    </div>
    <div class="flex row card">
            <div class="card-body">
                <table width="100%" class="table table-bordered">
                    <thead>
                    <tr>

                        <th>FIRSTNAME
                            </th>

<th>LASTNAME
                            </th>

<th>FULLNAME
                            </th>

<th>SEX
                            </th>

<th>TITLE
                            </th>

<th>TYPE
                            </th>

<th>TITLE EN
                            </th>

<th>TYPE VALUE
                            </th>

<th>TYPE VALUE EN
                            </th>

<th>FUND ID
                            </th>

<th>CONTENT
                            </th>

<th>CONTENT EN
                            </th>

<th>IMG
                            </th>

<th>TITLE 2
                            </th>

<th>TITLE 2 EN
                            </th>



                        <th scope="col">
                            <span class="sr-only">Actions</span>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($rows as $row)
<tr> <td>{{ $row->firstname}}</td>

 <td>{{ $row->lastname}}</td>

 <td>{{ $row->fullname}}</td>

 <td>{{ $row->sex}}</td>

 <td>{{ $row->title}}</td>

 <td>{{ $row->type}}</td>

 <td>{{ $row->title en}}</td>

 <td>{{ $row->type value}}</td>

 <td>{{ $row->type value en}}</td>

 <td>{{ $row->fund id}}</td>

 <td>{{ $row->content}}</td>

 <td>{{ $row->content en}}</td>

 <td>{{ $row->img}}</td>

 <td>{{ $row->title 2}}</td>

 <td>{{ $row->title 2 en}}</td>

<td>
                                <a href="#" class="text-primary" wire:click.prevent="edit({{ $row->id }})">
<svg xmlns="http://www.w3.org/2000/svg" style="width:20px; height: 20px;" viewBox="0 0 20 20" fill="currentColor">
  <path d="M17.414 2.586a2 2 0 00-2.828 0L7 10.172V13h2.828l7.586-7.586a2 2 0 000-2.828z" />
  <path fill-rule="evenodd" d="M2 6a2 2 0 012-2h4a1 1 0 010 2H4v10h10v-4a1 1 0 112 0v4a2 2 0 01-2 2H4a2 2 0 01-2-2V6z" clip-rule="evenodd" />
</svg>
</a>
                                <a href="#" class="text-danger" wire:click.prevent="confirmDelete({{ $row->id }})"> <svg xmlns="http://www.w3.org/2000/svg" style="width:20px; height: 20px;" viewBox="0 0 20 20" fill="currentColor">
                                        <path fill-rule="evenodd" d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z" clip-rule="evenodd" />
                                    </svg></a>
                            </td></tr>@empty  <tr><td>No Records Found</td></tr>   @endforelse

                    </tbody>
                </table>
                <div class="p-2">
                    {{ $rows->links() }}
                </div>
        </div>


    </div>


    {{--    create / edit form --}}

       <div class="modal fade" wire:ignore.self id="showForm" tabindex="-1" role="dialog" aria-labelledby="showFormLabel" aria-hidden="true">
           <div class="modal-dialog" role="document">
               <div class="modal-content">
                   <div class="modal-header">
                       <h5 class="modal-title" id="showFormLabel"> {{ $mode == 'create' ? 'Add New Record' : 'Update Record ' }}</h5>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                       </button>
                   </div>
                   <div class="modal-body">
                        <div class='form-group'><label for='firstname'>Firstname</label><input type='text' class='form-control @error('firstname')  is-invalid @enderror' wire:model='firstname'>@error('firstname')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='lastname'>Lastname</label><input type='text' class='form-control @error('lastname')  is-invalid @enderror' wire:model='lastname'>@error('lastname')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='fullname'>Fullname</label><input type='text' class='form-control @error('fullname')  is-invalid @enderror' wire:model='fullname'>@error('fullname')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='sex'>Sex</label><input type='text' class='form-control @error('sex')  is-invalid @enderror' wire:model='sex'>@error('sex')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='title'>Title</label><input type='text' class='form-control @error('title')  is-invalid @enderror' wire:model='title'>@error('title')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='type'>Type</label><input type='text' class='form-control @error('type')  is-invalid @enderror' wire:model='type'>@error('type')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='title_en'>Title en</label><input type='text' class='form-control @error('title_en')  is-invalid @enderror' wire:model='title_en'>@error('title_en')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='type_value'>Type value</label><input type='text' class='form-control @error('type_value')  is-invalid @enderror' wire:model='type_value'>@error('type_value')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='type_value_en'>Type value en</label><input type='text' class='form-control @error('type_value_en')  is-invalid @enderror' wire:model='type_value_en'>@error('type_value_en')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='fund_id'>Fund id</label><input type='text' class='form-control @error('fund_id')  is-invalid @enderror' wire:model='fund_id'>@error('fund_id')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='content'>Content</label><input type='text' class='form-control @error('content')  is-invalid @enderror' wire:model='content'>@error('content')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='content_en'>Content en</label><input type='text' class='form-control @error('content_en')  is-invalid @enderror' wire:model='content_en'>@error('content_en')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='img'>Img</label><input type='text' class='form-control @error('img')  is-invalid @enderror' wire:model='img'>@error('img')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='title_2'>Title 2</label><input type='text' class='form-control @error('title_2')  is-invalid @enderror' wire:model='title_2'>@error('title_2')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='title_2_en'>Title 2 en</label><input type='text' class='form-control @error('title_2_en')  is-invalid @enderror' wire:model='title_2_en'>@error('title_2_en')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>

                   </div>
                   <div class="modal-footer">
                       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                       <button type="button" @if($mode == 'create') wire:click="store()" @else wire:click="update()" @endif  class="btn btn-primary">
                         {{ $mode == 'create' ? 'Save Record' : 'Update Record' }}
                       </button>
                   </div>
               </div>
           </div>
       </div>
    {{--    /create /edit form--}}


    {{--    delete popup--}}
    <div wire:ignore>
            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Are You Sure?</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                           This Action Can not be Undone.
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" wire:click="destroy()" class="btn btn-danger">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {{--    /delete popup--}}
</div>
