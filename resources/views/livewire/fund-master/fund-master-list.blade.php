<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="card-title my-3">Cấu hình quỹ đầu tư</h3>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group search-expertise">
                            <div class="search-expertise inline-block">
                                <input type="text" placeholder="{{__('common.button.search')}}" name="search"
                                    class="form-control" id='input_vn_name' autocomplete="off" wire:model="searchTerm">
                            </div>


                        </div>
                    </div>

                    <div wire:ignore class="col-md-2">
                        <select wire:model.lazy="typeFilterFund" class="form-control" onchange="showFundMasterTypes(this.options[this.selectedIndex].getAttribute('data-type'))">
                            <option value=''>
                                Chọn tên quỹ
                            </option>
                            @foreach($getTypeFund as $gtf)
                                <option value='{{$gtf->id}}' data-type='{{$gtf->type}}'>
                                    {{ $gtf->shortname }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div wire:ignore class="col-md-2">
                        <select wire:model.lazy="typeFilter" class="form-control" id="fund-master-type">
                            <option value=''>
                                {{__('master/masterManager.menu_name.type')}}
                            </option>
                                <option value=''>
                                </option>
                        </select>
                    </div>

                    <div wire:ignore class="col-md-2">
                        <select wire:model.lazy="vkeyFilter" class="form-control">
                            <option value=''>
                                Chọn tên cấu hình
                            </option>
                            @foreach($getVkey as $gvk)
                                <option value='{{$gvk->v_key}}'>
                                    {{ $gvk->v_key }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <input type="hidden" id="fund-master-value" value="{{ json_encode(chuanHoa($fundMasterTypes)) }}">

                    <div class="col-md-3">
                        <a href="{{route('admin.fundmaster.create')}}" class="float-right"
                            style="border-radius: 11px; border:none;">
                            <div class="btn-sm btn-primary">
                                <i class="fa fa-plus"></i> Tạo mới
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="example2" class="table table-bordered table-hover dataTable dtr-inline"
                                        role="grid" aria-describedby="example2_info">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="example2"
                                                    rowspan="1" colspan="1">STT</th>
                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="example2"
                                                    rowspan="1" colspan="1">
                                                    Tên quỹ đầu tư
                                                </th>
                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="example2"
                                                    rowspan="1" colspan="1">
                                                    Tiêu đề
                                                </th>
                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="example2"
                                                    rowspan="1" colspan="1">
                                                    Nội dung
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1">
                                                    Phân loại
                                                </th>
                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="example2"
                                                    rowspan="1" colspan="1">
                                                    URL
                                                </th>
                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="example2"
                                                    rowspan="1" colspan="1">
                                                    Sắp xếp thứ tự
                                                </th>
                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="example2"
                                                    rowspan="1" colspan="1">
                                                    Giá trị số
                                                </th>
                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="example2"
                                                    rowspan="1" colspan="1">
                                                    Ghi chú
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1">
                                                    Tên cấu hình
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1">
                                                    Hình ảnh
                                                </th>
                                                <th colspan="1">
                                                    Hành động
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody wire:sortable="updateOrder">
                                            @if($data)
                                            @foreach($data as $row)
                                            {{-- {{dd($row)}} --}}
                                            <tr @if($typeFilter )wire:sortable.item="{{$row->order_number}}" wire:sortable.handle @endif class="odd">
                                                <td class="dtr-control sorting_1">{{$row->id}}</td>
                                                <td>{{$row->fund_name}}</td>
                                                <td>{{$row->title}}</td>
                                                @if(strlen($row->content)>50)
                                                <td>{{substr($row->content,0,50)}}<span class="read-more-show hide_content">...Đọc thêm</span><span class="read-more-content">{{substr($row->content,50,strlen($row->content))}}<span class="read-more-hide hide_content">...Ẩn bớt</span></span></td>
                                                @else
                                                <td>{{$row->content}}</td>
                                                @endif
                                                <td>{{ \App\Enums\EFundMaster::valueToName($row->fund->type??null, $row->type) }}</td>
                                                <td>{{$row->url}}</td>
                                                <td>{{$row->order_number}}</td>
                                                <td>{{$row->number_value}}</td>
                                                <td>{{$row->note}}</td>
                                                <td>{{$row->v_key}}</td>
                                                <td>
                                                
                                                    @if(!empty($row->img))
                                                        <img src="{{(strpos(url()->current(),'mbcap-cms.evotek.vn')!==false)?'https://mbcapital.com.vn/'.Storage::url($row->img):Storage::url($row->img)}}" alt="" width="70px" height="70px">
                                                    @else
                                                        No image
                                                    @endif
                                                </td>
                                                <td class="d-flex">
                                                    <a href="{{route('admin.fundmaster.edit',['id'=>$row->id])}}">
                                                        <img src="/images/pent2.svg" alt="pent">
                                                    </a>
                                                    <button type="button" href="#" data-toggle="modal" data-target="#deleteModal" data-toggle="tooltip" data-original-title="Xóa" wire:click="deleteId({{$row->id}})" class="btn-sm border-0 bg-transparent">
                                                        <img src="/images/trash.svg" alt="Delete">
                                                    </button>
                                                </td>
                                            </tr>
                                            @endforeach

                                        </tbody>
                                    </table>
                                    {{$data->links()}}
                                    @endif
                                    </tbody>
                                    </table>
                                    @if(!isset($data) || !count($data))
                                    <div class="pb-2 pt-3 text-center">Không tìm thấy dữ liệu</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('livewire.common._modalDelete')
</section>

<script>
    var fundMasterTypes = JSON.parse(document.getElementById('fund-master-value').value);
    function showFundMasterTypes(fundType) {
        let $select = document.getElementById('fund-master-type');
        // console.log('hello');
        if(!fundType) return;
        $select.innerHTML = `
        <option value="0">
            {{__('master/masterManager.menu_name.type')}}
        </option>
        `;

        let types = fundMasterTypes[fundType];
        for(let key in types) {
            $select.innerHTML += `
                <option value="${key}">${types[key]}</option>
            `;
        }
    }

</script>

<script type="text/javascript">
    $('.read-more-content').addClass('hide_content')
            $('.read-more-show, .read-more-hide').removeClass('hide_content')

            // Set up the toggle effect:
            $('.read-more-show').on('click', function(e) {
              $(this).next('.read-more-content').removeClass('hide_content');
              $(this).addClass('hide_content');
              e.preventDefault();
            });

            // Changes contributed by @diego-rzg
            $('.read-more-hide').on('click', function(e) {
              var p = $(this).parent('.read-more-content');
              p.addClass('hide_content');
              p.prev('.read-more-show').removeClass('hide_content'); // Hide only the preceding "Read More"
              e.preventDefault();
            });
</script>
