<div class="body-content p-2">
    <div class="p-2 pb-3 d-flex align-items-center justify-content-between">
        <div class="">
            <h4 class="m-0">
                Quản lý NAV quỹ
            </h4>
        </div>
        <div class="paginate">
            <div class="d-flex">
                <div class="">
                    <a href="{{ route('home') }}"><i class="fa fa-home"></i> Trang chủ</a>
                </div>
                <span class="px-2">/</span>
                <div class="">
                    <div class="disable">Quản lý NAV quỹ</div>
                </div>
            </div>
        </div>
    </div>
    @if(Session::get('success'))
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">x</a>
            <strong>{{ Session::get('success') }}</strong>
        </div>
    @endif
    <div class="card">
        <div class="card-body p-2">
            <div class="filter d-flex align-items-center justify-content-between mb-2">
                <div class="row">
                    {{-- <div class="col-md-4">
                        <div class="form-group search-expertise">
                            <div class="search-expertise inline-block">
                                <input type="text" placeholder="Tìm kiếm theo tiêu đề" name="search"
                                    class="form-control" id='input_vn_name' autocomplete="off" wire:model.debounce.1000ms="searchName">
                            </div>
                        </div>
                    </div> --}}

                    <div class="col-md-3">
                        <select wire:model.debounce.1000ms="searchFund" class="form-control">
                            <option value=''>
                                --Chọn tên quỹ--
                            </option>
                            <option value='type_1'>VNINDEX</option>
                            @foreach($funds as $fund)
                                <option value="{{ $fund->id }}">
                                    {{ $fund->shortname }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-3">
                        <input type="date" class="form-control" placeholder="dd-mm-yyyy" name="fromDate" wire:model.debounce.1000ms="from_date" autocomplete="off">
                    </div>
                    <div class="col-md-3">
                        <input type="date" class="form-control" placeholder="dd-mm-yyyy" name="toDate" wire:model.debounce.1000ms="to_date" autocomplete="off">
                    </div>
                    <div class="col-md-3">
                        <div id="category-table_filter" class="dataTables_filter">
                            <button name="submit" type="submit" class="btn btn-primary" data-target="#exampleModal"
                            data-toggle="modal" type="button" {{count($data) ? '': 'disabled'}}><i
                                    class="fa fa-file-excel-o"></i> Export file</button>
                        </div>
                    </div>
                </div>
                
                

                <div>
                    <div class="input-group">
                        <a href="{{ route('admin.fundnav.create') }}"
                            data-toggle="tooltip" data-original-title="Tải tệp lên" wire:click="">
                            <div class="btn-sm btn-primary">
                                <i class="fa fa-plus"></i> TẠO MỚI
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            
            <table class="table table-striped">
                <thead class="">
                    <tr>
                        <th style="width: 15px;">STT</th>
                        <th>Tên quỹ đầu tư</th>
                        <th>Số tiền</th>
                        <th>Ngày công bố</th>
                        <th style="width: 100.5px;">Hành động</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $html = '';?>
                    @if(isset($data) && count($data) > 0)
                        @foreach($data as $key => $val)
                            <tr>
                                <td>{{ ($data->currentPage() - 1) * $data->perPage() + $loop->iteration }}</td>
                                <td>
                                    @if ($val->type == 1)
                                        <span>VNINDEX</span>
                                    @else
                                        <span>{{ $val->fundname }}</span>
                                    @endif
                                </td>
                                <td>{{ numberFormat($val->amount) }}</td>
                                <td>{{ ReFormatDate($val->trading_session_time,'d-m-Y') }}</td>
                                <td class="text-center">
                                    <a href="{{route('admin.fundnav.edit',['id'=>$val->id])}}" 
                                            class="btn-sm border-0 bg-transparent mr-1 show_modal_edit">
                                            <img src="/images/pent2.svg" alt="Edit">
                                    </a>
                                    <a href="#" data-toggle="modal" data-target="#deleteModal" 
                                        data-toggle="tooltip" data-original-title="Xóa"
                                        wire:click="deleteId({{$val->id}})" 
                                        class="btn-sm border-0 bg-transparent">
                                        <img src="/images/trash.svg" alt="Delete">
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
            @if(!isset($data) || !count($data))
                <div class="pb-2 pt-3 text-center">Không tìm thấy dữ liệu</div>
            @endif
        </div>
        @if(count($data))
            {{ $data->links() }}
        @endif
    </div>
    @include('livewire.common._modalDelete')
    @include('livewire.common._modalExportFile')
</div>