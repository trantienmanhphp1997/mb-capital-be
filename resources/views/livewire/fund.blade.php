<div>
    @if (session()->has('message'))
        <div class="flex mx-auto">
            <div class="alert alert-success">
                <span class="inline-block align-middle mr-8">
                <b class="capitalize">{{ __('Success') }}!</b> {{ session('message') }}
                </span>
                <button wire:click="clearFlash()"
                        class="float-right btn-xs btn btn-outline-success">
                    <span>×</span>
                </button>
            </div>
        </div>
    @endif
    <div>
        <div class="row">
            <div class="col-md-3">
                <input wire:model="search"
                       class="form-control"
                       id="search" type="text" name="search" wire:model="search" required="required"
                       autofocus="autofocus"/>
            </div>
            <div class="col-md-9 float-right">
                <button type="button"
                        class="btn btn-primary float-right"
                        wire:click="create">
                   {{ __('Add New Record') }}
                </button>
            </div>
        </div>
    </div>
    <div class="flex row card">
            <div class="card-body">
                <table width="100%" class="table table-bordered">
                    <thead>
                    <tr>

                        <th>SHORTNAME
                            </th>

<th>SHORTNAME EN
                            </th>

<th>FULLNAME
                            </th>

<th>FULLNAME EN
                            </th>

<th>DESCRIPTION
                            </th>

<th>DESCRIPTION EN
                            </th>

<th>CONTENT
                            </th>

<th>CONTENT EN
                            </th>

<th>CREATED BY
                            </th>

<th>TYPE
                            </th>

<th>CURRENT NAV
                            </th>

<th>GROWTH
                            </th>

<th>SLUG
                            </th>

<th>SLUG EN
                            </th>

<th>FUN CODE
                            </th>

<th>INTEREST
                            </th>

<th>PARENT ID
                            </th>

<th>PRIORITY
                            </th>

<th>SHORTNAME2
                            </th>

<th>SHORTNAME2 EN
                            </th>

<th>FULLNAME2
                            </th>

<th>FULLNAME2 EN
                            </th>

<th>SHORTNAME2
                            </th>

<th>SHORTNAME2 EN
                            </th>



                        <th scope="col">
                            <span class="sr-only">Actions</span>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($rows as $row)
<tr> <td>{{ $row->shortname}}</td>

 <td>{{ $row->shortname en}}</td>

 <td>{{ $row->fullname}}</td>

 <td>{{ $row->fullname en}}</td>

 <td>{{ $row->description}}</td>

 <td>{{ $row->description en}}</td>

 <td>{{ $row->content}}</td>

 <td>{{ $row->content en}}</td>

 <td>{{ $row->created by}}</td>

 <td>{{ $row->type}}</td>

 <td>{{ $row->current nav}}</td>

 <td>{{ $row->growth}}</td>

 <td>{{ $row->slug}}</td>

 <td>{{ $row->slug en}}</td>

 <td>{{ $row->fun code}}</td>

 <td>{{ $row->interest}}</td>

 <td>{{ $row->parent id}}</td>

 <td>{{ $row->priority}}</td>

 <td>{{ $row->shortname2}}</td>

 <td>{{ $row->shortname2 en}}</td>

 <td>{{ $row->fullname2}}</td>

 <td>{{ $row->fullname2 en}}</td>

 <td>{{ $row->shortname2}}</td>

 <td>{{ $row->shortname2 en}}</td>

<td>
                                <a href="#" class="text-primary" wire:click.prevent="edit({{ $row->id }})">
<svg xmlns="http://www.w3.org/2000/svg" style="width:20px; height: 20px;" viewBox="0 0 20 20" fill="currentColor">
  <path d="M17.414 2.586a2 2 0 00-2.828 0L7 10.172V13h2.828l7.586-7.586a2 2 0 000-2.828z" />
  <path fill-rule="evenodd" d="M2 6a2 2 0 012-2h4a1 1 0 010 2H4v10h10v-4a1 1 0 112 0v4a2 2 0 01-2 2H4a2 2 0 01-2-2V6z" clip-rule="evenodd" />
</svg>
</a>
                                <a href="#" class="text-danger" wire:click.prevent="confirmDelete({{ $row->id }})"> <svg xmlns="http://www.w3.org/2000/svg" style="width:20px; height: 20px;" viewBox="0 0 20 20" fill="currentColor">
                                        <path fill-rule="evenodd" d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z" clip-rule="evenodd" />
                                    </svg></a>
                            </td></tr>@empty  <tr><td>No Records Found</td></tr>   @endforelse

                    </tbody>
                </table>
                <div class="p-2">
                    {{ $rows->links() }}
                </div>
        </div>


    </div>


    {{--    create / edit form --}}

       <div class="modal fade" wire:ignore.self id="showForm" tabindex="-1" role="dialog" aria-labelledby="showFormLabel" aria-hidden="true">
           <div class="modal-dialog" role="document">
               <div class="modal-content">
                   <div class="modal-header">
                       <h5 class="modal-title" id="showFormLabel"> {{ $mode == 'create' ? 'Add New Record' : 'Update Record ' }}</h5>
                       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                           <span aria-hidden="true">&times;</span>
                       </button>
                   </div>
                   <div class="modal-body">
                        <div class='form-group'><label for='shortname'>Shortname</label><input type='text' class='form-control @error('shortname')  is-invalid @enderror' wire:model='shortname'>@error('shortname')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='shortname_en'>Shortname en</label><input type='text' class='form-control @error('shortname_en')  is-invalid @enderror' wire:model='shortname_en'>@error('shortname_en')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='fullname'>Fullname</label><input type='text' class='form-control @error('fullname')  is-invalid @enderror' wire:model='fullname'>@error('fullname')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='fullname_en'>Fullname en</label><input type='text' class='form-control @error('fullname_en')  is-invalid @enderror' wire:model='fullname_en'>@error('fullname_en')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='description'>Description</label><input type='text' class='form-control @error('description')  is-invalid @enderror' wire:model='description'>@error('description')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='description_en'>Description en</label><input type='text' class='form-control @error('description_en')  is-invalid @enderror' wire:model='description_en'>@error('description_en')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='content'>Content</label><input type='text' class='form-control @error('content')  is-invalid @enderror' wire:model='content'>@error('content')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='content_en'>Content en</label><input type='text' class='form-control @error('content_en')  is-invalid @enderror' wire:model='content_en'>@error('content_en')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='created_by'>Created by</label><input type='text' class='form-control @error('created_by')  is-invalid @enderror' wire:model='created_by'>@error('created_by')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='type'>Type</label><input type='text' class='form-control @error('type')  is-invalid @enderror' wire:model='type'>@error('type')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='current_nav'>Current nav</label><input type='text' class='form-control @error('current_nav')  is-invalid @enderror' wire:model='current_nav'>@error('current_nav')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='growth'>Growth</label><input type='text' class='form-control @error('growth')  is-invalid @enderror' wire:model='growth'>@error('growth')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='slug'>Slug</label><input type='text' class='form-control @error('slug')  is-invalid @enderror' wire:model='slug'>@error('slug')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='slug_en'>Slug en</label><input type='text' class='form-control @error('slug_en')  is-invalid @enderror' wire:model='slug_en'>@error('slug_en')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='fun_code'>Fun code</label><input type='text' class='form-control @error('fun_code')  is-invalid @enderror' wire:model='fun_code'>@error('fun_code')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='interest'>Interest</label><input type='text' class='form-control @error('interest')  is-invalid @enderror' wire:model='interest'>@error('interest')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='parent_id'>Parent id</label><input type='text' class='form-control @error('parent_id')  is-invalid @enderror' wire:model='parent_id'>@error('parent_id')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='priority'>Priority</label><input type='text' class='form-control @error('priority')  is-invalid @enderror' wire:model='priority'>@error('priority')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='shortname2'>Shortname2</label><input type='text' class='form-control @error('shortname2')  is-invalid @enderror' wire:model='shortname2'>@error('shortname2')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='shortname2_en'>Shortname2 en</label><input type='text' class='form-control @error('shortname2_en')  is-invalid @enderror' wire:model='shortname2_en'>@error('shortname2_en')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='fullname2'>Fullname2</label><input type='text' class='form-control @error('fullname2')  is-invalid @enderror' wire:model='fullname2'>@error('fullname2')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='fullname2_en'>Fullname2 en</label><input type='text' class='form-control @error('fullname2_en')  is-invalid @enderror' wire:model='fullname2_en'>@error('fullname2_en')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='shortname2'>Shortname2</label><input type='text' class='form-control @error('shortname2')  is-invalid @enderror' wire:model='shortname2'>@error('shortname2')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>
<div class='form-group'><label for='shortname2_en'>Shortname2 en</label><input type='text' class='form-control @error('shortname2_en')  is-invalid @enderror' wire:model='shortname2_en'>@error('shortname2_en')<div class='invalid-feedback'>{{ $message }}</div>@enderror</div>

                   </div>
                   <div class="modal-footer">
                       <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                       <button type="button" @if($mode == 'create') wire:click="store()" @else wire:click="update()" @endif  class="btn btn-primary">
                         {{ $mode == 'create' ? 'Save Record' : 'Update Record' }}
                       </button>
                   </div>
               </div>
           </div>
       </div>
    {{--    /create /edit form--}}


    {{--    delete popup--}}
    <div wire:ignore>
            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Are You Sure?</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                           This Action Can not be Undone.
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" wire:click="destroy()" class="btn btn-danger">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {{--    /delete popup--}}
</div>
