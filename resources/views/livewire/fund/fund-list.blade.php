<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="card-title my-3">Danh sách quỹ đầu tư</h3>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group search-expertise">
                            <div class="search-expertise inline-block">
                                <input type="text" placeholder="{{__('common.button.search')}}" name="search"
                                    class="form-control" wire:model="search" id='input_vn_name' autocomplete="off">
                            </div>

                        </div>
                    </div>
                    {{-- <div wire:ignore class="col-md-2">
                        <select wire:model.lazy="typeFilter" class="form-control">
                            <option value=''>
                                {{__('master/masterManager.menu_name.type')}}
                            </option>
                            @foreach($dataType as $item)
                            <option value='{{$item->type}}'>
                                {{$item->v_key}}
                            </option>1
                            @endforeach
                        </select>
                    </div> --}}
                    <div class="col-md-9">
                        <a href="{{route('admin.funds.create')}}" class="float-right"
                            style="border-radius: 11px; border:none;">
                            <div class="btn-sm btn-primary">
                                <i class="fa fa-plus"></i> Tạo mới
                            </div>
                        </a>
                    </a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="example2" class="table table-bordered table-hover dataTable dtr-inline"
                                        role="grid" aria-describedby="example2_info">
                                        <thead>
                                            <tr role="row">
                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="example2"
                                                    rowspan="1" colspan="1">STT</th>
                                                <th class="sorting sorting_asc" tabindex="0" aria-controls="example2"
                                                    rowspan="1" colspan="1">
                                                    Tên rút gọn
                                                </th>

                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1">
                                                    Tên đầy đủ
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1">
                                                    Nav quỹ 
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1">
                                                    Tăng trưởng
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1">
                                                    Mô tả
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example2" rowspan="1"
                                                    colspan="1">
                                                    Nội dung
                                                </th>
                                                <th colspan="1">
                                                    Hành động
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if($data)
                                            @foreach($data as $row)
                                            <tr class="odd">
                                                <td class="dtr-control sorting_1">{{$row->id}}</td>
                                                <td>{{$row->shortname}}</td>
                                                <td>{{$row->fullname}}</td>
                                                <td>{{numberFormat($row->current_nav)}}</td>
                                                <td>{{$row->growth}}</td>
                                                <td>{{$row->description}}</td>
                                                <td>{!! chuanHoa($row->content)!!}</td>
                                                <td>
                                                    <a href="{{route('admin.funds.edit', ["id"=>$row->id])}}"
                                                        class="float-right"
                                                        style="border-radius: 11px; border:none;">
                                                        <img src="/images/pent2.svg" alt="pent">
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach

                                        </tbody>
                                    </table>
                                    {{$data->links()}}
                                    @endif
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>