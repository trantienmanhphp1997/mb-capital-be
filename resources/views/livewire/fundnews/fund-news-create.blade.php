<div class="card py-2 px-3">
    <form>   
        <div class="form_content">
            <label>Tải tệp lên </label>
            <div class="row">
                <div class="col">
                    <form>
                        <div class="input-group form-group">
                            <input type="file" wire:model.lazy="file" id="file-upload" name="file-upload" accept=".doc, .xlsx, .png,.docx,.jpg,.pdf,.rar,.zip,.xls,.txt">
                            @error('file')
                                @include('layouts.partials.text._error')
                            @enderror
                        </div>
                        <label>{{ $file_name }}</label>
                    </form>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="text-danger mt-1" style="margin-left: 13px" wire:loading wire:target="file">
                            Đang tải tệp lên
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form_title">
            <div class="row">
                <div class="col">
                    <div class="form_content">
                        <label>Quỹ đầu tư </label>
                        <div class="input-group form-group">
                            <select wire:model.lazy="fund_id" class="form-control-sm form-control custom-input-control">
                                <option value=""></option>
                                @foreach ($funds as $fund)
                                    <option value="{{ $fund->id }}">{{ $fund->shortname }}</option>
                                @endforeach
                            </select>
                        </div> 
                    </div>
                </div>
            </div>
        </div>

        <div class="form_title">
            <div class="row">
                <div class="col">
                    <label>Loại <span class="text-danger">(*)</span> </label>
                    <div class="row">
                        <div class="col">
                            <div class="input-group form-group">
                                <select wire:model.lazy="type" class="form-control-sm form-control custom-input-control article-type">
                                    @foreach (\App\Enums\EFundNews::getListData() as $id => $value)
                                        <option value="{{ $id }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @error('type')
                                @include('layouts.partials.text._error')
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="col">
                    <div class="form_content">
                        <label>Ngày công bố </label>
                        <div class="form-group">
                            <input id="public_date" name="public_date" type="date" wire:model.lazy="public_date" class="form-control-sm form-control custom-input-control">
                        </div>  
                    </div>
                </div>
            </div>
        </div>

        <div class="form_title">
            <label>Tiêu đề <span class="text-danger">(*)</span> </label>
            <div class="row">
                <div class="col">
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text text-xs" id="nameViPrepend">
                                vi
                            </span>
                        </div>
                        <input id="title_vi" name="title_vi" wire:model.lazy="title_vi" type='text' class="form-control-sm form-control custom-input-control">
                    </div>
                    @error('title_vi')
                        @include('layouts.partials.text._error')
                    @enderror
                </div>
                <div class="col">
                    <div class="input-group form-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text text-xs" id="nameEnPrepend">
                                en
                            </span>
                        </div>
                        <input id="title_en" name="title_en" wire:model.lazy="title_en" type='text' class="form-control-sm form-control custom-input-control">
                    </div>
                    @error('title_en')
                        @include('layouts.partials.text._error')
                    @enderror
                </div>
            </div>
        </div>

        <!-- Content -->
        <div class="form_content">
            <label>Nội dung (vi) </label>
            <div class="form-group" wire:ignore>
                <textarea id="content_vi" name="content_vi" wire:model.lazy="content_vi" class="textarea form-control" id="somenote"></textarea>
            </div>
        </div>

        <div class="form_content">
            <label>Nội dung (en) </label>
            <div class="form-group" wire:ignore>
                <textarea id="content_en" name="content_en" wire:model.lazy="content_en" class="textarea form-control" id="somenote"></textarea>
            </div>  
        </div>

        <div class="form_content">
            <label>Link bài viết </label>
            <div class="form-group">
                <input placeholder="Nhập link bài viết" class="form-control-sm form-control"  wire:model.lazy="url" type="text" name="url"/>
                @error('url')
                    @include('layouts.partials.text._error')
                @enderror
            </div>  
        </div>
        
        <div class="w-100 clearfix my-2">
            <button wire:click="store" name="submit" type="submit" class="float-right btn ml-1 btn-primary">Lưu lại</button>
            <a href="{{ route('admin.fundnews') }}" type="submit" class="btn btn-secondary float-right mr-1">Hủy</a>          
        </div>
    </form>
</div>


<script>
    $(document).ready(function() {
        $('#content_vi').summernote({
            callbacks: {
                onChange: function(contents, $editable) {
                    @this.set('content_vi', contents);
                }
            }
        });
        $('#content_en').summernote({
            callbacks: {
                onChange: function(contents, $editable) {
                    @this.set('content_en', contents);
                }
            }
        });
    });
    
</script>
