<div class="body-content p-2">
    <div class="card">
        <div class="card-body p-2">
            {{-- Search --}}
            <div class="filter d-flex align-items-center justify-content-between mb-2">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group search-expertise">
                            <div class="search-expertise inline-block">
                                <input type="text" placeholder="Tìm kiếm" name="search"
                                    class="form-control" id='input_vn_name' autocomplete="off" wire:model.debounce.1000ms="searchName">
                            </div>
                        </div>
                    </div>

                    <div wire:ignore class="col-md-3">
                        <select wire:model.debounce.1000ms="searchFund" class="form-control">
                            <option value=''>
                                --Chọn tên quỹ--
                            </option>
                            @foreach($funds as $fund)
                                <option value="{{ $fund->id }}">
                                    {{ $fund->shortname }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    
                    <div wire:ignore class="col-md-3">
                        <select wire:model.debounce.1000ms="searchType" class="form-control">
                            <option value="">
                                --Chọn thể loại--
                            </option>
                            @foreach (\App\Enums\EFundNews::getListData() as $id => $value)
                                <option value="{{ $id }}">{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                
                <div>
                    <div class="input-group">
                        <a href="{{ route('admin.fundnews.create') }}">
                            <div class="btn-sm btn-primary">
                                <i class="fa fa-plus"></i> TẠO MỚI
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            
            <table class="table table-bordered table-hover table-repositive dataTable dtr-inline">
                <thead class="">
                    <tr>
                        <th>STT</th>
                        <th>Tiêu đề</th>
                        {{-- <th>Nội dung</th> --}}
                        <th>Quỹ đầu tư</th>
                        <th>Thể loại</th>
                        <th>Ngày công bố</th>
                        <th style=" width:12px" >File</th>
                        <th>Hành động</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($data as $key => $row)
                        <tr>
                            <td>{{ ($data->currentPage() - 1) * $data->perPage() + $loop->iteration }}</td>
                            <td>{!! boldTextSearch($row->title_vi, $searchName) !!}</td>
                            {{-- <td>{!! boldTextSearch($row->content_vi, $searchTerm) !!}</td> --}}
                            <td>{{ empty($row->fund->shortname) ? '' : $row->fund->shortname }}</td>
                            <td>
                                <span>{{ \App\Enums\EFundNews::typeToName($row->type) }}</span>
                            </td>
                            <td>{{ ReFormatDate($row->public_date,'d-m-Y') }}</td>
                            <td><a href="{{ '/storage/'. $row->file_path }}" target="_blank" title="{{$row->file_name ? $row->file_name : $row->file_path}}">{{ stringLimit($row->file_name ? $row->file_name : $row->file_path) }}</a></td>
                            <td>
                                {{-- <a href="{{route('admin.fundnews.detail', $row['id'])}}">
                                    <button class="btn btn-xs btn-secondary">
                                        <i class="fas fa-eye"></i>
                                    </button>
                                </a> --}}
                                <a href="{{ route('admin.fundnews.edit', ['id' => $row->id]) }}" 
                                        class="btn-sm border-0 bg-transparent">
                                        <img src="/images/pent2.svg" alt="Edit">
                                </a>
                                @include('livewire.common.buttons._delete')
    
                            </td>
                        </tr>
                    @empty
                        <td colspan='12' class='text-center'>Không tìm thấy dữ liệu</td>
                    @endforelse
                </tbody>
            </table>
        </div>
        @if(count($data))
            {{ $data->links() }}
        @endif
    </div>
    @include('livewire.common._modalDelete')
</div>
