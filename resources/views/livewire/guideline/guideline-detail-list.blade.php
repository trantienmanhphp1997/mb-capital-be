<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="card-title my-3">{{ $title }}</h3>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group search-expertise">
                            <div class="search-expertise inline-block">
                                <input type="text" placeholder="Tìm kiếm" name="search"
                                    class="form-control" id='input_vn_name' autocomplete="off" wire:model.debounce.1000ms="searchTerm">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">

                    </div>
                    <div class="col-md-3">
                        <a href="{{route('guideline_detail.create', ['guideid' => $guide_id])}}" class="float-right"
                            style="border-radius: 11px; border:none;">
                            <button class="btn-sm btn-primary">
                                <i class="fa fa-plus"></i> Create
                            </button>
                        </a>
                        <a href="{{route('guideline.index')}}" class="float-right"
                            style="border-radius: 11px; border:none; margin-right: 2%">
                            <button class="btn-sm btn-secondary">
                                Quay lại
                            </button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-hover dataTable dtr-inline" role="grid" aria-describedby="example2_info">
                                        <thead>
                                            <tr>
                                                <th>STT</th>
                                                <th>Tên các bước (vi)</th>
                                                <th>Tên các bước (en)</th>
                                                <th>Nội dung (vi)</th>
                                                <th>Nội dung (en)</th>
                                                <th>Nội dung Mobile(vi)</th>
                                                <th>Nội dung Mobile(en)</th>
                                                <th>Hành động</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($data as $row)
                                            <tr>
                                                <td>{{ ($data->currentPage() - 1) * $data->perPage() + $loop->iteration }}</td>
                                                <td>{!! boldTextSearch($row->name, $searchTerm) !!}</td>
                                                <td>{!! boldTextSearch($row->name_en, $searchTerm) !!}</td>
                                                <td>{!! boldTextSearch($row->content, $searchTerm) !!}</td>
                                                <td>{!! boldTextSearch($row->content_vi, $searchTerm) !!}</td>
                                                <td>{!! boldTextSearch($row->content_mobile, $searchTerm) !!}</td>
                                                <td>{!! boldTextSearch($row->content_mobile_en, $searchTerm) !!}</td>
                                                <td>
                                                    <a href="{{ route('guideline_detail.edit',['id'=>$row->id]) }}"
                                                        style="border-radius: 11px; border:none;">
                                                        <img src="/images/pent2.svg" alt="pent">
                                                    </a>
                                                    @include('livewire.common.buttons._delete')
                                                </td>
                                            </tr>
                                            @empty
                                                <td colspan='12' class='text-center'>Không tìm thấy dữ liệu</td>
                                            @endforelse
                                        </tbody>
                                    </table>
                                    @if(count($data))
                                        {{ $data->links() }}
                                    @endif
                                </div>
                            </div>
                            @include('livewire.common._modalDelete')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>