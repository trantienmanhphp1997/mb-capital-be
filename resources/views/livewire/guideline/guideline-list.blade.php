<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group search-expertise">
                            <div class="search-expertise inline-block">
                                <input type="text" placeholder="Tìm kiếm theo tên" name="search"
                                    class="form-control" id='input_vn_name' autocomplete="off" wire:model.debounce.1000ms="searchName">
                            </div>


                        </div>
                    </div>

                    <div wire:ignore class="col-md-3">
                        <select wire:model.debounce.1000ms="searchFund" class="form-control">
                            <option value=''>
                                --Chọn tên quỹ--
                            </option>
                            @foreach($listFund as $fund)
                                <option value="{{ $fund->id }}">
                                    {{ $fund->shortname }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div wire:ignore class="col-md-3">
                        <select wire:model.debounce.1000ms="searchType" class="form-control">
                            <option value=''>--Chọn loại--</option>
                            <option value='1'>Hướng dẫn app</option>
                            <option value='2'>Hướng dẫn web</option>

                        </select>
                    </div>

                    <div class="col-md-3">
                        <button class="float-right btn-sm btn-primary" 
                        style="border:none;" data-toggle="modal" 
                        data-target="#create_update_modal" wire:click="resetform()">
                                <i class="fa fa-plus"></i> Tạo mới   
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-md-12">
                                    <table  class="table table-bordered table-hover dataTable dtr-inline"
                                        role="grid" aria-describedby="example2_info">
                                        <thead>
                                            <tr role="row">
                                                <th>STT</th>
                                                <th>Tên hướng dẫn (vi)</th>
                                                <th>Tên hướng dẫn (en)</th>
                                                <th>Quỹ đầu tư</th>
                                                <th>Phân loại</th>
                                                <th>Trạng thái</th>
                                                <th>Hành động</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($data as $row)
                                            <tr class="odd">
                                                <td class="dtr-control sorting_1">{{ ($data->currentPage() - 1) * $data->perPage() + $loop->iteration }}</td>
                                                <td><a href="{{route('guideline.detail', ['id' => $row->id])}}">{!! boldTextSearch($row->name, $searchName) !!}<a></td>
                                                <td>{!! boldTextSearch($row->name_en, $searchName) !!}</td>
                                                <td>{{ empty($row->fund->shortname) ? '' : $row->fund->shortname }}</td>
                                                @if($row->type==1)
                                                <td>{{$row->type='Hướng dẫn app'}}</td>
                                                @else
                                                <td>{{$row->type='Hướng dẫn web'}}</td>
                                                @endif
                                                @if($row->status==1)
                                                <td>{{$row->status='Đang hoạt động'}}</td>
                                                @else
                                                <td>{{$row->status='Chưa kích hoạt'}}</td>
                                                @endif
                                                <td class="d-flex justify-content-between">
                                                    <a href="{{route('guideline.detail', ['id' => $row->id])}}">
                                                        <button class="btn btn-xs btn-cancel">
                                                            <i class="fas fa-eye"></i>
                                                        </button>
                                                    </a>
                                                    <a href="#" data-toggle="modal" data-target="#edit-modal" wire:click="edit({{ $row->id }})">
                                                        <img src="/images/pent2.svg" alt="pent">
                                                    </a>
                                                    @include('livewire.common.buttons._delete')
                                                </td>
                                            </tr>
                                            @empty
                                                <td colspan='12' class='text-center'>Không tìm thấy dữ liệu</td>
                                            @endforelse
                                        </tbody>
                                    </table>
                                    @if(count($data))
                                        {{ $data->links() }}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('livewire.common._modalDelete')

    <div wire:ignore.self class="modal fade" id="create_update_modal" role="dialog" >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Thêm mới hướng dẫn
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>
                                    Tên hướng dẫn (VI) <span class="text-danger">*</span>
                                </label>
                                <input type="text" name="question" class="form-control" wire:model.defer="name"
                                       placeholder="Tên hướng dẫn(VI)">
                                @error('name')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>
                                    Tên hướng dẫn (EN) <span class="text-danger">*</span>
                                </label>
                                <input type="text" name="question" class="form-control" wire:model.defer="name_en"
                                       placeholder="Tên hướng dẫn(EN)">
                                @error('name_en')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Quỹ đầu tư<span class="text-danger">&nbsp*</span></label>
                                <select  wire:model.defer="fund_id" class="form-control">
                                    <option value={{null}}>Chọn quỹ đầu tư</option>
                                    @foreach($listFund as $item)
                                        <option value="{{$item->id}}">{{$item->shortname}}</option>
                                    @endforeach
                                </select>
                                @error('fund_id')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Loại hướng dẫn<span class="text-danger">&nbsp*</span></label>
                                <select class="form-control" name="type" id="type" wire:model.defer="type">
                                    <option value={{null}}>Chọn loại hướng dẫn</option>
                                    <option value="1">
                                        Hướng dẫn app
                                    </option>
                                    <option value="2">
                                        Hướng dẫn web
                                    </option>
                                </select>
                                @error('type') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                            <div class="form-group">
                                <label>
                                    Image {{--<span class="text-danger">*</span>--}}
                                </label>
                                <input wire:model.defer="image" id="image" name="image" type="file">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="text-danger mt-1" style="margin-left: 13px" wire:loading wire:target="image">
                                            Đang tải tệp lên
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>
                                    Video {{--<span class="text-danger">*</span>--}}
                                </label>
                                <input wire:model.defer="video" id="video" name="video" type="file">
                                @error('video')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="text-danger mt-1" style="margin-left: 13px" wire:loading wire:target="video">
                                            Đang tải tệp lên
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>
                                    URL video <span class="text-danger">*</span>
                                </label>
                                <input type="text" name="url" class="form-control" wire:model.defer="url_video"
                                       placeholder="url video">
                                @error('url_video')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="close-modal-create" wire:click.prevent="resetform"
                            class="btn btn-secondary close-btn" data-dismiss="modal">Đóng
                    </button>
                    <button type="button" wire:click = "store" class="btn btn-primary close-modal">Lưu
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="edit-modal" role="dialog" >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Chỉnh sửa hướng dẫn
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>
                                    Tên hướng dẫn (VI) <span class="text-danger">*</span>
                                </label>
                                <input type="text" name="question" class="form-control" wire:model.defer="name"
                                       placeholder="Tên hướng dẫn(VI)">
                                @error('name')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>
                                    Tên hướng dẫn (EN) <span class="text-danger">*</span>
                                </label>
                                <input type="text" name="question" class="form-control" wire:model.defer="name_en"
                                       placeholder="Tên hướng dẫn(EN)">
                                @error('name_en')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Quỹ đầu tư<span class="text-danger">&nbsp*</span></label>
                                <select  wire:model.defer="fund_id" class="form-control">
                                    <option value={{null}}>Chọn quỹ đầu tư</option>
                                    @foreach($listFund as $item)
                                        <option value="{{$item->id}}">{{$item->shortname}}</option>
                                    @endforeach
                                </select>
                                @error('fund_id')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Loại hướng dẫn<span class="text-danger">&nbsp*</span></label>
                                <select class="form-control" name="type" id="type" wire:model.defer="type">
                                    <option value={{null}}>Chọn loại hướng dẫn</option>
                                    <option value="1">
                                        Hướng dẫn app
                                    </option>
                                    <option value="2">
                                        Hướng dẫn web
                                    </option>
                                </select>
                                @error('type') <span class="text-danger">{{ $message }}</span> @enderror
                            </div>
                            <div class="form-group">
                                <label>
                                    Image {{--<span class="text-danger">*</span>--}}
                                </label>
                                <input wire:model.defer="image" id="image" name="image" type="file" wire:click="$emit('remove_image')">
                                {{-- @error('image')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror --}}
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="text-danger mt-1" style="margin-left: 13px" wire:loading wire:target="image">
                                            Đang tải tệp lên
                                        </div>
                                    </div>
                                </div>
                                <div class="preview-image">
                                    @if($image)
                                        <div class="form_content ml-2 form-group preview-image" data="{{ './storage/'. $image }}">
                                            <div class="py-2 px-3 bg-light d-flex align-items-center" id="preview-image">
                                                <a href="{{ './storage/'. $image }}" target="_blank">
                                                    <i class="fas fa-file mr-2"></i> {{$image}}
                                                </a>
                                                <div class="border-left ml-2 px-2 btn btn-md" id="removeImage" wire:click="$emit('remove_image')">
                                                    <i class="fa fa-times mr-1"></i> Xóa
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label>
                                    Video {{--<span class="text-danger">*</span>--}}
                                </label>
                                <input wire:model.defer="video" id="video" name="video" type="file" wire:click="$emit('remove_video')">
                                {{-- @error('video')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror --}}
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="text-danger mt-1" style="margin-left: 13px" wire:loading wire:target="video">
                                            Đang tải tệp lên
                                        </div>
                                    </div>
                                </div>
                                <div class="preview-video">
                                    @if($video)
                                        <div class="form_content ml-2 form-group preview-video" data="{{ './storage/'. $video }}">
                                            <div class="py-2 px-3 bg-light d-flex align-items-center" id="preview-video">
                                                <a href="{{ './storage/'. $video }}" target="_blank">
                                                    <i class="fas fa-file mr-2"></i> {{$video}}
                                                </a>
                                                <div class="border-left ml-2 px-2 btn btn-md" id="removeVideo" wire:click="$emit('remove_video')">
                                                    <i class="fa fa-times mr-1"></i> Xóa
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label>
                                    URL video <span class="text-danger">*</span>
                                </label>
                                <input type="text" name="url" class="form-control" wire:model.defer="url_video"
                                       placeholder="url video">
                                @error('url_video')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="close-modal-edit" wire:click.prevent="resetform"
                            class="btn btn-secondary close-btn" data-dismiss="modal">Đóng
                    </button>
                    <button type="button" wire:click = "update" class="btn btn-primary close-modal">Lưu
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script>
        window.livewire.on('close-modal-create', () => {
            $('#close-modal-create').click();
        });
        window.livewire.on('close-modal-edit', () => {
            $('#close-modal-edit').click();
        });
        // window.livewire.on('resetContent', () => {
        //     var image = document.getElementById('image1');
        //     var video = document.getElementById('video1');

        //     image.value = null;
        //     video.value = null;
        // });
    </script>
    <script>
        $(function(){
            $(document).on('click', '#removeImage', function(){
                var x = $(this),
                    parent = x.parents(".preview-image"),
                    path = parent.attr('data'),
            }); 
            $(document).on('click', '#removeVideo', function(){
                var y = $(this),
                    parent = y.parents(".preview-video"),
                    path = parent.attr('data'),
            });

            $("#image").change(function(e){
                var f = this.files;
                if(f.length){
                    f = f[0];
                    $("#removeImage").trigger("click");
                    $(".preview-image").empty().html(
                        '<div class="form_content ml-2 form-group preview-image">'
                            +'<div class="py-2 px-3 bg-light d-flex align-items-center" id="preview-image">'
                                +'<div>'
                                    +'<i class="fas fa-file mr-2"></i>' + f.name
                                +'</div>'
                                +'<div class="border-left ml-2 px-2 btn btn-md" id="removeImage">'
                                    +'<i class="fa fa-times mr-1"></i> Xóa'
                                +'</div>'
                            +'</div>'
                        +'</div>'
                    );
                }
            });
            $("#video").change(function(e){
                var g = this.files;
                if(g.length){
                    g = g[0];
                    $("#removeVideo").trigger("click");
                    $(".preview-video").empty().html(
                        '<div class="form_content ml-2 form-group preview-video">'
                            +'<div class="py-2 px-3 bg-light d-flex align-items-center" id="preview-video">'
                                +'<div>'
                                    +'<i class="fas fa-file mr-2"></i>' + g.name
                                +'</div>'
                                +'<div class="border-left ml-2 px-2 btn btn-md" id="removeVideo">'
                                    +'<i class="fa fa-times mr-1"></i> Xóa'
                                +'</div>'
                            +'</div>'
                        +'</div>'
                    );
                }
            });

        });
    </script>
</section>
