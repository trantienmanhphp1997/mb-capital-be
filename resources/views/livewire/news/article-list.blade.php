<div class="card">
    <div class="card-body p-2">
        <div class="filter d-flex align-items-center justify-content-between mb-2">
            <div class="">
                    <div class="input-group">
                        <input type="text" class="form-control-sm form-control custom-input-control" wire:model.debounce.1000ms="searchName" id="searchName" placeholder="Search" aria-describedby="keywordAppend" name="searchName">
                        <div class="input-group-append">
                            <span class="input-group-text" id="keywordAppend">
                                <div class="text-xs">
                                    <i class="fa fa-search"></i>
                                </div>
                            </span>
                        </div>
                    </div>
            </div>
            <a href="{{route('admin.new.create')}}">
                <div class="btn-sm btn-primary">
                    <i class="fa fa-plus"></i> Tạo mới
                </div>
            </a>
        </div>
        
        <table class="table table-striped">
            <thead class="">
                <tr>
                    <th>STT</th>
                    <th>Tiêu đề</th>
                    <th>Mô tả</th>
                    <th>Ảnh đại diện</th>
                    <th>Tác giả</th>
                    <th>Action</th>
                </tr>
            </thead>
            <div wire:loading class="loader"></div>
            <tbody>
                @if(isset($newData) && count($newData) > 0)
                    @foreach($newData as $key => $row)
                        <tr>
                            <td class="align-middle">{{$key+1}}</td>
                            <td class="align-middle">{{strlen($row['name_vi']) > 80 ? mb_substr($row['name_vi'], 0, 80, 'UTF-8') . '...' : $row['name_vi']}}</td>
                            <td class="align-middle">
                                {{ strlen($row['intro_vi']) > 80 ? mb_substr($row['intro_vi'], 0, 80, 'UTF-8') . '...' : $row['intro_vi']}}
                            </td>
                            <td class="align-middle">
                                <img src="{{($_SERVER['HTTP_HOST']=='mbcap-cms.evotek.vn'&&checkUrlStorage($row['image']))?('https://mbcapital.com.vn/'.$row['image']):$row['image']}}" width="60px"/>
                            </td>
                            <td class="align-middle">{{$row['author']}}</td>
                            <td class="align-middle">
                                {{--<a href="{{route('admin.new.detail', $row['id'])}}">
                                    <button class="btn btn-xs btn-secondary">
                                        <i class="fas fa-eye"></i>
                                    </button>
                                </a>--}}
                                <a href="{{ route('admin.new.edit', $row['id']) }}" 
                                        class="btn-sm border-0 bg-transparent">
                                        <img src="/images/pent2.svg" alt="Edit">
                                </a>
                                @include('livewire.common.buttons._delete')
                                {{--<a href="{{ route('admin.new.edit', $row['id']) }}">
                                    <button class="btn btn-xs btn-warning">
                                        <i class="fas fa-pen"></i>
                                    </button>
                                </a>--}}
                                {{--<a href="{{ route('admin.new.delete', $row['id']) }}">
                                    <button class="btn btn-xs btn-danger">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </a>--}}
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        @if(!isset($newData) || !count($newData))
            <div class="pb-2 pt-3 text-center">Không tìm thấy dữ liệu</div>
        @endif
    </div>
    @if(count($newData))
        {{ $newData->links() }}
    @endif
    @include('livewire.common._modalDelete')
</div>