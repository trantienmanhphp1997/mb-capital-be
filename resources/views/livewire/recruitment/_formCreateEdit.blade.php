<div>
    <div wire:ignore.self class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{$canEdit?"Chỉnh sửa tuyển dụng nhân sự":"Thêm mới tuyển dụng nhân sự"}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" wire:click="resetValidate()">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Tiêu đề(VI) (<span style='color:red'>*</span>)</label>
                        <textarea name="name" class="form-control" placeholder="Tiêu đề" wire:model.defer='title'> </textarea>
                        @error('title')
                            @include('layouts.partials.text._error')
                        @enderror
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Tiêu đề(EN)</label>
                        <textarea name="name" class="form-control" placeholder="Tiêu đề" wire:model.defer='title_en'> </textarea>
                        @error('title_en')
                            @include('layouts.partials.text._error')
                        @enderror
                    </div>
                </div>

                <div class="modal-body">
                    <div class="form-group" wire:ignore>
                        <label for="short_content">Nội dung ngắn(VI)</label>
                        <textarea name="short_content" id="short_content" class="form-control textarea"> </textarea>
                        @error('short_content')
                            @include('layouts.partials.text._error')
                        @enderror
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group" wire:ignore>
                        <label for="short_content">Nội dung ngắn(EN)</label>
                        <textarea name="short_content" id="short_content_en" class="form-control textarea"> </textarea>
                        @error('short_content_en')
                            @include('layouts.partials.text._error')
                        @enderror
                    </div>
                </div>

                <div class="modal-body">
                    <div wire:ignore class="form-group">
                        <label for="name">Nội dung(VI) (<span style='color:red'>*</span>)</label>
                        <textarea id="content" class="form-control textarea"> </textarea>
                        @error('content')
                            @include('layouts.partials.text._error')
                        @enderror
                    </div>
                </div>
                <div class="modal-body">
                    <div wire:ignore class="form-group">
                        <label for="name">Nội dung(EN)</label>
                        <textarea id="content_en" class="form-control textarea"> </textarea>
                        @error('content_en')
                            @include('layouts.partials.text._error')
                        @enderror
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Đường dẫn (<span style='color:red'>*</span>)</label>
                        <textarea name="name" class="form-control" placeholder="Đường dẫn" wire:model.defer='url' rows='10'> </textarea>
                        @error('url')
                            @include('layouts.partials.text._error')
                        @enderror
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Ngày đăng tuyển</label>
                        <input type="date"  class="form-control" wire:model.defer='date_submit'>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">File tải lên</label>
                        <input type="file" class="form-control" wire:model.lazy='file'>
                    </div>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Cấp bậc (VI)</label>
                        <textarea class="form-control" placeholder="Cấp bậc (VI)" wire:model.defer='position'> </textarea>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Cấp bậc (EN)</label>
                        <textarea class="form-control" placeholder="Cấp bậc (EN)" wire:model.defer='position_en'> </textarea>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Ngành nghề (VI)</label>
                        <textarea class="form-control" placeholder="Ngành nghề (VI)" wire:model.defer='job'> </textarea>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Ngành nghề (EN)</label>
                        <textarea class="form-control" placeholder="Ngành nghề (EN)" wire:model.defer='job_en'> </textarea>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Kỹ năng (VI)</label>
                        <textarea class="form-control" placeholder="Kỹ năng (VI)" wire:model.defer='skill'> </textarea>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Kỹ năng (EN)</label>
                        <textarea class="form-control" placeholder="Kỹ năng (EN)" wire:model.defer='skill_en'> </textarea>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Ngôn ngữ trình bày hồ sơ (VI)</label>
                        <textarea class="form-control" placeholder="Ngôn ngữ trình bày hồ sơ (VI)" wire:model.defer='language'> </textarea>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">Ngôn ngữ trình bày hồ sơ  (EN)</label>
                        <textarea class="form-control" placeholder="Ngôn ngữ trình bày hồ sơ (EN)" wire:model.defer='language_en'> </textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal" wire:click="resetValidate()">{{__('common.button.cancel')}}</button>
                    <button id='btn-save' type="button" class="btn btn-primary">{{__('common.button.save')}}</button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
     $("document").ready(() => {
        window.livewire.on('setContent', () => {
            $('#content').summernote('code', '');
            $('#content_en').summernote('code', '');
            $('#short_content').summernote('code', '');
            $('#short_content_en').summernote('code', '');
        });
        window.livewire.on('setContentEdit', (content, content_en, short_content, short_content_en) => {
            $('#content').summernote('code', content);
            $('#content_en').summernote('code', content_en);
            $('#short_content').summernote('code', short_content);
            $('#short_content_en').summernote('code', short_content_en);
        });

        $('#btn-save').click(function() {
            window.livewire.emit('set-content', $('#content').summernote('code'), $('#content_en').summernote('code'), $('#short_content').summernote('code'), $('#short_content_en').summernote('code'));
        })
    });
</script>
