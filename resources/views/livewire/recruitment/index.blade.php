<div class="body-content p-2">
    <div class="p-2 pb-3 d-flex align-items-center justify-content-between">
        <div class="">
            <h4 class="m-0">
                Danh sách tuyển dụng
            </h4>
        </div>
        <div class="paginate">
            <div class="d-flex">
                <div class="">
                    <a href="{{ route('home') }}"><i class="fa fa-home"></i> Trang chủ</a>
                </div>
                <span class="px-2">/</span>
                <div class="">
                    <div class="disable">Danh sách tuyển dụng</div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="card">
        <div class="card-body p-2">
            <div class="filter d-flex align-items-center justify-content-between mb-2">
                <div>
                    <div class="input-group">
                        <input type="text" value="" wire:model.debounce.1000ms="searchTerm" 
                            class="form-control-sm form-control custom-input-control" autocomplete="off"
                            id="keyword" placeholder="Search" aria-describedby="keywordAppend" name="keyword">
                        <div class="input-group-append">
                            <span class="input-group-text" id="keywordAppend">
                                <div class="text-xs">
                                    <i class="fa fa-search"></i>
                                </div>
                            </span>
                        </div>
                        
                    </div>
                </div>

                <a href="#" data-toggle="modal" data-target="#exampleModal" wire:click='create'>
                    <div class="btn-sm btn-primary">
                        <i class="fa fa-plus"></i> Tạo mới
                    </div>
                </a>
            </div>
            
            <table class="table table-striped">
                <thead class="">
                    <tr>
                        <th style="width: 15px;">STT</th>
                        <th>Tiêu đề(VI)</th>
                        <th>Tiêu đề(EN)</th>
                        <th>Nội dung(VI)</th>
                        <th>Nội dung(EN)</th>
                        <th>Đường dẫn</th>
                        <th>Ngày tạo</th>
                        <th>Hành động</th>
                    </tr>
                </thead>
                <div wire:loading class="loader"></div>
                <tbody>
                    @forelse($data as $key => $row)
                        <tr>
                            <td>{{ ($data->currentPage() - 1) * $data->perPage() + $loop->iteration }}</td>
                            <td>{!! boldTextSearchV2($row->title, $searchTerm) !!}</td>
                            <td>{!! boldTextSearchV2($row->title_en, $searchTerm) !!}</td>
                            <td>{!! boldTextSearchV2($row->content, $searchTerm) !!}</td>
                            <td>{!! boldTextSearchV2($row->content_en, $searchTerm) !!}</td>
                            <td><a href='{{$row->url}}' target='_blank'>{{$row->url}}</a></td>
                            <td>{{ reFormatDate($row->created_at,'d-m-Y H:i:s') }}</td>
                            <td>
                                <button type="button" data-toggle="modal" data-target="#exampleModal"  class="btn par6" title="update" wire:click="edit({{$row}})">
                                    <img src="/images/pent2.svg" alt="pent">
                                </button>
                                @include('livewire.common.buttons._delete')
                            </td>
                        </tr>
                    @empty
                        <td colspan='8' class='text-center'>Không tìm thấy dữ liệu</td>
                    @endforelse
                </tbody>
            </table>
        </div>
        @if(count($data))
            {{ $data->links() }}
        @endif
    </div>
    @include('livewire.common._modalDelete')
    @include('livewire.recruitment._formCreateEdit')
</div>

<script>
    $("document").ready(() => {
        window.livewire.on('closeModalCreateEdit', () => {
            $('#exampleModal').modal('hide');
        });
    });
</script>
