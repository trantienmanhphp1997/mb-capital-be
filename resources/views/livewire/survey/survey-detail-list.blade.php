<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h3 class="card-title my-3">Câu hỏi: {{ $title }}</h3>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group search-expertise">
                            <div class="search-expertise inline-block">
                                <input type="text" placeholder="Tìm kiếm" name="search"
                                    class="form-control" id='input_vn_name' autocomplete="off" wire:model.debounce.1000ms="searchTerm">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">

                    </div>
                    <div class="col-md-3">
                        <button class="float-right btn-sm btn-primary"
                        style="border:none;" data-toggle="modal"
                        data-target="#create_update_modal" wire:click="resetform()">
                                <i class="fa fa-plus"></i> Tạo mới
                        </button>
                        <a href="{{route('survey.index')}}" class="float-right"
                            style="border-radius: 11px; border:none; margin-right: 2%">
                        <button class="btn-sm btn-secondary">
                                Quay lại
                        </button>
                    </a>
                    </div>

                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="table table-bordered table-hover dataTable dtr-inline" role="grid" aria-describedby="example2_info">
                                        <thead>
                                            <tr>
                                                <th>STT</th>
                                                <th>Tên câu trả lời (vi)</th>
                                                <th>Tên câu trả lời (en)</th>
                                                <th>Điểm</th>
                                                <th>Hành động</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($data as $row)
                                            <tr>
                                                <td>{{ ($data->currentPage() - 1) * $data->perPage() + $loop->iteration }}</td>
                                                <td>{!! boldTextSearch($row->name, $searchTerm) !!}</td>
                                                <td>{!! boldTextSearch($row->name_en, $searchTerm) !!}</td>
                                                <td>{!! boldTextSearch($row->point, $searchTerm) !!}</td>
                                                <td>
                                                    <a href="#" data-toggle="modal" data-target="#edit-modal" wire:click="edit({{ $row->id }})">
                                                        <img src="/images/pent2.svg" alt="pent">
                                                    </a>
                                                    @include('livewire.common.buttons._delete')
                                                </td>
                                            </tr>
                                            @empty
                                                <td colspan='12' class='text-center'>Không tìm thấy dữ liệu</td>
                                            @endforelse
                                        </tbody>
                                    </table>
                                    @if(count($data))
                                        {{ $data->links() }}
                                    @endif
                                </div>
                            </div>
                            @include('livewire.common._modalDelete')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div wire:ignore.self class="modal fade" id="create_update_modal" role="dialog" >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Thêm mới câu hỏi
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>
                                    Tên câu hỏi (VI) <span class="text-danger">*</span>
                                </label>
                                <input type="text" name="question" class="form-control" wire:model.defer="name"
                                       placeholder="Tên câu hỏi (VI)">
                                @error('name')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>
                                    Tên câu hỏi (EN) <span class="text-danger">*</span>
                                </label>
                                <input type="text" name="question" class="form-control" wire:model.defer="name_en"
                                       placeholder="Tên câu hỏi (EN)">
                                @error('name_en')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>
                                    Điểm
                                </label>
                                <input type="number" name="question" class="form-control" wire:model.defer="point"
                                       placeholder="Điểm">
                                @error('point')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="close-modal-create" wire:click.prevent="resetform"
                            class="btn btn-secondary close-btn" data-dismiss="modal">Đóng
                    </button>
                    <button type="button" wire:click = "store" class="btn btn-primary close-modal">Lưu
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="edit-modal" role="dialog" >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Chỉnh sửa hướng dẫn
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>
                                    Tên câu hỏi (VI) <span class="text-danger">*</span>
                                </label>
                                <input type="text" name="question" class="form-control" wire:model.defer="name"
                                       placeholder="Tên câu hỏi (VI)">
                                @error('name')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>
                                    Tên câu hỏi (EN) <span class="text-danger">*</span>
                                </label>
                                <input type="text" name="question" class="form-control" wire:model.defer="name_en"
                                       placeholder="Tên câu hỏi (EN)">
                                @error('name_en')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>
                                    Điểm
                                </label>
                                <input type="number" name="question" class="form-control" wire:model.defer="point"
                                       placeholder="Điểm">
                                @error('point')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="close-modal-edit" wire:click.prevent="resetform"
                            class="btn btn-secondary close-btn" data-dismiss="modal">Đóng
                    </button>
                    <button type="button" wire:click = "update" class="btn btn-primary close-modal">Lưu
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script>
        window.livewire.on('close-modal-create', () => {
            $('#close-modal-create').click();
        });
        window.livewire.on('close-modal-edit', () => {
            $('#close-modal-edit').click();
        });
    </script>
</section>
