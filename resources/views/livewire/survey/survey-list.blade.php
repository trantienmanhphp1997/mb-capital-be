<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group search-expertise">
                            <div class="search-expertise inline-block">
                                <input type="text" placeholder="Tìm kiếm theo tên" name="search"
                                    class="form-control" id='input_vn_name' autocomplete="off" wire:model.debounce.1000ms="searchName">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-3">
                        <button class="float-right btn-sm btn-primary" 
                        style="border:none;" data-toggle="modal" 
                        data-target="#create_update_modal" wire:click="resetform()">
                                <i class="fa fa-plus"></i> Tạo mới   
                        </button>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                            <div class="row">
                                <div class="col-md-12">
                                    <table  class="table table-bordered table-hover dataTable dtr-inline"
                                        role="grid" aria-describedby="example2_info">
                                        <thead>
                                            <tr role="row">
                                                <th>STT</th>
                                                <th>Tên câu hỏi (vi)</th>
                                                <th>Tên câu trả lời (en)</th>
                                                <th>Sô thứ tự</th>
                                                <th>Cho chọn nhiều câu trả lời</th>
                                                <th style="width: 15%">Hành động</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse($data as $row)
                                            <tr class="odd">
                                                <td class="dtr-control sorting_1">{{ ($data->currentPage() - 1) * $data->perPage() + $loop->iteration }}</td>
                                                <td><a href="{{route('survey.detail', ['id' => $row->id])}}">{!! boldTextSearch($row->name, $searchName) !!}<a></td>
                                                <td>{!! boldTextSearch($row->name_en, $searchName) !!}</td>
                                                <td>{{$row->order_number}}</td>
                                                <td>{{$row->multi_select?'Có':'Không'}}</td>
                                                <td class="d-flex justify-content-between">
                                                    <a href="{{route('survey.detail', ['id' => $row->id])}}">
                                                        <button class="btn btn-xs btn-cancel">
                                                            <i class="fas fa-eye"></i>
                                                        </button>
                                                    </a>
                                                    <a href="#" data-toggle="modal" data-target="#edit-modal" wire:click="edit({{ $row->id }})">
                                                        <img src="/images/pent2.svg" alt="pent">
                                                    </a>
                                                    @include('livewire.common.buttons._delete')
                                                </td>
                                            </tr>
                                            @empty
                                                <td colspan='12' class='text-center'>Không tìm thấy dữ liệu</td>
                                            @endforelse
                                        </tbody>
                                    </table>
                                    @if(count($data))
                                        {{ $data->links() }}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('livewire.common._modalDelete')

    <div wire:ignore.self class="modal fade" id="create_update_modal" role="dialog" >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Thêm mới câu hỏi
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>
                                    Tên câu hỏi (VI) <span class="text-danger">*</span>
                                </label>
                                <input type="text" name="question" class="form-control" wire:model.defer="name"
                                       placeholder="Tên câu hỏi (VI)">
                                @error('name')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>
                                    Tên câu hỏi (EN) <span class="text-danger">*</span>
                                </label>
                                <input type="text" name="question" class="form-control" wire:model.defer="name_en"
                                       placeholder="Tên câu hỏi (EN)">
                                @error('name_en')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>
                                    Số thứ tự câu hỏi
                                </label>
                                <input type="number" name="question" class="form-control" wire:model.defer="order_number"
                                       placeholder="Số thứ tự câu hỏi">
                                @error('order_number')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="form-group ">
                                <label class="col-md-4">
                                    Cho phép chọn nhiều câu trả lời
                                </label>
                                <input type="checkbox" wire:model.defer="multi_select">
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="close-modal-create" wire:click.prevent="resetform"
                            class="btn btn-secondary close-btn" data-dismiss="modal">Đóng
                    </button>
                    <button type="button" wire:click = "store" class="btn btn-primary close-modal">Lưu
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div wire:ignore.self class="modal fade" id="edit-modal" role="dialog" >
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Chỉnh sửa hướng dẫn
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true close-btn">×</span>
                    </button>
                </div>
                <div class="modal-body container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label>
                                    Tên câu hỏi (VI) <span class="text-danger">*</span>
                                </label>
                                <input type="text" name="question" class="form-control" wire:model.defer="name"
                                       placeholder="Tên câu hỏi (VI)">
                                @error('name')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>
                                    Tên câu hỏi (EN) <span class="text-danger">*</span>
                                </label>
                                <input type="text" name="question" class="form-control" wire:model.defer="name_en"
                                       placeholder="Tên câu hỏi (EN)">
                                @error('name_en')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>
                                    Số thứ tự câu hỏi
                                </label>
                                <input type="number" name="question" class="form-control" wire:model.defer="order_number"
                                       placeholder="Số thứ tự câu hỏi">
                                @error('order_number')
                                <div class="text-danger mt-1">{{$message}}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label class="col-md-4">
                                    Cho phép chọn nhiều câu trả lời
                                </label>
                                <input type="checkbox" wire:model.defer="multi_select">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="close-modal-edit" wire:click.prevent="resetform"
                            class="btn btn-secondary close-btn" data-dismiss="modal">Đóng
                    </button>
                    <button type="button" wire:click = "update" class="btn btn-primary close-modal">Lưu
                    </button>
                </div>
            </div>
        </div>
    </div>

    <script>
        window.livewire.on('close-modal-create', () => {
            $('#close-modal-create').click();
        });
        window.livewire.on('close-modal-edit', () => {
            $('#close-modal-edit').click();
        });
    </script>

</section>
