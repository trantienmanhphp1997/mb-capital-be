<div>
    <section class="content">
        <div class="container-fluid">
            <div class="row m-3">
                <div class="form-group col-4 row">
                    <label class="col-6">Chọn kết quả muốn xem</label>
                    <select class="form-control col-6" id="input_question" wire:model.defer="searchTerm">
                        @foreach($selectShow as $key => $order_number)
                            <option value="{{$key}}" {{$order_number==1 ? 'selected' : ''}}>
                                Câu  {{$order_number}}</option>
                        @endforeach
                                <option value="rate_level_risk">
                                    Mức độ rủi ro</option>
                    </select>
                </div>
                <div class="form-group col-5 row">
                    <label for="from_date" class="col-2 pl-4">Từ này</label>
                    <input type="date" class="form-control col-4" wire:model.lazy="fromDate" max="{{date('Y-m-d')}}">

                    <label for="to_date" class="col-2 pl-2">Đến ngày</label>
                    <input type="date" class="form-control col-4" wire:model.lazy="toDate" min="{{$fromDate}}" max="{{date('Y-m-d')}}"
                           >
                </div>
                <div class="form-group col-3  row">
                    <button type="button" class="btn btn-default  ml-5 mr-3" wire:click="resetSearch()"><i
                            class="fa fa-undo"></i> Làm mới
                    </button>
                    <button type="button" class="btn btn-primary" wire:click="search()"> Xem kết quả</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body p-2">
                            <div class="row m-3">
                                <div class="col-8 mt-5">
                                    <canvas id="pie-1"></canvas>
                                </div>
                                <div class="col-4 mt-5">
                                    <p class="">Tổng số người trả lời: {{$all}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@section('js')
    <script type="text/javascript">
        var pie1 = document.getElementById('pie-1');
        var labelname = 'My First Dataset';
        var type = 'doughnut';
        var dataHealth= <?php echo json_encode(chuanHoa($dataPie)); ?>;
        let widthScreen = $(window).width();

        var datas = dataHealth.map($item => $item.value.value);
        var colors = dataHealth.map($item => $item.value.color);
        let position = widthScreen >= 768 ? 'right' : 'bottom'
        var labels = dataHealth.map($item => $item.label ? ($item.label + ': ' + $item.value.value + '%') : ($item.value.value + '%'));
        var myChart1 = initChart(pie1, type, labels, labelname, datas, colors, 4, position, 'pie-1');
    </script>
    <script>
        window.addEventListener('update_scripts', event=> {
            dataHealth= event.detail.data;
            datas = dataHealth.map($item => $item.value.value);
            labels = dataHealth.map($item => $item.label ? ($item.label + ': ' + $item.value.value + '%') : ($item.value.value + '%'));
            if(myChart1!= undefined) myChart1.destroy();
            myChart1 = initChart(pie1, type, labels, labelname, datas, colors, 4, position, 'pie-1');

        })
    </script>
@endsection
