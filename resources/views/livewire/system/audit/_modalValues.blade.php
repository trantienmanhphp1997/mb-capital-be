<div wire:ignore class="modal fade" id="{{'show'. $row['id']}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h5>GIÁ TRỊ CŨ</h5>
                <div class="text-left">
                    @if(!empty($row['old_values']) && count($row['old_values']) > 0)
                        @foreach($row['old_values'] as $index => $item)
                            <p class="mb-1 content-audit-data" title='{{$index.": ".json_encode(chuanHoa($item))}}'><span class="font-weight-bold">{{$index}}</span>: {!! boldTextSearch(mb_convert_encoding($item,'UTF-8'), $searchTerm) !!}</p>
                        @endforeach
                    @endif
                </div>
            </div>
            <div class="modal-header">
            </div>
            <div class="modal-body">
                <h5>GIÁ TRỊ MỚI</h5>
                <div class="text-left">
                    @if(!empty($row['new_values']) && count($row['new_values']) > 0)
                        @foreach($row['new_values'] as $index => $item)
                            <p class="mb-1 content-audit-data" title='{{$index.": ".json_encode(chuanHoa($item))}}'><span class="font-weight-bold">{{$index}}</span>: {!! boldTextSearch(json_encode(chuanHoa($item)), $searchTerm) !!}</p>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>

