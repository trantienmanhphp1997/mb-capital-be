<div class="body-content p-2">
    <div class="card">
        <div class="card-body p-2">
            <div class="filter d-flex align-items-center justify-content-between mb-2">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group search-expertise">
                            <div class="search-expertise inline-block">
                                <input type="text" placeholder="Tìm kiếm" name="search"
                                    class="form-control" id='input_vn_name' autocomplete="off" wire:model.debounce.1000ms="searchTerm">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div>
                            <select wire:model.debounce.1000ms="userId" class="form-control">
                                <option value=''>
                                    --Người tác động--
                                </option>
                                @foreach($userList as $item)
                                    <option value="{{$item['id']}}">
                                        {{$item['name']}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <input type="date" class="form-control" name="fromDate" wire:model.debounce.1000ms="from_date" autocomplete="off">
                    </div>

                    <div class="col-md-3">
                        <input type="date" class="form-control" name="toDate" wire:model.debounce.1000ms="to_date" autocomplete="off">
                    </div>
                </div>
            </div>
            
            <table class="table table-bordered table-hover dataTable dtr-inline">
                <thead class="">
                    <tr>
                        <th>Người tác động</th>
                        <th>Thao tác</th>
                        <th>Loại bảng Audit</th>
                        <th>ID bảng Audit</th>
                        {{-- <th>Giá trị cũ</th> --}}
                        {{-- <th>Giá trị mới</th> --}}
                        <th>Lịch sử</th>
                        <th>Ngày tạo</th>
                        <th>URL</th>
                        <th>Địa chỉ IP</th>
                        <th>Tác nhân</th>
                        <th>Ghi chú</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($data as $key => $row)
                        <tr>
                            <td class="text-left" title="{{$row['perfomer']}}">{!! boldTextSearch($row['perfomer'], $searchTerm) !!}</td>
                            <td class="text-left" title="{{$row['event']}}">{!! boldTextSearch($row['event'], $searchTerm) !!}</td>
                            <td class="text-left" title="{{$row['audittable_type']}}">{!! boldTextSearch($row['audittable_type'], $searchTerm) !!}</td>
                            <td class="text-left" title="{{$row['audittable_id']}}">{!! boldTextSearch($row['audittable_id'], $searchTerm) !!}</td>
                            <td class="text-left">
                                <a href="" data-toggle="modal" data-target="{{'#show' . $row['id']}}">Xem chi tiết</a>
                            </td>
                            <td class="text-left" title="{{$row['created_at']}}">{!!  chuanHoa($row['created_at']) !!}</td>
                            <td class="text-left" title="{{$row['url']}}">
                                <p class="mb-1 content-audit-data">{!! boldTextSearch($row['url'], $searchTerm) !!}</p>
                            </td>
                            <td class="text-left" title="{{$row['ip_address']}}">{!! boldTextSearch($row['ip_address'], $searchTerm) !!}</td>
                            <td class="text-left" title="{{$row['user_agent']}}">
                                <p class="mb-1 content-audit-data">{!! boldTextSearch($row['user_agent'], $searchTerm) !!}</p>
                            </td>
                            <td class="text-left" title="{{$row['note']}}">{!! boldTextSearch($row['note'], $searchTerm) !!}</td>
                        </tr>
                    @empty
                        <td colspan='12' class='text-center'>Không tìm thấy dữ liệu</td>
                    @endforelse
                </tbody>
            </table>
        </div>
        @if(count($data))
            {{ $data->links() }}
        @endif
    </div>
    @include('livewire.common._modalDelete')
    @foreach($data as $key => $row)
        @include('livewire.system.audit._modalValues')
    @endforeach
</div>