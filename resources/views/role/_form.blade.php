@section('css')
    <link rel="stylesheet" href="{{asset('plugins/select2/css/select2.min.css')}}">
@endsection
<div class="card py-2 px-3">
    <div class="form_title">
        <label>Tên vai trò (<span style='color:red;'>*</span>)</span> </label>
        <div class="row">
            <div class="col">
                <div class="input-group form-group">
                    {!! Form::text('name',null, array('placeholder' => 'Nhập tên vai trò','class' => 'form_control col-md-3')) !!}
                    @error('name')
                        @include('layouts.partials.text._error')
                    @enderror
                </div>
            </div>
        </div>
    </div>

    <div class="form_title">
        <label>Quyền (<span style='color:red;'>*</span>) </label>
        <div class="row">
            <div class="col">
                <div class="input-group form-group">
                    <select name="permissions[]" id="permissions" autocomplete="off" class="form_control select_box col-md-3" multiple>
                    @foreach($rolePermissions as $key => $rolePermission)
                        <option value='{{$key}}' @foreach($permissions as $value)@if($key == $value) selected @endif @endforeach>{{$rolePermission}}</option>
                    @endforeach
                    </select>
                    @error('permissions')
                        @include('layouts.partials.text._error')
                    @enderror
                </div>
            </div>
        </div>
    </div> 
    
    <div class="w-100 clearfix my-2">
        <button name="submit" type="submit" id="save" class="float-right btn ml-1 btn-primary">Lưu lại</button>
        <a href="{{ route('roles.index') }}" class="btn btn-secondary float-right mr-1">Hủy</a>          
    </div>
</div>

@section('js')
    <script src="{{asset('plugins/select2/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $(function () {
                $(".select_box").select2({
                    placeholder: "Chọn  quyền...",
                    allowClear: true
                });
            });
        });
    </script>
@endsection