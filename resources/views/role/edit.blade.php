@extends('layouts.master')


@section('content')
<div class="body-content p-2">
    <div class="p-2 pb-3 d-flex align-items-center justify-content-between">
        <div class="">
            <h4 class="m-0">
                Chỉnh sửa vai trò
            </h4>
        </div>
        <div class="paginate">
            <div class="d-flex">
                <div class="">
                    <a href="{{ route('home') }}"><i class="fa fa-home"></i> Trang chủ</a>
                </div>
                <span class="px-2">/</span>
                <div class="">
                    <div class="disable">Chỉnh sửa vai trò</div>
                </div>
            </div>
        </div>
    </div>
</div>
    {!! Form::model($data, ['method' => 'POST', 'autocomplete' => "off",'route' => ['roles.update', $data->id]]) !!}
        @include('role._form')
    {!! Form::close() !!}
@endsection

@section('js')
    <script src="{{asset('assets/select2/select2.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $(function () {
                $(".select_box").select2({
                    placeholder: "Chọn  quyền...",
                    allowClear: true
                });
            });
        });
    </script>
@endsection
