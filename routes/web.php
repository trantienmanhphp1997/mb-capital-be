<?php

use App\Http\Livewire\Product\Index;
use App\Http\Livewire\Shop\Cart;
use App\Http\Livewire\Shop\Checkout;
use App\Http\Livewire\Shop\Index as ShopIndex;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/', function () {
    return view('auth.login');
});
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/admin/product', Index::class)->middleware('auth')->name('admin.product');
// Route::get('/', ShopIndex::class)->name('shop.index');
Route::get('/cart', Cart::class)->name('shop.cart');
Route::get('/checkout', Checkout::class)->name('shop.checkout');

Route::get('/adviselist','App\Http\Controllers\AdviseController@index')->name('advise_list');
Route::get('/faq','App\Http\Controllers\FaqController@index')->name('faq.index');

Route::group(['middleware' => ['auth','preventBackHistory']], function () {
    // Route article
    Route::group([
        'prefix' => 'new',
    ], function(){
        Route::get('/', 'App\Http\Controllers\Admin\News\ArticleController@index')->name('admin.new.list.index');
        Route::get('/edit/{id}', 'App\Http\Controllers\Admin\News\ArticleController@edit')->name('admin.new.edit');
        Route::put('/update/{id}', 'App\Http\Controllers\Admin\News\ArticleController@update')->name('admin.new.update');
        Route::get('/create', 'App\Http\Controllers\Admin\News\ArticleController@create')->name('admin.new.create');
        Route::put('/store', 'App\Http\Controllers\Admin\News\ArticleController@store')->name('admin.new.store');
        Route::get('/detail/{id}', 'App\Http\Controllers\Admin\News\ArticleController@detail')->name('admin.new.detail');
        Route::delete('/delete/{id}', 'App\Http\Controllers\Admin\News\ArticleController@destroy')->name('admin.new.delete');
    });

    Route::get('/menu-config', 'App\Http\Controllers\MenuConfigController@index')->name('admin.menu-config.index');
    Route::group([
        'prefix' => 'master',
    ], function () {
        Route::get('/', 'App\Http\Controllers\Admin\Config\MasterController@index')->name('admin.config.master');
        Route::get('delete/{id}', 'App\Http\Controllers\Admin\Config\MasterController@delete')->name('admin.config.master.delete');
        Route::get('create', 'App\Http\Controllers\Admin\Config\MasterController@create')->name('admin.config.master.create');
        Route::get('edit/{id}', 'App\Http\Controllers\Admin\Config\MasterController@edit')->name('admin.config.master.edit');
        Route::post('insert', 'App\Http\Controllers\Admin\Config\MasterController@insert')->name('admin.config.master.insert');
        Route::post('update', 'App\Http\Controllers\Admin\Config\MasterController@update')->name('admin.config.master.update');
    });

    Route::group([
        'prefix' => 'funds',
    ], function () {
        Route::get('/', 'App\Http\Controllers\FundsController@index')->name('admin.funds');
        Route::get('/create', 'App\Http\Controllers\FundsController@detail')->name('admin.funds.create');
        Route::get('/edit/{id}', 'App\Http\Controllers\FundsController@detail')->name('admin.funds.edit');
        Route::post('/store', 'App\Http\Controllers\FundsController@actionDetail')->name('admin.funds.store');
        Route::put('/update/{id}', 'App\Http\Controllers\FundsController@actionDetail')->name('admin.funds.update');
    });
    Route::group([
        'prefix' => 'fundnews',
    ], function () {
        Route::get('/', 'App\Http\Controllers\FundNewsController@index')->name('admin.fundnews');
        Route::get('/create', 'App\Http\Controllers\FundNewsController@create')->name('admin.fundnews.create');
        Route::get('/edit/{id}', 'App\Http\Controllers\FundNewsController@edit')->name('admin.fundnews.edit');
        Route::get('/download/{id}', 'App\Http\Controllers\FundNewsController@download')->name('admin.fundnews.download');
        Route::put('/update/{id}', 'App\Http\Controllers\FundNewsController@update')->name('admin.fundnews.update');
    });

    Route::group([
        'prefix' => 'fundmaster',
    ], function () {

        Route::get('/', 'App\Http\Controllers\FundMasterController@index')->name('admin.fundmaster');
        Route::get('/create', 'App\Http\Controllers\FundMasterController@detail')->name('admin.fundmaster.create');
        Route::get('/edit/{id}', 'App\Http\Controllers\FundMasterController@detail')->name('admin.fundmaster.edit');
        Route::post('/store', 'App\Http\Controllers\FundMasterController@actionDetail')->name('admin.fundmaster.store');
        Route::put('/update/{id}', 'App\Http\Controllers\FundMasterController@actionDetail')->name('admin.fundmaster.update');
        Route::get('/destroy/{id}', 'App\Http\Controllers\FundMasterController@destroy')->name('admin.fundmaster.destroy');
    });

    Route::group([
        'prefix' => 'fundnav',
    ], function () {

        Route::get('/', 'App\Http\Controllers\FundNAVController@index')->name('admin.fundnav');
        Route::get('/create', 'App\Http\Controllers\FundNAVController@create')->name('admin.fundnav.create');
        Route::get('/edit/{id}', 'App\Http\Controllers\FundNAVController@edit')->name('admin.fundnav.edit');
        Route::post('/store', 'App\Http\Controllers\FundNAVController@store')->name('admin.fundnav.store');
        Route::post('/update/{id}', 'App\Http\Controllers\FundNAVController@update')->name('admin.fundnav.update');
        // Route::get('/destroy/{id}', 'App\Http\Controllers\FundNAVController@destroy')->name('admin.fundnav.destroy');
    });

    Route::group([
        'prefix' => 'recruitment',
    ], function () {
        Route::get('/', 'App\Http\Controllers\RecruitmentController@index')->name('admin.recruitment.index');
    });

    Route::group([
        'prefix' => 'employee',
    ], function () {
        Route::get('/', 'App\Http\Controllers\EmployeeController@index')->name('admin.employee.index');
        Route::get('/create', 'App\Http\Controllers\EmployeeController@create')->name('admin.employee.create');
        Route::get('/edit/{id}', 'App\Http\Controllers\EmployeeController@edit')->name('admin.employee.edit');
        Route::post('/store', 'App\Http\Controllers\EmployeeController@store')->name('admin.employee.store');
        Route::put('/update/{id}', 'App\Http\Controllers\EmployeeController@update')->name('admin.employee.update');
    });


    Route::prefix('nguoiDung')->group(function (){
        Route::get('/create', 'App\Http\Controllers\NguoiDungController@create')->name('nguoiDung.create.index');
        Route::post('/store', 'App\Http\Controllers\NguoiDungController@store')->name('nguoiDung.store');
        Route::get('/{id}/edit', 'App\Http\Controllers\NguoiDungController@edit')->name('nguoiDung.edit.index');
        Route::patch('/{id}', 'App\Http\Controllers\NguoiDungController@update')->name('nguoiDung.update');
        Route::patch('/updateRole', 'App\Http\Controllers\NguoiDungController@updateRole')->name('nguoiDung.update_role');
        Route::get('/', 'App\Http\Controllers\NguoiDungController@index')->name('nguoiDung.index');

    });

    Route::group(['prefix' => 'role'], function() {
        Route::get('/', 'App\Http\Controllers\RoleController@index')->name('roles.index');
        Route::get('/edit/{id}', 'App\Http\Controllers\RoleController@edit')->name('roles.edit');
        Route::get('/create', 'App\Http\Controllers\RoleController@create')->name('roles.create');
        Route::post('/store', 'App\Http\Controllers\RoleController@store')->name('roles.store');
        Route::get('/show/{id}', 'App\Http\Controllers\RoleController@show')->name('roles.show');
        Route::post('/update/{id}', 'App\Http\Controllers\RoleController@update')->name('roles.update');
    });

    Route::group(['prefix' => 'guideline'], function() {
        Route::get('/', 'App\Http\Controllers\GuidelineController@index')->name('guideline.index');
        Route::get('/edit/{id}', 'App\Http\Controllers\GuidelineController@edit')->name('guideline.edit');
        Route::post('/update/{id}', 'App\Http\Controllers\GuidelineController@update')->name('guideline.update');
        Route::get('/create', 'App\Http\Controllers\GuidelineController@edit')->name('guideline.create');
        Route::post('/store', 'App\Http\Controllers\GuidelineController@edit')->name('guideline.store');
        Route::get('/destroy/{id}', 'App\Http\Controllers\GuidelineController@destroy')->name('guideline.destroy');
        Route::get('/detail/{id}', 'App\Http\Controllers\GuidelineController@detail')->name('guideline.detail');
    });

    Route::group(['prefix' => 'survey'], function() {
        Route::get('/', 'App\Http\Controllers\SurveyController@index')->name('survey.index');
        Route::get('/detail/{id}', 'App\Http\Controllers\SurveyController@detail')->name('survey.detail');
    });

    Route::group(['prefix' => 'guideline_detail'], function() {
        // Route::get('/', 'App\Http\Controllers\GuidelineDetailController@index')->name('guideline_detail.index');
        Route::get('/edit/{id}', 'App\Http\Controllers\GuidelineDetailController@edit')->name('guideline_detail.edit');
        Route::patch('/update/{id}', 'App\Http\Controllers\GuidelineDetailController@update')->name('guideline_detail.update');
        Route::get('/create', 'App\Http\Controllers\GuidelineDetailController@create')->name('guideline_detail.create');
        Route::post('/store', 'App\Http\Controllers\GuidelineDetailController@store')->name('guideline_detail.store');
        Route::get('/destroy/{id}', 'App\Http\Controllers\GuidelineDetailController@destroy')->name('guideline_detail.destroy');
    });
    Route::group(['prefix' => 'files'], function() {
        Route::get('/', 'App\Http\Controllers\FilesController@index')->name('files.index');
        Route::get('/edit/{id}', 'App\Http\Controllers\FilesController@edit')->name('files.edit');
    });

    Route::group([
        'prefix' => 'system',
    ], function () {
        Route::get('/audit', 'App\Http\Controllers\AuditController@index')->name('system.audit.list');
    });

    Route::group(['prefix' => 'log_access'], function() {
        Route::get('/', 'App\Http\Controllers\LogAccessController@index')->name('log_access.index');
    });

    Route::group(['prefix' => 'seo_config'], function() {
        Route::get('/', 'App\Http\Controllers\SEOConfigController@index')->name('admin.seo_config.index');
    });

    Route::group(['prefix' => 'log_survey'], function () {
        Route::get('/', 'App\Http\Controllers\SurveyController@log_survey')->name('admin.log_survey.index');
    });

    Route::get("/test", "App\Http\Controllers\Admin\Test\TestController@index")->name("admin.test.index");

    Route::get("/fund-expected-profit", "App\Http\Controllers\Admin\Test\FundExpectedProfitController@index")->name("admin.fund-expected-profit.index");

    Route::get('/survey-response', 'App\Http\Controllers\Admin\Test\SurveyResponseController@index')->name('admin.survey_response.index');
    Route::get('/fund-employee-xrf', 'App\Http\Controllers\Admin\Test\FundEmployeeXRFController@index')->name('admin.fund_employee_xrf.index');
});
